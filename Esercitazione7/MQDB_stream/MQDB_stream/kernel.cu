#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"
#include "../../../Lib/mqdb.h"
#include "../../../Lib/mqdb.c"

#define BLOCK_SIZE 32     // block size

struct tms {
	double CPUtms;
	double GPUtmsNaive;
	double GPUtmsMQDB;
	double GPUtmsMQDBDynPar1;
	double GPUtmsMQDBDynPark;
	float density;
};

/*
 * Kernel for block sub-matrix product of mqdb
 */
__global__ void mqdbBlockProd(mqdb mA, mqdb mB, mqdb mC, int D, int N, int *offset_addr=NULL) {
	int offset = (offset_addr == NULL) ? 0 : *offset_addr;
	float* A = mA.elem;
	float* B = mB.elem;
	float* C = mC.elem;
	uint Row = blockIdx.y * blockDim.y + threadIdx.y;
	uint Col = blockIdx.x * blockDim.x + threadIdx.x;

	__shared__ float A_sm[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float B_sm[BLOCK_SIZE][BLOCK_SIZE];

	float ris = 0.0f;
	int nBlock = CEIL(N, BLOCK_SIZE);

	for (int m = 0; m < nBlock; ++m) {
		int c = BLOCK_SIZE * m + threadIdx.x;
		int r = BLOCK_SIZE * m + threadIdx.y;

		A_sm[threadIdx.y][threadIdx.x] = (Row < N && c < N) ? A[(Row + offset) * D + offset + c] : 0;
		B_sm[threadIdx.y][threadIdx.x] = (r < N && Col < N) ? B[(r + offset) * D + Col + offset] : 0;

		__syncthreads();

		int lim = (m + 1 != nBlock) ? BLOCK_SIZE : N - m * BLOCK_SIZE;

		for (int k = 0; k < lim; ++k) {
			ris += A_sm[threadIdx.y][k] * B_sm[k][threadIdx.x];
		}

		__syncthreads();
	}

	if (Row < N && Col < N) {
		C[(Row + offset) * D + Col + offset] = ris;
	}	
}

/*
 * Kernel for block sub-matrix product of mqdb: parent grid (version grid 1)
 */
__global__ void mqdbProd(mqdb d_A, mqdb d_B, mqdb d_C, int n, int k, int* offsets) {
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	for (int i = 0; i < k; ++i) {
		dim3 grid(ceil(d_A.blkSize[i] / (float)(block.x)), ceil(d_A.blkSize[i] / (float)(block.y)));
		mqdbBlockProd << <grid, block >> > (d_A, d_B, d_C, n, d_A.blkSize[i], &offsets[i]);
	}
}

/*
 * Kernel for block sub-matrix product of mqdb: parent grid (version grid k)
 */
__global__ void mqdbProdK(mqdb d_A, mqdb d_B, mqdb d_C, int n, int* offsets) {
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid(ceil(n / (float)(block.x)), ceil(n / (float)(block.y)));
	mqdbBlockProd << <grid, block >> > (d_A, d_B, d_C, n, d_A.blkSize[threadIdx.x], &offsets[threadIdx.x]);
}

/*
 * main function
 */
int main(int argc, char* argv[]) {

	const uint n = 10 * 1024;
	const uint min_k = 50;       // min num of blocks
	const uint max_k = 50;       // max num of blocks

	struct tms times[max_k - min_k + 1];

	for (uint k = min_k; k <= max_k; k++) {
		mqdb A, B, C, C1;

		int* blkSize = (int*)malloc(k * sizeof(int));

		// fill in
		A = mqdbConst_UNIMEM(n, k, 10, 1);
		B = mqdbConst_UNIMEM(n, k, 10, 1);
		C = mqdbConst_UNIMEM(n, k, 10, 1);
		C1 = mqdbConst_UNIMEM(n, k, 10, 1);

		memcpy(blkSize, A.blkSize, k * sizeof(int));

		//offsets
		int* offsets;
		CHECK(cudaMallocManaged(&offsets, sizeof(int) * k));

		offsets[0] = 0;
		for (int i = 1; i < k; ++i) {
			offsets[i] = offsets[i - 1] + A.blkSize[i - 1];
		}

		ulong nBytes = n * n * sizeof(float);
		ulong kBytes = k * sizeof(uint);
		printf("Memory size required = %.1f (MB)\n", (float)nBytes / (1024.0 * 1024.0));

		cudaEvent_t start, stop;
		float ms;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);		
		
		dim3 block(BLOCK_SIZE, BLOCK_SIZE);
		dim3 grid;

		/***********************************************************/
	   /*                     GPU MQDB product                    */
	   /***********************************************************/
		printf("Kernel MQDB product...\n");
		cudaEventRecord(start, 0);
		for (int i = 0; i < k; ++i) {
			grid = dim3(ceil(blkSize[i] / (float)(block.x)), ceil(blkSize[i] / (float)(block.y)));
			mqdbBlockProd << <grid, block >> > (A, B, C, n, blkSize[i], &offsets[i]);
		}		
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&ms, start, stop);
		printf("   elapsed time:                        %.2f (millisec)\n", ms);				

		/***********************************************************/
		/*                     GPU MQDB GRID(1) product            */
		/***********************************************************/

		printf("Kernel MQDB GRID(1) product...\n");
		cudaEventRecord(start, 0);
		mqdbProd << <1, 1 >> > (A, B, C1, n, k, offsets);		
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&ms, start, stop);
		printf("   elapsed time:                        %.2f (millisec)\n", ms);
		checkResult(C1, C);
		CHECK(cudaMemset(C1.elem, 0.0, nBytes));

		/***********************************************************/
		/*                     GPU MQDB GRID(k) product            */
		/***********************************************************/

		printf("Kernel MQDB GRID(k) product...\n");
		cudaEventRecord(start, 0);
		mqdbProdK << <1, k >> > (A, B, C1, n, offsets);		
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&ms, start, stop);
		printf("   elapsed time:                        %.2f (millisec)\n", ms);
		checkResult(C1, C);
		CHECK(cudaMemset(C1.elem, 0.0, nBytes));

		/***********************************************************/
		/*                     GPU MQDB streams product            */
		/***********************************************************/

		printf("Kernel MQDB streams product...\n");
		cudaStream_t* streams = (cudaStream_t*)malloc(k * sizeof(cudaStream_t));
		for (int i = 0; i < k; ++i) {
			CHECK(cudaStreamCreate(&streams[i]));
		}
		cudaEventRecord(start, 0);
		for (int i = 0; i < k; ++i) {
			grid = dim3(ceil(blkSize[i] / (float)(block.x)), ceil(blkSize[i] / (float)(block.y)));
			mqdbBlockProd << <grid, block, 0, streams[i] >> > (A, B, C1, n, blkSize[i], &offsets[i]);
		}
		for (int i = 0; i < k; ++i) {
			CHECK(cudaStreamSynchronize(streams[i]));
			CHECK(cudaStreamDestroy(streams[i]));
		}
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&ms, start, stop);
		printf("   elapsed time:                        %.2f (millisec)\n", ms);
		checkResult(C1, C);
		CHECK(cudaMemset(C1.elem, 0.0, nBytes));

		/***********************************************************************/

		CHECK(cudaFree(A.elem));
		CHECK(cudaFree(A.blkSize));
		CHECK(cudaFree(B.elem));
		CHECK(cudaFree(B.blkSize));
		CHECK(cudaFree(C.elem));
		CHECK(cudaFree(C.blkSize));
		CHECK(cudaFree(C1.elem));
		CHECK(cudaFree(C1.blkSize));

		cudaEventDestroy(start);
		cudaEventDestroy(stop);

	}
	return 0;
}
