#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define PI 3.141592f

/*
 * Kernel: tabular function
 */
__global__ void tabular(float* a, int n) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < n) {
		float x = a[i];
		float s = sinf(x);
		float c = cosf(x);
		a[i] = sqrtf(abs(s * s - c * c));
	}
}


/*
 * Kernel: tabular function using streams
 */
__global__ void tabular_streams(float* a, int n, int offset) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i + offset < n) {
		float x = a[i];
		float s = sinf(x);
		float c = cosf(x);
		a[i] = sqrtf(abs(s * s - c * c));
	}
}

/*
 * Error measure
 */
float maxError(float* a, int n) {
	float maxE = 0;
	for (int i = 0; i < n; i++) {
		float error = fabs(a[i] - 1.0f);
		if (error > maxE)
			maxE = error;
	}
	return maxE;
}

/*
 * MAIN: tabular function
 */
int main(void) {
	const int blockSize = 256;
	const int nStreams = 16;
	const long GB = 1 << 30;
	const long n = 1 << 28;
	const long streamSize = n / nStreams;   // if n multiple of nStreams!
	const long streamBytes = streamSize * sizeof(float);
	const long nbytes = n * sizeof(float);

	int devId = 0;
	cudaDeviceProp prop;
	CHECK(cudaGetDeviceProperties(&prop, devId));
	printf("Device : %s\n", prop.name);
	CHECK(cudaSetDevice(devId));

	// prints
	printf("array size: %.2f GB\n", ((float)nbytes / GB));
	printf("num streams: %d\n", nStreams);
	printf("GB for stream: %f\n", (float)streamBytes / GB);

	// allocate pinned host memory and device memory
	float* cpu_a, * cpu_ris1, * cpu_ris2;
	cpu_a = (float*)malloc(nbytes);
	cpu_ris1 = (float*)malloc(nbytes);
	cpu_ris2 = (float*)malloc(nbytes);
	float* a, * d_a;

	cudaEvent_t start, stop;
	float ms;
	
	for (int i = 0; i < n; ++i) {
		cpu_a[i] = (i * PI) / n;
	}

	printf("GO\n");

	// baseline case - sequential transfer and execute
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	CHECK(cudaMalloc((void**)&d_a, nbytes));    // device mem	
	cudaMemcpy(d_a, cpu_a, nbytes, cudaMemcpyHostToDevice);
	tabular << < ceil(n / blockSize), blockSize >> > (d_a, n);
	cudaDeviceSynchronize();
	cudaMemcpy(cpu_ris1, d_a, nbytes, cudaMemcpyDeviceToHost);
	
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&ms, start, stop);

	printf("default stream time: \t %f sec\n", ms/1000.0);
	cudaFree(d_a);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	// asynchronous version 1: loop over {copy, kernel, copy}
	CHECK(cudaMallocHost((void**)&a, nbytes));  // host pinned mem

	cudaStream_t streams[nStreams];

	// create events and streams
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamCreate(&streams[i]);
	}

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	for (int i = 0; i < nStreams; ++i) {
		cudaMemcpyAsync(&a[i * streamSize], &cpu_a[i * streamSize], streamBytes, cudaMemcpyHostToDevice, streams[i]);
		tabular_streams << < ceil(streamSize / blockSize), blockSize, 0, streams[i] >> > (&a[i * streamSize], n, i * streamSize);
		cudaMemcpyAsync(&cpu_ris2[i * streamSize], &a[i * streamSize], streamBytes, cudaMemcpyDeviceToHost, streams[i]);
	}
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamSynchronize(streams[i]);
	}
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&ms, start, stop);
	printf("multi stream time v1: \t %f sec\n", ms/1000.0);

	for (int i = 0; i < nStreams; ++i) {
		cudaStreamDestroy(streams[i]);
	}

	for (int i = 0; i < n; ++i) {
		if (cpu_ris1[i] != cpu_ris2[i]) {
			printf("Errore elemento %d --> %.2f - %.2f\n", i, cpu_ris1[i], cpu_ris2[i]);
		}
	}

	// asynchronous version 2:
	// loop over copy, loop over kernel, loop over copy

	// create events and streams
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamCreate(&streams[i]);
	}

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	for (int i = 0; i < nStreams; ++i) {
		cudaMemcpyAsync(&a[i * streamSize], &cpu_a[i * streamSize], streamBytes, cudaMemcpyHostToDevice, streams[i]);
	}
	for (int i = 0; i < nStreams; ++i) {
		tabular_streams << < ceil(streamSize / blockSize), blockSize, 0, streams[i] >> > (&a[i * streamSize], n, i * streamSize);
	}
	for (int i = 0; i < nStreams; ++i) {
		cudaMemcpyAsync(&cpu_ris2[i * streamSize], &a[i * streamSize], streamBytes, cudaMemcpyDeviceToHost, streams[i]);
	}
	for (int i = 0; i < nStreams; ++i) {
		cudaStreamSynchronize(streams[i]);
	}
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&ms, start, stop);
	printf("multi stream v2 time: \t %f sec\n", ms / 1000.0);

	for (int i = 0; i < nStreams; ++i) {
		cudaStreamDestroy(streams[i]);
	}

	for (int i = 0; i < n; ++i) {
		if (cpu_ris1[i] != cpu_ris2[i]) {
			printf("Errore elemento %d --> %.2f - %.2f\n", i, cpu_ris1[i], cpu_ris2[i]);
		}
	}

	// cleanup


	return 0;
}
