#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <assert.h>
#include "../../../Lib/common.h"
#include <limits.h>

__global__ void blockParReduce1(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (idx >= n) {
		return;
	}

	array_in += bid;

	for (int i = 1; i < blockDim.x; i *= 2) {
		if (!(tid % (2 * i))) {
			array_in[tid] += array_in[tid + i];
		}
		__syncthreads();
	}

	if (tid == 0) {
		array_out[blockIdx.x] = array_in[0];
	}
}

__global__ void blockParReduce1_NoDiv(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (idx >= n) {
		return;
	}

	array_in += bid;

	for (int stride = 1; stride < blockDim.x; stride *= 2) {
		int index = 2 * stride * tid;
		if (index < blockDim.x) {
			array_in[index] += array_in[index + stride];
		}
		__syncthreads();
	}

	if (tid == 0) {
		array_out[blockIdx.x] = array_in[0];
	}
}

__global__ void blockParReduce2(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (idx >= n) {
		return;
	}

	array_in += bid;

	for (int i = blockDim.x / 2; i > 0; i /= 2) {
		if (tid < i) {
			array_in[tid] += array_in[tid + i];
		}
		__syncthreads();
	}

	if (tid == 0) {
		array_out[blockIdx.x] = array_in[0];
	}
}

__global__ void blockParReduceUroll(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (idx >= n) {
		return;
	}

	array_in += bid;

	for (int i = blockDim.x / 2; i > 32; i /= 2) {
		if (tid < i) {
			array_in[tid] += array_in[tid + i];
		}
		__syncthreads();
	}

	if (tid < 32) {
		volatile int* volMem = array_in;
		volMem[tid] += volMem[tid + 32];
		volMem[tid] += volMem[tid + 16];
		volMem[tid] += volMem[tid + 8];
		volMem[tid] += volMem[tid + 4];
		volMem[tid] += volMem[tid + 2];
		volMem[tid] += volMem[tid + 1];

		if (tid == 0) {
			array_out[blockIdx.x] = volMem[0];
		}
	}
}

__global__ void multBlockParReduceUroll8(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (bid * 8 + tid >= n) {
		return;
	}

	array_in += (bid * 8);

	int a1 = array_in[tid];
	int a2 = (bid * 8 + tid + blockDim.x < n) ? array_in[tid + blockDim.x] : 0;
	int a3 = (bid * 8 + tid + 2 * blockDim.x) ? array_in[tid + 2 * blockDim.x] : 0;
	int a4 = (bid * 8 + tid + 3 * blockDim.x) ? array_in[tid + 3 * blockDim.x] : 0;
	int a5 = (bid * 8 + tid + 4 * blockDim.x) ? array_in[tid + 4 * blockDim.x] : 0;
	int a6 = (bid * 8 + tid + 5 * blockDim.x) ? array_in[tid + 5 * blockDim.x] : 0;
	int a7 = (bid * 8 + tid + 6 * blockDim.x) ? array_in[tid + 6 * blockDim.x] : 0;
	int a8 = (bid * 8 + tid + 7 * blockDim.x) ? array_in[tid + 7 * blockDim.x] : 0;
	array_in[tid] = a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8;

	__syncthreads();

	for (int i = blockDim.x / 2; i > 32; i >>= 1) {
		if (tid < i) {
			array_in[tid] += array_in[tid + i];
		}
		__syncthreads();
	}

	if (tid < 32) {
		volatile int* volMem = array_in;
		volMem[tid] += volMem[tid + 32];
		volMem[tid] += volMem[tid + 16];
		volMem[tid] += volMem[tid + 8];
		volMem[tid] += volMem[tid + 4];
		volMem[tid] += volMem[tid + 2];
		volMem[tid] += volMem[tid + 1];
		if (tid == 0) {
			array_out[blockIdx.x] = volMem[0];
		}
	}
}

__global__ void multBlockParReduceUroll16(int* array_in, int* array_out, unsigned int n) {
	unsigned int tid = threadIdx.x;
	unsigned int bid = blockIdx.x * blockDim.x;
	unsigned int idx = bid + tid;

	if (bid * 16 + tid >= n) {
		return;
	}

	array_in += (bid * 16);

	int a1 = array_in[tid];
	int a2 = (bid * 16 + tid + blockDim.x < n) ? array_in[tid + blockDim.x] : 0;
	int a3 = (bid * 16 + tid + 2 * blockDim.x) ? array_in[tid + 2 * blockDim.x] : 0;
	int a4 = (bid * 16 + tid + 3 * blockDim.x) ? array_in[tid + 3 * blockDim.x] : 0;
	int a5 = (bid * 16 + tid + 4 * blockDim.x) ? array_in[tid + 4 * blockDim.x] : 0;
	int a6 = (bid * 16 + tid + 5 * blockDim.x) ? array_in[tid + 5 * blockDim.x] : 0;
	int a7 = (bid * 16 + tid + 6 * blockDim.x) ? array_in[tid + 6 * blockDim.x] : 0;
	int a8 = (bid * 16 + tid + 7 * blockDim.x) ? array_in[tid + 7 * blockDim.x] : 0;
	int a9 = (bid * 16 + tid + 8 * blockDim.x) ? array_in[tid + 8 * blockDim.x] : 0;
	int a10 = (bid * 16 + tid + 9 * blockDim.x) ? array_in[tid + 9 * blockDim.x] : 0;
	int a11 = (bid * 16 + tid + 10 * blockDim.x) ? array_in[tid + 10 * blockDim.x] : 0;
	int a12 = (bid * 16 + tid + 11 * blockDim.x) ? array_in[tid + 11 * blockDim.x] : 0;
	int a13 = (bid * 16 + tid + 12 * blockDim.x) ? array_in[tid + 12 * blockDim.x] : 0;
	int a14 = (bid * 16 + tid + 13 * blockDim.x) ? array_in[tid + 13 * blockDim.x] : 0;
	int a15 = (bid * 16 + tid + 14 * blockDim.x) ? array_in[tid + 14 * blockDim.x] : 0;
	int a16 = (bid * 16 + tid + 15 * blockDim.x) ? array_in[tid + 15 * blockDim.x] : 0;


	array_in[tid] = a1 + a2 + a3 + a4 + a5 + a6 + a7 + a8 + a9 + a10 + a11 + a12 + a13 + a14 + a15 + a16;

	__syncthreads();

	for (int i = blockDim.x / 2; i > 32; i >>= 1) {
		if (tid < i) {
			array_in[tid] += array_in[tid + i];
		}
		__syncthreads();
	}

	if (tid < 32) {
		volatile int* volMem = array_in;
		volMem[tid] += volMem[tid + 32];
		volMem[tid] += volMem[tid + 16];
		volMem[tid] += volMem[tid + 8];
		volMem[tid] += volMem[tid + 4];
		volMem[tid] += volMem[tid + 2];
		volMem[tid] += volMem[tid + 1];
		if (tid == 0) {
			array_out[blockIdx.x] = volMem[0];
		}
	}
}

int main()
{
	int* a, * b, * d_a, * d_b;
	const int blockSize = 1024;            // block dim 1D
	const ulong numBlock = 1024 * 96;      // grid dim 1D
	const ulong n = blockSize * numBlock;  // array dim
	ulong sum_CPU = 0, sum_GPU = 0;
	long double avg_CPU = 0, avg_GPU = 0, avg;
	const ulong nByte = n * sizeof(int), mByte = numBlock * sizeof(int);
	double start, stopGPU, stopCPU, speedup;

	avg = (n / 2.0) / n;

	printf("\n****  test on parallel reduction  ****\n");

	// init
	a = (int*)malloc(nByte);
	b = (int*)malloc(mByte);
	CHECK(cudaMalloc((void**)&d_a, nByte));
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_b, mByte));
	CHECK(cudaMemset((void*)d_b, 0, mByte));

	/***********************************************************/
	/*                     CPU reduction                       */
	/***********************************************************/
	printf("  Vector size: %.2f MB\n", n / (1024.0 * 1024.0));
	printf("\n  CPU procedure...\n");
	start = seconds();
	for (ulong i = 0; i < n; i++) {
		sum_CPU += a[i];
	}
	avg_CPU = sum_CPU;
	avg_CPU /= n;
	stopCPU = seconds() - start;
	printf("    Elapsed time: %f (sec) \n", stopCPU);
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_CPU == avg);
	printf("\n  GPU kernels (mem required %lu bytes)\n", nByte);

	/***********************************************************/
	/*         KERNEL blockParReduce1 (divergent)              */
	/***********************************************************/
	// block by block parallel implementation with divergence
	printf("\n  Launch kernel: blockParReduce1...\n");
	start = seconds();
	blockParReduce1 << <numBlock, blockSize >> > (d_a, d_b, n);
	CHECK(cudaGetLastError());
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < numBlock; i++) {
		sum_GPU += b[i];
	}
	avg_GPU = sum_GPU;
	avg_GPU /= n;	
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	// reset input vector on GPU
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));

	/***********************************************************/
	/*         KERNEL blockParReduce1.1 (divergent)              */
	/***********************************************************/
	// block by block parallel implementation with divergence
	printf("\n  Launch kernel: blockParReduce1 without divergence...\n");
	start = seconds();
	blockParReduce1_NoDiv << <numBlock, blockSize >> > (d_a, d_b, n);
	CHECK(cudaGetLastError());
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < numBlock; i++) {
		sum_GPU += b[i];
	}
	avg_GPU = sum_GPU;
	avg_GPU /= n;
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	// reset input vector on GPU
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));

	/***********************************************************/
	/*        KERNEL blockParReduce2  (non divergent)          */
	/***********************************************************/
	// block by block parallel implementation without divergence
	printf("\n  Launch kernel: blockParReduce2...\n");
	start = seconds();
	blockParReduce2 << <numBlock, blockSize >> > (d_a, d_b, n);
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	CHECK(cudaGetLastError());
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < numBlock; i++) {
		sum_GPU += b[i];
	}
	avg_GPU = sum_GPU;
	avg_GPU /= n;
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	// reset input vector on GPU
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));

	/***********************************************************/
	/*               KERNEL blockParReduceUroll                */
	/***********************************************************/
	// block by block parallel implementation without divergence
	printf("\n  Launch kernel: blockParReduceUroll...\n");
	start = seconds();
	blockParReduceUroll << <numBlock, blockSize >> > (d_a, d_b, n);
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	CHECK(cudaGetLastError());
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < numBlock; i++)
		sum_GPU += b[i];
	avg_GPU = sum_GPU;
	avg_GPU /= n;
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	// reset input vector on GPU
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));

	/***********************************************************/
	/*            KERNEL multBlockParReduceUroll8              */
	/***********************************************************/
	// block by block parallel implementation without divergence
	printf("\n  Launch kernel: multBlockParReduceUroll8...\n");
	start = seconds();
	multBlockParReduceUroll8 << <ceil(numBlock / 8.0), blockSize >> > (d_a, d_b, n);
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	CHECK(cudaGetLastError());
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < ceil(numBlock / 8.0); i++) {
		sum_GPU += b[i];
	}
	avg_GPU = sum_GPU;
	avg_GPU /= n;
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	// reset input vector on GPU
	for (ulong i = 0; i < n; i++) {
		a[i] = i % 2;
	}
	CHECK(cudaMemcpy(d_a, a, nByte, cudaMemcpyHostToDevice));

	/***********************************************************/
	/*            KERNEL multBlockParReduceUroll16             */
	/***********************************************************/
	// block by block parallel implementation without divergence
	printf("\n  Launch kernel: multBlockParReduceUroll16...\n");
	start = seconds();
	multBlockParReduceUroll16 << <ceil(numBlock / 16.0), blockSize >> > (d_a, d_b, n);
	CHECK(cudaDeviceSynchronize());
	stopGPU = seconds() - start;
	speedup = stopCPU / stopGPU;
	printf("    Elapsed time: %f (sec) - speedup %.1f\n", stopGPU, speedup);
	CHECK(cudaGetLastError());
	// memcopy D2H
	CHECK(cudaMemcpy(b, d_b, mByte, cudaMemcpyDeviceToHost));
	// check result
	sum_GPU = 0;
	for (uint i = 0; i < ceil(numBlock / 16.0); i++)
		sum_GPU += b[i];
	avg_GPU = sum_GPU;
	avg_GPU /= n;
	printf("    avg: %Lf\n", avg_CPU);
	assert(avg_GPU == avg);
	cudaFree(d_a);
	CHECK(cudaDeviceReset());
	return 0;
}