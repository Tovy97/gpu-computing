#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <stdio.h>
#include "../../../Lib/common.h"
#include "mqdb.c"

constexpr auto NUM_N = 3;
constexpr auto NUM_K = 3;

__global__ void matrix_mqdb_prod(mqdb* A, mqdb* B, mqdb* C, int n) {
	uint tid = blockIdx.x * blockDim.x + threadIdx.x;
	int block = 0;
	int offset = 0;
	int offset2 = 0;
	for (int i = 0; i < A->nBlocks; ++i) {
		if (tid >= offset + A->blkSize[i] * A->blkSize[i]) {
			block++;
			offset += A->blkSize[i] * A->blkSize[i];
			offset2 += A->blkSize[i];
		}
		else {
			break;
		}
	}
	if (block >= A->nBlocks) {
		return;
	}
	int r = (tid - offset) / A->blkSize[block];
	int c = (tid - offset) % A->blkSize[block];
	float* tempC = C->elem + (offset2 + r) * n + offset2 + c;

	for (int i = 0; i < A->blkSize[block]; ++i) {
		float* tempA = A->elem + (offset2 + r) * n + offset2 + i;
		float* tempB = B->elem + (offset2 + i) * n + offset2 + c;
		*tempC += *tempA * *tempB;
	}
}

__global__ void matrix_prod(mqdb* mA, mqdb* mB, mqdb* mC, int N, int M, int T) {
	float* A = mA->elem;
	float* B = mB->elem;
	float* C = mC->elem;
	uint tid = blockIdx.x * blockDim.x + threadIdx.x;
	int Row = tid / M;
	int Col = tid % M;
	if (Row < N && Col < M) {
		float val = 0;
		for (int k = 0; k < T; k++) {
			val += A[Row * T + k] * B[k * M + Col];
		}
		C[Row * M + Col] = val;
	}
}

__global__ void printTest(mqdb* mat) {
	mqdb M = *mat;
	int n = 0;
	printf("%s", M.desc);
	for (int j = 0; j < M.nBlocks; j++) {
		printf("%d  ", M.blkSize[j]);
		n += M.blkSize[j];
	}
	printf("\n");
	for (int j = 0; j < n * n; j++) {
		if (M.elem[j] == 0)
			printf("------");
		else
			printf("%5.2f ", M.elem[j]);
		if ((j + 1) % n == 0)
			printf("\n");
	}
	printf("\n");
}

__global__ void checkResultOnGPU(mqdb* d_mat, mqdb* matRes, int N, int M, int* res) {
	uint tid = blockIdx.x * blockDim.x + threadIdx.x;
	uint Row = tid / M;
	uint Col = tid % M;
	double epsilon = 1.0E-3;
	if (Row < N && Col < M && res == 0) {
		if (abs(d_mat->elem[Row * M + Col] - matRes->elem[Row * M + Col]) > epsilon) {
			*res = 1;
		}
	}
}

mqdb* copyMatrixDeviceToHost(mqdb* d_mat, int n, int k) {
	mqdb* mat = (mqdb*)malloc(sizeof(mqdb));
	float* elem = (float*)malloc(sizeof(float) * n * n);
	int* blkSize = (int*)malloc(sizeof(int) * k);

	CHECK(cudaMemcpy(mat, d_mat, sizeof(mqdb), cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(elem, mat->elem, sizeof(float) * n * n, cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(blkSize, mat->blkSize, sizeof(int) * k, cudaMemcpyDeviceToHost));

	mat->elem = elem;
	mat->blkSize = blkSize;

	return mat;
}

mqdb* copyMatrixHostToDevice(mqdb *mat, int n) {
	float* d_elem;
	CHECK(cudaMalloc(&d_elem, sizeof(float) * n * n));
	CHECK(cudaMemcpy(d_elem, mat->elem, sizeof(float) * n * n, cudaMemcpyHostToDevice));
	int* d_blkSize;
	CHECK(cudaMalloc(&d_blkSize, sizeof(int) * mat->nBlocks));
	CHECK(cudaMemcpy(d_blkSize, mat->blkSize, sizeof(int) * mat->nBlocks, cudaMemcpyHostToDevice));

	mat->elem = d_elem;
	mat->blkSize = d_blkSize;

	mqdb* d_mat;
	CHECK(cudaMalloc(&d_mat, sizeof(mqdb)));
	CHECK(cudaMemcpy(d_mat, mat, sizeof(mqdb), cudaMemcpyHostToDevice));
	return d_mat;
}

mqdb* createMatInDevice(int n, int k, int* blkSize, int seed = time(NULL), char type = 'R') {
	mqdb* mat = (mqdb*)malloc(sizeof(mqdb));
	if (type == 'R') {
		*mat = genRandMat(n, k, time(NULL));
	}
	else {
		*mat = mqdbConst(n, k, time(NULL), 0.0);
	}

	memcpy(mat->blkSize, blkSize, k * sizeof(int));
	fillBlocks(mat, n, k, type, 0.0);

	mqdb* d_mat = copyMatrixHostToDevice(mat, n);
	free(mat);
	return d_mat;
}

void check(mqdb* d_mat, mqdb* matRes, int n) {
	mqdb* d_matRes = copyMatrixHostToDevice(matRes, n);
	int* d_chck;
	CHECK(cudaMalloc(&d_chck, sizeof(int)));
	checkResultOnGPU << <ceil((n * n) / 1024.0), 1024 >> > (d_mat, d_matRes, n, n, d_chck);
	CHECK(cudaDeviceSynchronize());
	CHECK(cudaFree(d_matRes));
	int chck;
	CHECK(cudaMemcpy(&chck, d_chck, sizeof(int), cudaMemcpyDeviceToHost));
	CHECK(cudaFree(d_chck));
	if (chck) {
		printf("   * Arrays do not match!\n");
	}
	else {
		printf("   Arrays match\n");
	}
}

int main() {
	int n[NUM_N] = { 1000, 5000, 10000};
	int k[NUM_K] = { 3, 5, 10 };
	for (int i = 0; i < NUM_N; ++i) {
		for (int j = 0; j < NUM_K; ++j) {
			for (int s = 0; s < 3; ++s) {
				int* blkSize = genRandDims2(n[i], k[j]);
				mqdb* d_matA = createMatInDevice(n[i], k[j], blkSize, s);
				mqdb* d_matB = createMatInDevice(n[i], k[j], blkSize, s);
				mqdb* d_matC = createMatInDevice(n[i], k[j], blkSize, s, 'C');
				mqdb* matA = copyMatrixDeviceToHost(d_matA, n[i], k[j]);
				mqdb* matB = copyMatrixDeviceToHost(d_matB, n[i], k[j]);

				/*PROTODDO MQDB CPU*/
				double start = seconds();
				mqdb* ris = mqdbProd2(*matA, *matB, n[i]);
				double tmcpu1 = seconds() - start;
				printf("Prodotto MQDB [CPU] con seme = %5d, n = %5d e k = %5d --> Tempo: %f\n", s, n[i], k[j], tmcpu1);								
				free(ris);

				/*PROTODDO RIGA COLONNA CPU*/
				start = seconds();
				ris = matProd2(*matA, *matB, n[i]);
				double tmcpu2 = seconds() - start;
				printf("Prodotto RiCo [CPU] con seme = %5d, n = %5d e k = %5d --> Tempo: %f\n", s, n[i], k[j], tmcpu2);
				free(matA);
				free(matB);

				/*PRODOTTO MQDB GPU*/
				start = seconds();
				matrix_mqdb_prod << <ceil(matA->nElems / 1024.0), 1024 >> > (d_matA, d_matB, d_matC, n[i]);
				CHECK(cudaDeviceSynchronize());
				double tmgpu1 = seconds() - start;				

				printf("Prodotto MQDB [GPU] con seme = %5d, n = %5d e k = %5d --> Tempo: %f --> ", s, n[i], k[j], tmgpu1);
				check(d_matC, ris, n[i]);		

				cudaFree(d_matC);
				d_matC = createMatInDevice(n[i], k[j], blkSize, s, 'C');

				/*PRODOTTO CLASSICO RIGA COLONNA GPU*/
				start = seconds();
				matrix_prod << <ceil((n[i]* n[i] )/1024.0), 1024 >> > (d_matA, d_matB, d_matC, n[i], n[i], n[i]);
				CHECK(cudaDeviceSynchronize());
				double tmgpu2 = seconds() - start;				
				printf("Prodotto RiCo [GPU] con seme = %5d, n = %5d e k = %5d --> Tempo: %f --> ", s, n[i], k[j], tmgpu2);
				check(d_matC, ris, n[i]);

				printf("SpeedUp tra MQDB CPU e RiCo CPU %.1f\n", (tmcpu2 / tmcpu1));
				printf("SpeedUp tra RiCo GPU e RiCo CPU %.1f\n", (tmcpu2 / tmgpu2));
				printf("SpeedUp tra RiCo GPU e MQDB CPU %.1f\n", (tmcpu1 / tmgpu2));
				printf("SpeedUp tra MQDB GPU e RiCo CPU %.1f\n", (tmcpu2 / tmgpu1));
				printf("SpeedUp tra MQDB GPU e MQDB CPU %.1f\n", (tmcpu1 / tmgpu1));
				printf("SpeedUp tra MQDB GPU e RiCo GPU %.1f\n\n", (tmgpu2 / tmgpu1));

				free(ris);						
				free(blkSize);
				CHECK(cudaFree(d_matA));
				CHECK(cudaFree(d_matB));
				CHECK(cudaFree(d_matC));
				CHECK(cudaDeviceReset());
			}
		}
	}
}
