#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

__global__ void noDeadlock() {
	int test = threadIdx.x % 2;
	int a = 0;
	if (test) {
		a++;
	}
	else {
		a--;
	}
	__syncthreads();
}

__global__ void noDeadlock2() {
	int test = threadIdx.x % 2;
	int a = 0;
	if (test) {
		a++;
		__syncthreads();
	}
	else {
		a--; 
		__syncthreads();
	}
}

__global__ void siDeadlock() {
	int test = threadIdx.x % 2;
	int a = 0;
	if (test) {
		a++;		
		__syncthreads();		
	}
	else {
		a--;
		while (true) {

		}		
	}
}

int main() {
	printf("GO\nnoDedlock() ... ");
	noDeadlock << <1, 4 >> > ();
	cudaDeviceSynchronize();
	printf("FINE\nnoDedlock2() ... ");
	noDeadlock2 << <1, 4 >> > ();
	cudaDeviceSynchronize();
	printf("FINE\nsiDedlock() ... ");
	siDeadlock << <1, 4 >> > ();
	cudaDeviceSynchronize();
	printf("FINE\n");
}