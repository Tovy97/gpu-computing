#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

/*
 * Kernel with warp divergence
 */
__global__ void evenOdd_DIV(int* c, const unsigned int N) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int a = 0, b = 0;

	if (!(tid % 2)) { // branch divergence
		a = 2;
	} else {
		b = 1;
	}

	// check index
	if (tid < N) {
		c[tid] = a + b;
	}
}

/*
 * Kernel without warp divergence (N > 32)
 */
__global__ void evenOdd_NO_DIV(int* c, const unsigned int N) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	int a = 0, b = 0;

	int warp = tid / warpSize;
	//(tid % warpSize)							--> # numero del th dentro al warp (0..31)
	//2 * (tid % warpSize)						--> crea un spazio tra un th e l'altro (spazio riempito dai dispari)
	//((tid / (warpSize * 2)) * (warpSize * 2)) --> offset rispetto al warp
	int index = 2 * (tid % warpSize) + ((tid / (warpSize * 2)) * (warpSize * 2));
	if (!(warp % 2)) { //pari
		a = 2;
	}
	else { //dispari
		b = 1;
		index++;
	}

	if (index < N) {
		c[index] = a + b;
	}
}

/*
 * MAIN
 */
int main(int argc, char** argv) {
	// set up data size
	int blocksize = 1024;	
	//unsigned int data_size = 442925056; //limite per exec
	unsigned int data_size =   440270848; //limite per nvprof
	
	if (argc > 1) {
		blocksize = atoi(argv[1]);
	}
	if (argc > 2) {
		data_size = atoi(argv[2]);
	}

	printf("Data size %d\n", data_size);

	// set up execution configuration
	dim3 block(blocksize, 1, 1);
	dim3 grid((data_size + block.x - 1) / block.x, 1);
	printf("\nExecution Configure (block %d grid %d)\nKernels:\n", block.x, grid.x);

	// allocate memory
	int* d_C, * C;
	unsigned int nBytes = data_size * sizeof(int);
	C = (int*)malloc(nBytes);
	CHECK(cudaMalloc((void**)&d_C, nBytes));
	CHECK(cudaMemset(d_C, 0, nBytes));

	// run kernel 1
	double iStart, iElaps;

	SYSTEMTIME start, stop;
	GetSystemTime(&start);
	iStart = seconds();
	evenOdd_DIV << <grid, block >> > (d_C, data_size);
	CHECK(cudaDeviceSynchronize());
	iElaps = seconds() - iStart;
	GetSystemTime(&stop);
	printf("\tevenOddSIDIV<<<%d, %d>>> elapsed time %f sec (%f sec)\n\n", grid.x, block.x, iElaps, diffTime(start, stop));
	CHECK(cudaMemcpy(C, d_C, nBytes, cudaMemcpyDeviceToHost));

	// check
	for (unsigned int i = 0; i < data_size - 1; i += 2) {
		if (C[i] != 2 || C[i + 1] != 1) {
			printf("ERROR C[%d] = %d, C[%d] = %d,\n", i, C[i], i + 1, C[i + 1]);			
		}
	}

	CHECK(cudaMemset(d_C, 0, nBytes));

	// run kernel 2
	GetSystemTime(&start);
	iStart = seconds();
	evenOdd_NO_DIV << <grid, block >> > (d_C, data_size);
	CHECK(cudaDeviceSynchronize());
	iElaps = seconds() - iStart;
	GetSystemTime(&stop);
	printf("\tevenOddNODIV<<<%d, %d>>> elapsed time %f sec (%f sec)\n\n", grid.x, block.x, iElaps, diffTime(start, stop));
	CHECK(cudaGetLastError());

	CHECK(cudaMemcpy(C, d_C, nBytes, cudaMemcpyDeviceToHost));

	// check
	for (unsigned int i = 0; i < data_size - 1; i += 2) {
		if (C[i] != 2 || C[i + 1] != 1) {
			printf("ERROR C[%d] = %d, C[%d] = %d,\n", i, C[i], i + 1, C[i + 1]);
		}
	}

	free(C);
	// free gpu memory and reset device
	CHECK(cudaFree(d_C));
	CHECK(cudaDeviceReset());
	return EXIT_SUCCESS;
}