#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"
#include <stdbool.h>
#include <limits.h>

constexpr uint RIG = 32 * 32;
constexpr uint COL = 32 * 32;
constexpr uint MAX = UINT_MAX / 2;

__global__ void crivello(uint* d_matrix, uint* d_counter) {
	int r = blockIdx.y * blockDim.y + threadIdx.y;
	int c = blockIdx.x * blockDim.x + threadIdx.x;
	int idx = r * COL + c;

	for (int i = 0; i < idx; ++i) {
		if (d_matrix[i] != 0 && d_matrix[idx] % d_matrix[i] == 0) {
			d_matrix[idx] = 0;
			return;
		}
	}
	atomicAdd(d_counter, 1);
	for (int i = idx + 1; i < RIG * COL; ++i) {
		if (d_matrix[i] != 0 && d_matrix[i] % d_matrix[idx] == 0) {
			d_matrix[i] = 0;
		}
	}
}

__global__ void isPrime(uint* num, bool* prime) {
	uint tid = blockDim.x * blockIdx.x + threadIdx.x + 2;
	if (tid >= *num || !*prime) {
		return;
	}
	if (*num % tid == 0) {
		*prime = false;
	}
}

void crivello() {
	uint* matrix = (uint*)malloc(RIG * COL * sizeof(uint));
	uint* d_matrix;
	CHECK(cudaMalloc(&d_matrix, RIG * COL * sizeof(uint)));

	matrix[0] = 2;
	for (int i = 1; i < RIG * COL; ++i) {
		matrix[i] = 2 * i + 1;
	}

	uint* counter = (uint*)malloc(sizeof(uint));
	uint* d_counter;
	CHECK(cudaMalloc(&d_counter, sizeof(uint)));
	CHECK(cudaMemset(d_counter, 0, sizeof(uint)));

	CHECK(cudaMemcpy(d_matrix, matrix, RIG * COL * sizeof(uint), cudaMemcpyHostToDevice));

	crivello << < dim3(RIG / 32, COL / 32), dim3(32, 32) >> > (d_matrix, d_counter);

	CHECK(cudaDeviceSynchronize());

	CHECK(cudaMemcpy(matrix, d_matrix, RIG * COL * sizeof(uint), cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(counter, d_counter, sizeof(uint), cudaMemcpyDeviceToHost));

	for (int i = 0; i < RIG * COL; ++i) {
		if (matrix[i] != 0) {
			printf("%d\n", matrix[i]);
		}
	}

	CHECK(cudaFree(d_matrix));
	free(matrix);
}

typedef struct list {
	uint num;
	list* next;
} list;

inline void push(list** l, uint n) {
	list* node = (list*)malloc(sizeof(list));
	node->num = n;
	node->next = *l;
	*l = node;
}

inline void pop(list** l) {
	list* temp = *l;
	printf("%d\n", (*l)->num);
	*l = (*l)->next;
	free(temp);
}

void findPrime() {
	list* primes = NULL;
	push(&primes, 2);
	bool* d_prime;
	uint* d_num;
	CHECK(cudaMalloc(&d_prime, sizeof(bool)));
	CHECK(cudaMalloc(&d_num, sizeof(uint)));
	uint count = 1;
	uint grid = ceil(sqrt(MAX) / 1024.0);
	double start = seconds();
	for (int i = 1; i < MAX; ++i) {
		bool prime = true;
		uint num = i * 2 + 1;
		CHECK(cudaMemcpy(d_prime, &prime, sizeof(bool), cudaMemcpyHostToDevice));
		CHECK(cudaMemcpy(d_num, &num, sizeof(uint), cudaMemcpyHostToDevice));
		isPrime << <grid, 1024 >> > (d_num, d_prime);
		CHECK(cudaDeviceSynchronize());
		CHECK(cudaMemcpy(&prime, d_prime, sizeof(bool), cudaMemcpyDeviceToHost));
		if (prime) {
			push(&primes, num);
			count++;
		}
	}
	double end = seconds() - start;
	for (uint i = 0; i < count; ++i) {
		pop(&primes);
	}
	printf("\nTOTALE: %d in %f secondi\n", count, end);
	CHECK(cudaFree(d_num));
	CHECK(cudaFree(d_prime));
	CHECK(cudaDeviceReset());
}

int main() {
	findPrime();
	return 0;
}