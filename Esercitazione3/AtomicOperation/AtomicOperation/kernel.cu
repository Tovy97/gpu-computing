#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <stdio.h>
#include "../../../Lib/common.h"

__global__ void incrpu(int *c) {
    (*c)+=1;
}

__global__ void incrte(int* c) {
    int temp = *c;
    temp++;
    (*c) = temp;
}

__global__ void incrpp(int* c) {
    (*c)++;
}

__global__ void incrat(int* c) {
    atomicAdd(c, 1);
}

__global__ void testAtomic(int* g_odata) {
    // access thread id
    const unsigned int tid = blockDim.x * blockIdx.x + threadIdx.x;
    // Arithmetic atomic instructions
    atomicAdd(&g_odata[0], 10); // Atomic addition
    atomicSub(&g_odata[1], 10); // Atomic subtraction
    atomicMax(&g_odata[3], tid); // Atomic maximum
    atomicMin(&g_odata[4], tid); // Atomic minimum
    atomicInc((unsigned int*)&g_odata[5], 17); // Atomic increment
    atomicDec((unsigned int*)&g_odata[6], 137); // Atomic decrement
    // Arithmetic atomic instructions
    atomicExch(&g_odata[2], tid); // Atomic exchange
    atomicCAS(&g_odata[7], tid - 1, tid); // Atomic compare-and-swap
    // Bitwise atomic instructions
    atomicAnd(&g_odata[8], 2 * tid + 7); // Atomic AND
    atomicOr(&g_odata[9], 1 << tid); // Atomic OR
    atomicXor(&g_odata[10], tid); // Atomic XOR
}

int main() {
    int c, *d_c;
    CHECK(cudaMalloc(&d_c, sizeof(int)));
    cudaMemset(d_c, 0, sizeof(int));

    incrpp << <1024, 1024 >> > (d_c);

    CHECK(cudaDeviceSynchronize());
    CHECK(cudaMemcpy(&c, d_c, sizeof(int), cudaMemcpyDeviceToHost));

    if (c != 1024 * 1024) {
        printf("ERRORE, ci si aspettava %d e si � ottenuto %d\n\n", (1024*1024), c);
    }
    else {
        printf("CORRETTO\n\n");
    }

    cudaMemset(d_c, 0, sizeof(int));

    incrpu << <1024, 1024 >> > (d_c);

    CHECK(cudaDeviceSynchronize());
    CHECK(cudaMemcpy(&c, d_c, sizeof(int), cudaMemcpyDeviceToHost));

    if (c != 1024 * 1024) {
        printf("ERRORE, ci si aspettava %d e si � ottenuto %d\n\n", (1024 * 1024), c);
    }
    else {
        printf("CORRETTO\n\n");
    }

    cudaMemset(d_c, 0, sizeof(int));

    incrte << <1024, 1024 >> > (d_c);

    CHECK(cudaDeviceSynchronize());
    CHECK(cudaMemcpy(&c, d_c, sizeof(int), cudaMemcpyDeviceToHost));

    if (c != 1024 * 1024) {
        printf("ERRORE, ci si aspettava %d e si � ottenuto %d\n\n", (1024 * 1024), c);
    }
    else {
        printf("CORRETTO\n\n");
    }

    cudaMemset(d_c, 0, sizeof(int));

    incrat << <1024, 1024 >> > (d_c);

    CHECK(cudaDeviceSynchronize());
    CHECK(cudaMemcpy(&c, d_c, sizeof(int), cudaMemcpyDeviceToHost));

    if (c != 1024 * 1024) {
        printf("ERRORE, ci si aspettava %d e si � ottenuto %d\n\n", (1024 * 1024), c);
    }
    else {
        printf("CORRETTO\n\n");
    }

    int* test, * d_test;
    CHECK(cudaMalloc(&d_test, 11 * sizeof(int)));
    test = (int*)malloc(11 * sizeof(int));

    CHECK(cudaMemset(d_test, 0, 11 * sizeof(int)));
    memset(test, 0, 11 * sizeof(int));

    testAtomic << <1, 1 >> > (d_test);

    CHECK(cudaDeviceSynchronize());
    CHECK(cudaMemcpy(test, d_test, 11 * sizeof(int), cudaMemcpyDeviceToHost));

    for (int i = 0; i < 11; ++i) {
        printf("%d --> %d\n", i, test[i]);
    }

}