struct SoA {
	uchar r[N];
	uchar g[N];
	uchar b[N];
};

/*
 * Riscala l'immagine al valore massimo [max] fissato
 */
__global__ void rescaleImgSoA(SoA* img, const int max, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		img->r[i] = max * (float)img->r[i] / 255.0f;
		img->g[i] = max * (float)img->g[i] / 255.0f;
		img->b[i] = max * (float)img->b[i] / 255.0f;
	}	
}

/*
 * cancella un piano dell'immagine [plane = 'r' o 'g' o 'b'] fissato
 */
__global__ void deletePlaneSoA(SoA* img, const char plane, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		switch (plane) {
		case 'r':
			img->r[i] = 0;
			break;
		case 'g':
			img->g[i] = 0;
			break;
		case 'b':
			img->b[i] = 0;
			break;
		}
	}
}

/*
 * setup device
 */
__global__ void warmupSoA(SoA* img, const int max, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		img->r[i] = max * (float)img->r[i] / 255.0f;
		img->g[i] = max * (float)img->g[i] / 255.0f;
		img->b[i] = max * (float)img->b[i] / 255.0f;
	}
}

void initializeSoA(SoA* img, int size) {
	for (int i = 0; i < size; i++) {
		img->r[i] = rand() % 256;
		img->g[i] = rand() % 256;
		img->b[i] = rand() % 256;
	}
	return;
}

void checkResultSoA(SoA* img, SoA* new_img, int n_elem) {
	for (int i = 0; i < n_elem; i += 1000)
		printf("img[%d] = (%d,%d,%d) -- new_img[%d] = (%d,%d,%d)\n",
			i, img->r[i], img->g[i], img->b[i], i, new_img->r[i], new_img->g[i], new_img->b[i]);
	return;
}