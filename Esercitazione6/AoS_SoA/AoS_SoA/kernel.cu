#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define N 1<<24
#define blocksize 128

#include "AoS.h"
#include "SoA.h"


AoS* use_AoS(int argc, char** argv) {
	// set up device
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	printf("%s test AoS at ", argv[0]);
	printf("device %d: %s \n", dev, deviceProp.name);
	CHECK(cudaSetDevice(dev));

	// scelta del kernel da eseguire
	int kernel = 0;
	if (argc > 1) kernel = atoi(argv[1]);

	// allocate host memory
	int n_elem = N;
	size_t nBytes = n_elem * sizeof(AoS);
	AoS* img = (AoS*)malloc(nBytes);
	AoS* new_img = (AoS*)malloc(nBytes);

	// initialize host array
	initializeAoS(img, N);

	// allocate device memory
	AoS* d_img;
	CHECK(cudaMalloc((void**)&d_img, nBytes));

	// copy data from host to device
	CHECK(cudaMemcpy(d_img, img, nBytes, cudaMemcpyHostToDevice));

	// definizione max
	int max = 128;
	if (argc > 2) max = atoi(argv[2]);

	// configurazione per esecuzione
	dim3 block(blocksize, 1);
	dim3 grid((n_elem + block.x - 1) / block.x, 1);

	// kernel 1: warmup
	double iStart = seconds();
	warmupAoS << <1, 32 >> > (d_img, max, 32);
	CHECK(cudaDeviceSynchronize());
	double iElaps = seconds() - iStart;
	printf("warmup<<< 1, 32 >>> elapsed %f sec\n", iElaps);
	CHECK(cudaGetLastError());

	// kernel 2 rescaleImg o deletePlane
	iStart = seconds();
	if (kernel == 0) {
		rescaleImgAoS << <grid, block >> > (d_img, max, n_elem);
		printf("rescaleImg <<< %3d, %3d >>> elapsed %f sec\n", grid.x, block.x, iElaps);
	}
	else {
		deletePlaneAoS << <grid, block >> > (d_img, 'r', n_elem);
		printf("deletePlane <<< %3d, %3d >>> elapsed %f sec\n", grid.x, block.x, iElaps);
	}
	CHECK(cudaDeviceSynchronize());
	iElaps = seconds() - iStart;
	CHECK(cudaMemcpy(new_img, d_img, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaGetLastError());

	//checkResultAoS(img, new_img, n_elem);

	// free memories both host and device
	CHECK(cudaFree(d_img));
	free(img);	

	// reset device
	CHECK(cudaDeviceReset());

	return new_img;
}

SoA* use_SoA(int argc, char** argv) {
	// set up device
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	printf("\n%s test SoA at ", argv[0]);
	printf("device %d: %s \n", dev, deviceProp.name);
	CHECK(cudaSetDevice(dev));

	// scelta del kernel da eseguire
	int kernel = 0;
	if (argc > 1) kernel = atoi(argv[1]);

	// allocate host memory
	int n_elem = N;
	size_t nBytes = sizeof(SoA);
	SoA* img = (SoA*)malloc(nBytes);
	SoA* new_img = (SoA*)malloc(nBytes);

	// initialize host array
	initializeSoA(img, N);

	// allocate device memory
	SoA* d_img;
	CHECK(cudaMalloc((void**)&d_img, nBytes));

	// copy data from host to device
	CHECK(cudaMemcpy(d_img, img, nBytes, cudaMemcpyHostToDevice));

	// definizione max
	int max = 128;
	if (argc > 2) max = atoi(argv[2]);

	// configurazione per esecuzione
	dim3 block(blocksize, 1);
	dim3 grid((n_elem + block.x - 1) / block.x, 1);

	// kernel 1: warmup
	double iStart = seconds();
	warmupSoA << <1, 32 >> > (d_img, max, 32);
	CHECK(cudaDeviceSynchronize());
	double iElaps = seconds() - iStart;
	printf("warmup<<< 1, 32 >>> elapsed %f sec\n", iElaps);
	CHECK(cudaGetLastError());

	// kernel 2 rescaleImg o deletePlane
	iStart = seconds();
	if (kernel == 0) {
		rescaleImgSoA << <grid, block >> > (d_img, max, n_elem);
		printf("rescaleImg <<< %3d, %3d >>> elapsed %f sec\n", grid.x, block.x, iElaps);
	}
	else {
		deletePlaneSoA << <grid, block >> > (d_img, 'r', n_elem);
		printf("deletePlane <<< %3d, %3d >>> elapsed %f sec\n", grid.x, block.x, iElaps);
	}
	CHECK(cudaDeviceSynchronize());
	iElaps = seconds() - iStart;
	CHECK(cudaMemcpy(new_img, d_img, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaGetLastError());

	//checkResultAoS(img, new_img, n_elem);

	// free memories both host and device
	CHECK(cudaFree(d_img));
	free(img);	

	// reset device
	CHECK(cudaDeviceReset());
	return new_img;
}

void check(AoS* aos, SoA* soa) {
	for (int i = 0; i < N; ++i) {
		if (aos[i].r != soa->r[i] || aos[i].g != soa->g[i] || aos[i].b != soa->b[i]) {
			printf("\n\nErrore all'elemento %d \t aos -> (%d, %d, %d) \t soa (%d, %d, %d)\n", i, aos[i].r, aos[i].g, aos[i].b, soa->r[i], soa->g[i], soa->b[i]);
			return;
		}
	}
	printf("\n\nOK!\n");
}

/*
 * Legge da stdin quale kernel eseguire: 0 per rescaleImg, 1 per deletePlane
 */
int main(int argc, char** argv) {
	AoS* aos = use_AoS( argc, argv);
	SoA* soa = use_SoA(argc, argv);

	check(aos, soa);

	return EXIT_SUCCESS;
}

