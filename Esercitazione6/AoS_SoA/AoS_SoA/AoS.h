struct AoS {
	uchar r;
	uchar g;
	uchar b;
};

/*
 * Riscala l'immagine al valore massimo [max] fissato
 */
__global__ void rescaleImgAoS(AoS* img, const int max, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		float r, g, b;
		AoS tmp = img[i];
		r = max * (float)tmp.r / 255.0f;
		tmp.r = (uchar)r;
		g = max * (float)tmp.g / 255.0f;
		tmp.g = (uchar)g;
		b = max * (float)tmp.b / 255.0f;
		tmp.b = (uchar)b;
		img[i] = tmp;
	}
}

/*
 * cancella un piano dell'immagine [plane = 'r' o 'g' o 'b'] fissato
 */
__global__ void deletePlaneAoS(AoS* img, const char plane, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		switch (plane) {
		case 'r':
			img[i].r = 0;
			break;
		case 'g':
			img[i].g = 0;
			break;
		case 'b':
			img[i].b = 0;
			break;
		}
	}
}

/*
 * setup device
 */
__global__ void warmupAoS(AoS* img, const int max, const int n) {
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < n) {
		float r, g, b;
		AoS tmp = img[i];
		r = max * (float)tmp.r / 255.0f;
		tmp.r = (uchar)r;
		g = max * (float)tmp.g / 255.0f;
		tmp.g = (uchar)g;
		b = max * (float)tmp.b / 255.0f;
		tmp.b = (uchar)b;
		img[i] = tmp;
	}
}

void initializeAoS(AoS* img, int size) {
	for (int i = 0; i < size; i++) {
		img[i].r = rand() % 256;
		img[i].g = rand() % 256;
		img[i].b = rand() % 256;
	}
	return;
}

void checkResultAoS(AoS* img, AoS* new_img, int n_elem) {
	for (int i = 0; i < n_elem; i += 1000)
		printf("img[%d] = (%d,%d,%d) -- new_img[%d] = (%d,%d,%d)\n",
			i, img[i].r, img[i].g, img[i].b, i, new_img[i].r, new_img[i].g, new_img[i].b);
	return;
}