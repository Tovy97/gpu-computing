#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define BLOCK_SIZE 32 //100% di ld/st eff
#define RIG (1<<13)
#define COL (1<<13)

__global__ void trasposizione(const int *in, int *out) {
    int i = threadIdx.y;
    int j = threadIdx.x;
    int r = threadIdx.y + blockDim.y * blockIdx.y;
    int c = threadIdx.x + blockDim.x * blockIdx.x;

    __shared__ int matrix[BLOCK_SIZE][BLOCK_SIZE];

    matrix[i][j] = in[r * COL + c];
    
    __syncthreads();

    int tempR = i + blockDim.x * blockIdx.x;
    int tempC = j + blockDim.y * blockIdx.y;

    if ((tempR < COL) && (tempC < RIG)) {        
        out[tempR * RIG + tempC] = matrix[j][i];
    }
}

__global__ void naiveGmem(int* out, const int* in) {
    // coordinate matrice (x,y)
    unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;
    unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
    // transpose with boundary test
    if (y < RIG && x < COL)
        out[x * RIG + y] = in[y * COL +  x];
}

int main() {
    int *in, *out;
    CHECK(cudaMallocManaged(&in, sizeof(int) * RIG * COL));
    CHECK(cudaMallocManaged(&out, sizeof(int) * RIG * COL));

    for (int i = 0; i < RIG; ++i) {
        for (int j = 0; j < COL; ++j) {
            in[i * COL + j] = i * COL + j; 
            out[j * RIG + i] = 0;
        }
    }
    dim3 block(BLOCK_SIZE, BLOCK_SIZE);
    dim3 grid(CEIL(RIG, BLOCK_SIZE), CEIL(COL, BLOCK_SIZE));

    /*for (int i = 0; i < RIG; ++i) {
        for (int j = 0; j < COL; ++j) {
            printf("% 4d", in[i * COL + j]);
        }
        printf("\n");
    }
    printf("\n");*/

    double start = seconds();
    naiveGmem << < grid, block >> > (out, in);
    CHECK(cudaDeviceSynchronize());
    printf("Versione no SMEM:  %.2f sec\n", (seconds() - start));

    start = seconds();
    trasposizione << <grid, block >> > (in, out);    
    CHECK(cudaDeviceSynchronize());
    printf("Versione con SMEM: %.2f sec\n", (seconds() - start));

    /*for (int i = 0; i < COL; ++i) {
        for (int j = 0; j < RIG; ++j) {
            printf("% 4d", out[i * RIG + j]);
        }
        printf("\n");
    }*/

    return 0;
}
