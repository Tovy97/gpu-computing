/*
 * mqdb.cpp
 */

#include "mqdb.h"
#include <stdbool.h>

 /**
  * random generate block dimensions
  */
int genRandDims(mqdb* M, uint n, uint k) {

	if (n == 0 || k == 0 || k > n) {
		printf("error: n,k must be positive and n > k!\n");
		return(-1);
	}
	// random generation of block sizes
	M->blkSize = (int*)malloc(k * sizeof(int));
	int sum = 0;
	int r;
	float mu = 2.0f * (float)n / (float)k;
	for (int i = 0; i < k - 1; i++) {
		// expected value E[block_size] = n/k
		while ((r = round(mu * randu())) > n - sum - k + i + 1);
		if (!r)
			r += 1;
		M->blkSize[i] = r;
		sum += r;
	}
	M->blkSize[k - 1] = n - sum;
	return(0);
}

/**
 * random generate block dimensions
 */
int genRandDims_UNIMEM(mqdb* M, uint n, uint k) {
	if (n == 0 || k == 0 || k > n) {
		printf("error: n,k must be positive and n > k!\n");
		return(-1);
	}
	// random generation of block sizes	
	CHECK(cudaMallocManaged(&M->blkSize, k * sizeof(int)));
	int sum = 0;
	int r;
	float mu = 2.0f * (float)n / (float)k;
	for (int i = 0; i < k - 1; i++) {
		// expected value E[block_size] = n/k
		while ((r = round(mu * randu())) > n - sum - k + i + 1);
		if (!r)
			r += 1;
		M->blkSize[i] = r;
		sum += r;
	}
	M->blkSize[k - 1] = n - sum;
	return(0);
}

/**
  * random generate block dimensions 2
  */
int* genRandDims2(uint n, uint k) {

	if (n == 0 || k == 0 || k > n) {
		printf("error: n,k must be positive and n > k!\n");
		return NULL;
	}
	// random generation of block sizes
	int* blkSize = (int*)malloc(k * sizeof(int));
	int sum = 0;
	int r;
	float mu = 2.0f * (float)n / (float)k;
	for (int i = 0; i < k - 1; i++) {
		// expected value E[block_size] = n/k
		while ((r = round(mu * randu())) > n - sum - k + i + 1);
		if (!r)
			r += 1;
		blkSize[i] = r;
		sum += r;
	}
	blkSize[k - 1] = n - sum;
	return blkSize;
}

/**
 * fill blocks either random or constant
 */
void fillBlocks(mqdb* M, uint n, uint k, char T, float c) {
	//mat size n*n
	M->elem = (float*)calloc(n * n, sizeof(float));
	M->nElems = 0;
	int offset = 0;
	// loop on blocks
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < M->blkSize[i]; j++)
			for (int k = 0; k < M->blkSize[i]; k++)
				if (T == 'C')  	    // const fill mat entries
					M->elem[offset * n + j * n + k + offset] = c;
				else if (T == 'R') 	// random fill mat entries
					M->elem[offset * n + j * n + k + offset] = randu();
		offset += M->blkSize[i];
		M->nElems += M->blkSize[i] * M->blkSize[i];
	}
	// set description
	sprintf(M->desc, "Random mqdb:  mat. size = %d, num. blocks = %d, blk sizes: ", n, k);
}

void fillBlocks_UNIMEM(mqdb* M, uint n, uint k, char T, float c) {
	//mat size n*n	
	CHECK(cudaMallocManaged(&(M->elem), n * n * sizeof(float)));
	CHECK(cudaMemset(M->elem, 0, n * n * sizeof(float)));
	M->nElems = 0;
	int offset = 0;
	// loop on blocks
	for (int i = 0; i < k; i++) {
		for (int j = 0; j < M->blkSize[i]; j++)
			for (int k = 0; k < M->blkSize[i]; k++)
				if (T == 'C')  	    // const fill mat entries
					M->elem[offset * n + j * n + k + offset] = c;
				else if (T == 'R') 	// random fill mat entries
					M->elem[offset * n + j * n + k + offset] = randu();
		offset += M->blkSize[i];
		M->nElems += M->blkSize[i] * M->blkSize[i];
	}
	// set description
	sprintf(M->desc, "Random mqdb:  mat. size = %d, num. blocks = %d, blk sizes: ", n, k);
}

/**
 * rand_gen_mqdb: mqdb  type returned
 *                n     square matrix size
 *                k     number of blocks
 *                seed  seed for random generator
 */
mqdb genRandMat(unsigned n, unsigned k, unsigned seed) {
	mqdb M;
	srand(seed);
	genRandDims(&M, n, k);
	M.nBlocks = k;

	// random fill mat entries
	fillBlocks(&M, n, k, 'R', 0.0);

	return M;
}

/**
 * rand_gen_mqdb: mqdb  type returned
 *                n     square matrix size
 *                k     number of blocks
 *                seed  seed for random generator
 */
mqdb genRandMat_UNIMEM(unsigned n, unsigned k, unsigned seed) {
	mqdb M;
	srand(seed);
	genRandDims_UNIMEM(&M, n, k);
	M.nBlocks = k;

	// random fill mat entries
	fillBlocks_UNIMEM(&M, n, k, 'R', 0.0);

	return M;
}

/**
 * const_mqdb: mqdb  is the type returned
 *                n     is the square matrix size
 *                k     is the number of blocks
 *                seed  is the seed for random generator
 *                c   	is the constant value assigned
 */
mqdb mqdbConst(uint n, uint k, uint seed, float c) {
	mqdb M;
	srand(seed);
	genRandDims(&M, n, k);
	M.nBlocks = k;

	// fill mat entries with a constant
	fillBlocks(&M, n, k, 'C', c);

	return M;
}

/**
 * const_mqdb: mqdb  is the type returned
 *                n     is the square matrix size
 *                k     is the number of blocks
 *                seed  is the seed for random generator
 *                c   	is the constant value assigned
 */
mqdb mqdbConst_UNIMEM(uint n, uint k, uint seed, float c) {
	mqdb M;
	srand(seed);
	genRandDims_UNIMEM(&M, n, k);
	M.nBlocks = k;

	// fill mat entries with a constant
	fillBlocks_UNIMEM(&M, n, k, 'C', c);

	return M;
}

/*
 * product between mqdb matrices restricted to blocks
 */
void mqdbProd(mqdb A, mqdb B, mqdb C) {
	uint n = 0;
	for (uint i = 0; i < A.nBlocks; i++)
		n += A.blkSize[i];                  // mat dim
	int k = A.nBlocks;                      // num blks
	int dl = 0;                             // blk left bound
	int dr = 0;                             // blk left bound
	for (uint i = 0; i < k; i++) {           // loop on blks
		dr += A.blkSize[i];                 // blk right bound
		for (uint r = dl; r < dr; r++) {     // scan block rows
			for (uint c = dl; c < dr; c++) { // scan block cols
				float s = 0;
				for (uint l = dl; l < dr; l++)
					s += A.elem[r * n + l] * B.elem[c + l * n];
				C.elem[r * n + c] = s;
			}
		}
		dl = dr;
	}
}

/*
 * product 2 between mqdb matrices restricted to blocks
 */
mqdb* mqdbProd2(mqdb A, mqdb B, int N) {
	mqdb* C = (mqdb*)malloc(sizeof(mqdb));
	*C = mqdbConst(N, A.nBlocks, NULL, 0.0);
	memcpy(C->blkSize, A.blkSize, A.nBlocks * sizeof(int));
	uint n = 0;
	for (uint i = 0; i < A.nBlocks; i++)
		n += A.blkSize[i];                  // mat dim
	int k = A.nBlocks;                      // num blks
	int dl = 0;                             // blk left bound
	int dr = 0;                             // blk left bound
	for (uint i = 0; i < k; i++) {           // loop on blks
		dr += A.blkSize[i];                 // blk right bound
		for (uint r = dl; r < dr; r++) {     // scan block rows
			for (uint c = dl; c < dr; c++) { // scan block cols
				float s = 0;
				for (uint l = dl; l < dr; l++)
					s += A.elem[r * n + l] * B.elem[c + l * n];
				C->elem[r * n + c] = s;
			}
		}
		dl = dr;
	}
	return C;
}

/*
 * standard (naive) matrix product on host
 */
void matProd(mqdb A, mqdb B, mqdb C) {
	int n = 0;
	for (uint i = 0; i < A.nBlocks; i++)
		n += A.blkSize[i];

	for (uint r = 0; r < n; r++)
		for (uint c = 0; c < n; c++) {
			double sum = 0;
			for (uint l = 0; l < n; l++) {
				double a = A.elem[r * n + l];
				double b = B.elem[l * n + c];
				sum += a * b;
			}
			C.elem[r * n + c] = (float)sum;
		}
}

/*
 * standard (naive) matrix product on host
 */
mqdb* matProd2(mqdb A, mqdb B, int N) {
	mqdb* C = (mqdb*)malloc(sizeof(mqdb));
	*C = mqdbConst(N, A.nBlocks, NULL, 0.0);
	memcpy(C->blkSize, A.blkSize, A.nBlocks * sizeof(int));
	int n = 0;
	for (uint i = 0; i < A.nBlocks; i++)
		n += A.blkSize[i];

	for (uint r = 0; r < n; r++)
		for (uint c = 0; c < n; c++) {
			double sum = 0;
			for (uint l = 0; l < n; l++) {
				double a = A.elem[r * n + l];
				double b = B.elem[l * n + c];
				sum += a * b;
			}
			C->elem[r * n + c] = (float)sum;
		}
	return C;
}

/*
 * elementwise comparison between two mqdb
 */
void checkResult(mqdb A, mqdb B) {
	double epsilon = 1.0E-8;
	bool match = 1;
	int n = 0;
	for (int i = 0; i < A.nBlocks; i++)
		n += A.blkSize[i];
	for (int i = 0; i < n * n; i++) {
		if (abs(A.elem[i] - B.elem[i]) > epsilon) {
			match = 0;
			printf("   * Arrays do not match!\n");
			printf("     gpu: %2.2f,  host: %2.2f at current %d\n", A.elem[i],
				B.elem[i], i);
			exit(1);
		}
	}
	if (match)
		printf("   Arrays match\n");
}

/*
 * print mqdb
 */
void mqdbDisplay(mqdb M) {
	int n = 0;
	printf("%s", M.desc);
	for (int j = 0; j < M.nBlocks; j++) {
		printf("%d  ", M.blkSize[j]);
		n += M.blkSize[j];
	}
	printf("\n");
	for (int j = 0; j < n * n; j++) {
		if (M.elem[j] == 0)
			printf("------");
		else
			printf("%5.2f ", M.elem[j]);
		if ((j + 1) % n == 0)
			printf("\n");
	}
	printf("\n");
}