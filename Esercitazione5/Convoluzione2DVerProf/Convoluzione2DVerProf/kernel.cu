#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <assert.h>
#include "../../../Lib/common.h"

#define KERNEL_RADIUS 5
#define MASK_WIDTH (2 * KERNEL_RADIUS + 1)
#define DATA_WIDTH 32
#define TILE_WIDTH 16

__constant__ float M_dev[MASK_WIDTH][MASK_WIDTH];
/*
 * kernel for convolution 2D
 */
__global__ void conv2D(float* A, float* B) {

	const int TILE_SIZE = TILE_WIDTH + MASK_WIDTH - 1;

	__shared__ float A_s[TILE_SIZE][TILE_SIZE];

	// SHARED MEMORY BLOCK
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;

	int n = KERNEL_RADIUS;
	int lx = blockIdx.x * blockDim.x;
	int rx = (blockIdx.x + 1) * blockDim.x;
	int ly = blockIdx.y * blockDim.y;
	int ry = (blockIdx.y + 1) * blockDim.y;


	if ((x - n < lx) || (y - n < ly)) {
		A_s[threadIdx.y][threadIdx.x] = ((x - n + lx) < 0 || (y - n + ly) < 0) ? 0 : A[y * DATA_WIDTH + x - n];
		//if (blockIdx.x ==1 && blockIdx.y==1)
			//printf("A_s[%d][%d] = %f -- (y,x)=(%d,%d) --  (ly,lx)=(%d,%d)\n",threadIdx.x,threadIdx.y,A_s[threadIdx.y][threadIdx.x],y,x,ly,lx);
	}
	if (x + n >= rx || y + n >= ry) {
		A_s[n + threadIdx.y][n + threadIdx.x] = (x + n > DATA_WIDTH || y + n > DATA_WIDTH) ? 0 : A[y * DATA_WIDTH + x + n];
		//if (blockIdx.x == 0 && blockIdx.y == 0)
		//	printf("A_s[%d][%d] = %f -- (y,x)=(%d,%d) -- (ly,lx)=(%d,%d)\n", n + threadIdx.x, n + threadIdx.y, A_s[n + threadIdx.y][n + threadIdx.x], y, x, ly, lx);
	}


	A_s[n + threadIdx.y][n + threadIdx.x] = A[y * DATA_WIDTH + x];
	// END SHARED MEMORY BLOCK

	__syncthreads();

	float Pvalue = 0;
	for (int i = 0; i < MASK_WIDTH; i++)
		for (int j = 0; j < MASK_WIDTH; j++)
			Pvalue += A_s[threadIdx.y + i][threadIdx.x + j] * M_dev[i][j];
	B[x * blockDim.x + y] = Pvalue;
}

/*
 * Average filter
 */
void Avg_mask(float* mask) {
	for (int i = 0; i < MASK_WIDTH * MASK_WIDTH; i++)
		mask[i] = (float)1 / (MASK_WIDTH * MASK_WIDTH);
}

/*
 * main
 */
int main(void) {

	float* M;
	float* a, * b, * a_dev, * b_dev;
	const int datasize = DATA_WIDTH * DATA_WIDTH * sizeof(float);
	const int masksize = MASK_WIDTH * MASK_WIDTH * sizeof(float);

	printf("Initializing data...\n");
	M = (float*)malloc(masksize);
	a = (float*)malloc(datasize);
	b = (float*)malloc(datasize);

	// initialize data
	for (int i = 0; i < DATA_WIDTH; i++)
		for (int j = 0; j < DATA_WIDTH; j++)
			a[i * DATA_WIDTH + j] = j + 1;

	// print data
	printf("Print matrix a...\n");
	for (int i = 0; i < DATA_WIDTH; i++) {
		for (int j = 0; j < DATA_WIDTH; j++)
			printf("%3.0f", a[i * DATA_WIDTH + j]);
		printf("\n");
	}

	// mask used
	Avg_mask(M);

	// cuda allocation & computation
	cudaMemcpyToSymbol(M_dev, M, masksize);
	cudaMalloc((void**)&a_dev, datasize);
	cudaMalloc((void**)&b_dev, datasize);
	cudaMemcpy(a_dev, a, datasize, cudaMemcpyHostToDevice);
	dim3 blocks(TILE_WIDTH, TILE_WIDTH);
	dim3 grids(DATA_WIDTH / TILE_WIDTH, DATA_WIDTH / TILE_WIDTH);
	conv2D << <grids, blocks >> > (a_dev, b_dev);
	cudaMemcpy(b, b_dev, datasize, cudaMemcpyDeviceToHost);

	// print data
	printf("Print results...\n");
	for (int i = 0; i < DATA_WIDTH; i++) {
		for (int j = 0; j < DATA_WIDTH; j++)
			printf("%3.0f", b[i * DATA_WIDTH + j]);
		printf("\n");
	}

	cudaFree(a_dev);
	cudaFree(b_dev);
	free(a);
	free(b);
	free(M);
	return 0;
}