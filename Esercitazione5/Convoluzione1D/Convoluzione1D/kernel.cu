#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <assert.h>
#include "../../../Lib/common.h"

#define MASK_RADIUS  25
#define MASK_SIZE    (2 * MASK_RADIUS + 1)
#define TILE         128
#define TILE_SIZE    (TILE + MASK_SIZE - 1)
#define DATA_SIZE    (1 << 24)

__device__ __constant__ float d_mask[MASK_SIZE];

void checkResult(float* d_result, float* h_result) {
	double epsilon = 1.0E-8;
	for (int i = 0; i < DATA_SIZE; i++) {
		if (ABS(h_result[i], d_result[i]) > epsilon) {
			printf("Error at element %d --> host: %f - device: %f\n", i, h_result[i], d_result[i]);
			return;
		}
	}
	printf("OK\n");
}

void printData(float* a) {
	printf("\n");
	for (int i = 0; i < DATA_SIZE; i++) {
		printf("%.2f ", a[i]);
	}
	printf("\n");
	return;
}

void initData(float* data) {
	// initialize the data
	for (int i = 0; i < DATA_SIZE; i++) {
		data[i] = i;
	}
}

void initAvgMask(float* mask) {
	// initialize mask moving average
	for (int i = 0; i < MASK_SIZE; i++) {
		mask[i] = 1.0 / MASK_SIZE;
	}
	return;
}

void convCPU(const float * data, float* result, const float * mask) {
	for (int i = 0; i < DATA_SIZE; ++i) {
		float sum = 0.0f;
		for (int h = i - MASK_RADIUS, m = 0; m < MASK_SIZE; ++m, ++h) {
			if (h >= 0 && h < DATA_SIZE) {
				sum += data[h] * mask[m];
			}
		}
		result[i] = sum;
	}
}

__global__ void convGPU(const float* data, float* result) {
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i >= DATA_SIZE) {
		return;
	}
	float sum = 0.0f;
	for (int h = i - MASK_RADIUS, m = 0; m < MASK_SIZE; ++m, ++h) {
		sum += (h >= 0 && h < DATA_SIZE) ? data[h] * d_mask[m] : 0.0f;
	}
	result[i] = sum;	
}

__global__ void convGPU_SMEM(const float* data, float* result) {
	int idx = threadIdx.x + blockIdx.x * blockDim.x;	

	int i = threadIdx.x;

	__shared__ float tile[TILE_SIZE];

	if (i < MASK_RADIUS) {
		tile[i] = (idx - MASK_RADIUS >= 0) ? data[idx - MASK_RADIUS] : 0.0f;
	}
	if (i >= TILE - MASK_RADIUS) {
		tile[i + 2 * MASK_RADIUS] = (idx + MASK_RADIUS < DATA_SIZE) ? data[idx + MASK_RADIUS] : 0.0f;
	}

	tile[i + MASK_RADIUS] = (idx < DATA_SIZE) ? data[idx] : 0.0f;

	__syncthreads();
	
	float sum = 0.0f;
	for (int h = i, m = 0; m < MASK_SIZE; ++m, ++h) {
		sum += tile[h] * d_mask[m];		
	}

	if (idx < DATA_SIZE) {
		result[idx] = sum;
	}
}

int main() {

	assert(TILE >= MASK_RADIUS, "TILE must be greater or equal to MASK_RADIUS\n");
	/*Setto il device*/
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	printf("Starting conv-1D at device %d: %s", dev, deviceProp.name);
	CHECK(cudaSetDevice(dev));

	size_t nBytes = DATA_SIZE * sizeof(float);
	size_t nBytes_mask = MASK_SIZE * sizeof(float);

	// set up array size	
	printf(" with array of %d byte (%d elements)\n\n", nBytes, DATA_SIZE);

	float* data, * result, *result_gpu, * mask;
	data = (float*)malloc(nBytes);
	result = (float*)malloc(nBytes);
	result_gpu = (float*)malloc(nBytes);
	mask = (float*)malloc(nBytes_mask);

	initData(data);
	initAvgMask(mask);
	memset(result, 0.0f, nBytes);
	memset(result_gpu, 0.0f, nBytes);

	/************************************************** CPU ******************************************************/
	double start = seconds();
	convCPU(data, result, mask);
	printf("CPU time: \t\t %.2f sec\n", (seconds() - start));

	/************************************************** GPU no SMEM ******************************************************/

	float* d_data, * d_result;
	CHECK(cudaMalloc(&d_data, nBytes));
	CHECK(cudaMalloc(&d_result, nBytes));
	CHECK(cudaMemcpy(d_data, data, nBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpyToSymbol(d_mask, mask, nBytes_mask));
	CHECK(cudaMemset(d_result, 0.0f, nBytes));

	dim3 block(1024);
	dim3 grid(CEIL(DATA_SIZE, block.x));

	start = seconds();
	convGPU<<< grid, block>>>(d_data, d_result);
	CHECK(cudaDeviceSynchronize());
	printf("GPU no SMEM time: \t %.2f sec --> ", (seconds() - start));

	CHECK(cudaMemcpy(result_gpu, d_result, nBytes, cudaMemcpyDeviceToHost));
	checkResult(result_gpu, result);

	/************************************************** GPU SMEM ******************************************************/
	CHECK(cudaMemset(d_result, 0.0f, nBytes));

	block = dim3(TILE);
	grid = dim3(CEIL(DATA_SIZE, block.x));	

	start = seconds();
	convGPU_SMEM << < grid, block >> > (d_data, d_result);
	CHECK(cudaDeviceSynchronize());
	printf("GPU SMEM time: \t\t %.2f sec --> ", (seconds() - start));

	CHECK(cudaMemcpy(result_gpu, d_result, nBytes, cudaMemcpyDeviceToHost));
	checkResult(result_gpu, result);

	/*printData(result);
	printData(result_gpu);*/

	/*fine*/
	CHECK(cudaFree(d_result));
	CHECK(cudaFree(d_data));
	free(data);
	free(mask);
	free(result);
	free(result_gpu);
	CHECK(cudaDeviceReset());

	return EXIT_SUCCESS;
}
