#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <assert.h>
#include "../../../Lib/common.h"

#define MASK_RADIUS   5
#define MASK_WIDTH    (2 * MASK_RADIUS + 1)
#define DATA_WIDTH    (1 << 13)
#define TILE	      8
#define TILE_WIDTH    (TILE + MASK_WIDTH - 1)
#define BORDER(index) (index >= 0 && index < DATA_WIDTH) 

__constant__ float d_mask[MASK_WIDTH][MASK_WIDTH];

__global__ void conv2D_GPUSMEM(const float* data, float* result) {
	uint row = threadIdx.y + blockDim.y * blockIdx.y;
	uint col = threadIdx.x + blockDim.x * blockIdx.x;

	__shared__ float tile[TILE_WIDTH][TILE_WIDTH];

	uint i = threadIdx.y;
	uint j = threadIdx.x;
	uint indexR;
	uint indexC;

	//righe
	if (i < MASK_RADIUS) {
		indexR = (row - MASK_RADIUS);
		tile[i][j + MASK_RADIUS] = BORDER(indexR) ? data[indexR* DATA_WIDTH + col] : 0.0f;
	}
	if (i >= TILE - MASK_RADIUS) {
		indexR = (row + MASK_RADIUS);				
		tile[i + 2 * MASK_RADIUS][j + MASK_RADIUS] = BORDER(indexR) ? data[indexR * DATA_WIDTH + col] : 0.0f;
	}
	//colonne
	if (j < MASK_RADIUS) {
		indexC = (col - MASK_RADIUS);
		tile[i + MASK_RADIUS][j] = BORDER(indexC) ? data[row * DATA_WIDTH + indexC] : 0.0f;
	}
	if (j >= TILE - MASK_RADIUS) {
		indexC = (col + MASK_RADIUS);
		tile[i + MASK_RADIUS][j + 2 * MASK_RADIUS] = BORDER(indexC) ? data[row * DATA_WIDTH + indexC] : 0.0f;
	}
	//angoli
	if (i < MASK_RADIUS && j < MASK_RADIUS) {
		indexR = (row - MASK_RADIUS);
		indexC =(col - MASK_RADIUS);
		tile[i][j] = (BORDER(indexR) && BORDER(indexC)) ? data[indexR * DATA_WIDTH + indexC] : 0.0f;
	}
	if (i >= TILE - MASK_RADIUS && j < MASK_RADIUS) {
		indexR = (row + MASK_RADIUS) ;
		indexC = (col - MASK_RADIUS);
		tile[i + 2 * MASK_RADIUS][j] = (BORDER(indexR) && BORDER(indexC)) ? data[indexR* DATA_WIDTH + indexC] : 0.0f;
	}
	if (i < MASK_RADIUS && j >= TILE - MASK_RADIUS) {
		indexR = (row - MASK_RADIUS);
		indexC = (col + MASK_RADIUS);
		tile[i][j + 2 * MASK_RADIUS] = (BORDER(indexR) && BORDER(indexC)) ? data[indexR * DATA_WIDTH + indexC] : 0.0f;
	}
	if (i >= TILE - MASK_RADIUS && j >= TILE - MASK_RADIUS) {
		indexR = (row + MASK_RADIUS) ;
		indexC = (col + MASK_RADIUS);
		tile[i + 2 * MASK_RADIUS][j + 2 * MASK_RADIUS] = (BORDER(indexR) && BORDER(indexC)) ? data[indexR* DATA_WIDTH + indexC] : 0.0f;
	}

	tile[i + MASK_RADIUS][j + MASK_RADIUS] = (row < DATA_WIDTH&& col< DATA_WIDTH) ? data[row * DATA_WIDTH + col] : 0.0f;
	
	__syncthreads();

	/*if (row == 8 && col == 0) {
		for(int o = 0 ; o < TILE_WIDTH; ++o) {
			for (int k = 0; k < TILE_WIDTH; ++k) {
				printf("%.2f ", tile[o][k]);
			}
			printf("\n");
		}

	}*/

	float sum = 0.0f;
	for (int h = i , m = 0; m < MASK_WIDTH; ++h, ++m) {
		for (int k = j , n = 0; n < MASK_WIDTH; ++k, ++n) {
			sum += tile[h][k] * d_mask[m][n];			
		}
	}
	if (row < DATA_WIDTH && col < DATA_WIDTH) {
		result[row * DATA_WIDTH + col] = sum;
	}
}

void initAvgMask(float* mask) {
	for (int i = 0; i < MASK_WIDTH * MASK_WIDTH; i++) {
		mask[i] = (float)1 / (MASK_WIDTH * MASK_WIDTH);
	}
}

void initData(float* data) {
	for (int i = 0; i < DATA_WIDTH; i++) {
		for (int j = 0; j < DATA_WIDTH; j++) {
			data[i * DATA_WIDTH + j] = j + 1;
		}
	}
}

void printData(const float* data) {
	printf("Print matrix...\n");
	for (int i = 0; i < DATA_WIDTH; i++) {
		for (int j = 0; j < DATA_WIDTH; j++) {
			printf("% 6.2f ", data[i * DATA_WIDTH + j]);
		}
		printf("\n");
	}
}

void checkResult(float* d_result, float* h_result) {
	double epsilon = 1.0E-8;//1.0E-2
	for (int i = 0; i < DATA_WIDTH * DATA_WIDTH; ++i) {
		if (ABS(h_result[i], d_result[i]) > epsilon) {
			printf("Error at element %d --> host: %f - device: %f\n", i, h_result[i], d_result[i]);
			return;
		}
	}
	printf("OK\n");
}

void conv2D_CPU(const float* data, float* result, const float* mask) {
	for (int i = 0; i < DATA_WIDTH; i++) {
		for (int j = 0; j < DATA_WIDTH; j++) {
			float sum = 0.0f;
			for (int h = i - MASK_RADIUS, m = 0; m < MASK_WIDTH; ++h, ++m) {
				for (int k = j - MASK_RADIUS, n = 0; n < MASK_WIDTH; ++k, ++n) {
					if (h >= 0 && h < DATA_WIDTH && k >= 0 && k < DATA_WIDTH) {
						sum += data[h * DATA_WIDTH + k] * mask[m * MASK_WIDTH + n];
					}
				}
			}
			result[i * DATA_WIDTH + j] = sum;
		}
	}
}

__global__ void conv2D_GPU(const float* data, float* result) {	
	uint i = threadIdx.y + blockDim.y * blockIdx.y;
	uint j = threadIdx.x + blockDim.x * blockIdx.x;
	
	if (i >= DATA_WIDTH || j >= DATA_WIDTH) {
		return;
	}

	float sum = 0.0f;
	for (int h = i - MASK_RADIUS, m = 0; m < MASK_WIDTH; ++h, ++m) {
		for (int k = j - MASK_RADIUS, n = 0; n < MASK_WIDTH; ++k, ++n) {
			sum += (h >= 0 && h < DATA_WIDTH && k >= 0 && k < DATA_WIDTH)?data[h * DATA_WIDTH + k] * d_mask[m][n] : 0.0f;			
		}
	}
	
	result[i * DATA_WIDTH + j] = sum;
}

int main(void) {

	assert(TILE >= MASK_RADIUS, "TILE must be greater or equal to MASK_RADIUS\n");

	/*Setto il device*/
	int dev = 0;
	cudaDeviceProp deviceProp;
	CHECK(cudaGetDeviceProperties(&deviceProp, dev));
	printf("Starting conv-2D at device %d: %s", dev, deviceProp.name);
	CHECK(cudaSetDevice(dev));

	const int data_bytesize = DATA_WIDTH * DATA_WIDTH * sizeof(float);
	const int mask_bytesize = MASK_WIDTH * MASK_WIDTH * sizeof(float);

	float* mask, * data, * result, * result_gpu;
	printf(" with matrix of %d bytes (%d elements) and mask of %d bytes (%d elements) \n\n", data_bytesize, DATA_WIDTH * DATA_WIDTH, mask_bytesize, MASK_WIDTH * MASK_WIDTH);

	printf("Initializing data...\n\n");
	mask = (float*)malloc(mask_bytesize);
	data = (float*)malloc(data_bytesize);
	result = (float*)malloc(data_bytesize);
	result_gpu = (float*)malloc(data_bytesize);

	// initialize data
	initData(data);
	initAvgMask(mask);
	memset(result, 0.0f, data_bytesize);
	memset(result_gpu, 0.0f, data_bytesize);

	/******************CPU************************/
	double start = seconds();
	conv2D_CPU(data, result, mask);
	printf("CPU time \t\t %.2f sec\n", (seconds() - start));

	/************GPU data init*************/
	float* d_data, * d_result;
	CHECK(cudaMalloc(&d_data, data_bytesize));
	CHECK(cudaMalloc(&d_result, data_bytesize));

	CHECK(cudaMemcpy(d_data, data, data_bytesize, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpyToSymbol(d_mask, mask, mask_bytesize));
	CHECK(cudaMemset(d_result, 0.0f, data_bytesize));

	/******************GPU no SMEM*************/
	dim3 block(32, 32);
	dim3 grid(CEIL(DATA_WIDTH, block.x), CEIL(DATA_WIDTH, block.y));

	start = seconds();
	conv2D_GPU << < grid, block >> > (d_data, d_result);
	CHECK(cudaDeviceSynchronize());
	printf("GPU no SMEM time: \t %.2f sec --> ", (seconds() - start));

	CHECK(cudaMemcpy(result_gpu, d_result, data_bytesize, cudaMemcpyDeviceToHost));
	checkResult(result_gpu, result);
	
	/******************GPU SMEM*************/
	CHECK(cudaMemset(d_result, 0.0f, data_bytesize));

	block = dim3(TILE, TILE);
	grid = dim3(CEIL(DATA_WIDTH, block.x), CEIL(DATA_WIDTH, block.y));

	start = seconds();
	conv2D_GPUSMEM << <grid, block >> > (d_data, d_result);
	CHECK(cudaDeviceSynchronize());
	printf("GPU SMEM time: \t\t %.2f sec --> ", (seconds() - start));

	CHECK(cudaMemcpy(result_gpu, d_result, data_bytesize, cudaMemcpyDeviceToHost));
	checkResult(result_gpu, result);

	//printData(result);
	//printData(result_gpu);

	/**********fine*********************/
	cudaFree(d_data);
	cudaFree(d_result);
	free(data);
	free(result);
	free(result_gpu);
	free(mask);
	CHECK(cudaDeviceReset());
	
	return EXIT_SUCCESS;
}