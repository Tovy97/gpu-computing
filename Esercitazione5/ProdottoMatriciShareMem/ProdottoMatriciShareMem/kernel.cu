#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define N_RIG_A 1024			//N
#define N_COL_B 1024			//M
#define N_COL_A_RIG_B 1024		//P

#define DIM_A N_RIG_A * N_COL_A_RIG_B
#define DIM_B N_COL_A_RIG_B * N_COL_B
#define DIM_C N_RIG_A * N_COL_B

#define BLOCK_SIZE 32

void multCPU(float* A, float* B, float* C) {
	for (int i = 0; i < N_RIG_A; i++)
		for (int j = 0; j < N_COL_B; j++) {
			float sum = 0;
			for (int k = 0; k < N_COL_A_RIG_B; k++)
				sum += A[i * N_COL_A_RIG_B + k] * B[k * N_COL_B + j];
			C[i * N_COL_B + j] = sum;
		}
}

__global__ void mult(float* A, float* B, float* C) {
	int row = threadIdx.y + blockIdx.y * blockDim.y;
	int col = threadIdx.x + blockIdx.x * blockDim.x;

	if (row >= N_RIG_A || col >= N_COL_B) {
		return;
	}

	float ris = 0.0f;
	for (int k = 0; k < N_COL_A_RIG_B; ++k) {
		ris += A[row * N_COL_A_RIG_B + k] * B[k * N_COL_B + col];
	}
	C[row * N_COL_B + col] = ris;
}

__global__ void multSharedMem(float* A, float* B, float* C) {
	int row = threadIdx.y + blockIdx.y * blockDim.y;
	int col = threadIdx.x + blockIdx.x * blockDim.x;

	__shared__ float A_sm[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float B_sm[BLOCK_SIZE][BLOCK_SIZE];

	float ris = 0.0f;
	int nBlock = CEIL(N_COL_A_RIG_B, BLOCK_SIZE);

	for (int m = 0; m < nBlock; ++m) {
		int c = BLOCK_SIZE * m + threadIdx.x;
		int r = BLOCK_SIZE * m + threadIdx.y;

		A_sm[threadIdx.y][threadIdx.x] = (row < N_RIG_A && c < N_COL_A_RIG_B) ? A[row * N_COL_A_RIG_B + c] : 0;
		B_sm[threadIdx.y][threadIdx.x] = (r < N_COL_A_RIG_B && col < N_COL_B) ? B[r * N_COL_B + col] : 0;

		__syncthreads();

		int lim = (m + 1 != nBlock) ? BLOCK_SIZE : N_COL_A_RIG_B - m * BLOCK_SIZE;
		                                         //BLOCK_SIZE - (BLOCK_SIZE * nBlock - N_COL_A_RIG_B);

		for (int k = 0; k < lim; ++k) {
			ris += A_sm[threadIdx.y][k] * B_sm[k][threadIdx.x];
		}

		__syncthreads();
	}

	if (row < N_RIG_A && col < N_COL_B) {
		C[row * N_COL_B + col] = ris;
	}
}

__global__ void multSharedMemDyn(float* A, float* B, float* C, const uint SMEMsize) {
	uint row = blockIdx.y * blockDim.y + threadIdx.y;
	uint col = blockIdx.x * blockDim.x + threadIdx.x;

	int blocksize = blockDim.x;

	extern __shared__ float smem[];

	float* A_sm = smem;
	float* B_sm = smem + SMEMsize;

	float ris = 0.0f;
	int nBlock = CEIL(N_COL_A_RIG_B, blocksize);
	for (int m = 0; m < nBlock; ++m) {
		int c = blocksize * m + threadIdx.x;
		int r = blocksize * m + threadIdx.y;

		A_sm[threadIdx.y * blocksize + threadIdx.x] = (row < N_RIG_A&& c < N_COL_A_RIG_B) ? A[row * N_COL_A_RIG_B + c] : 0;
		B_sm[threadIdx.y * blocksize + threadIdx.x] = (r < N_COL_A_RIG_B&& col < N_COL_B) ? B[r * N_COL_B + col] : 0;

		__syncthreads();

		int lim = (m + 1 != nBlock) ? blocksize : N_COL_A_RIG_B - m * blocksize;

		for (int k = 0; k < lim; ++k) {
			ris += A_sm[threadIdx.y * blocksize + k] * B_sm[k * blocksize + threadIdx.x];
		}

		__syncthreads();
	}
	if (row < N_RIG_A && col < N_COL_B) {
		C[row * N_COL_B + col] = ris;
	}
}

unsigned long testCUDADevice(void) {
	int dev = 0;

	cudaDeviceSetCacheConfig(cudaFuncCachePreferEqual);
	cudaDeviceProp deviceProp;
	cudaSetDevice(dev);
	cudaGetDeviceProperties(&deviceProp, dev);
	printf("Device %d: \"%s\"\n", dev, deviceProp.name);
	printf("Total amount of shared memory available per block: %lu KB\n",
		deviceProp.sharedMemPerBlock / 1024);
	return deviceProp.sharedMemPerBlock;
}

void compare(float* C1, float* C2) {
	double epsilon = 1.0E-8;
	for (int i = 0; i < DIM_C; ++i) {
		if (ABS(C1[i], C2[i]) > epsilon) {			
			printf("ERROR at element %d\n", i);
		}
	}
	printf("OK!\n");	
}

int main() {
	/*Calcolo la shared mem necessaria*/
	uint nByteSMEM = 2 * BLOCK_SIZE * BLOCK_SIZE * sizeof(float);
	
	/*Controllo la  la shared mem nel dispositivo*/
	ulong maxSMEMbytes = testCUDADevice();
	if (maxSMEMbytes < nByteSMEM) {
		printf("Shared memory usage WARNING: available: %lu, required: %d bytes\n", maxSMEMbytes, nByteSMEM);
	}
	else {
		printf("Total amount of shared memory required per block %.1f KB\n", (float)nByteSMEM / (float)1024);
	}

	/*Creo le matrici nell'host*/
	float* A, * B, *C_CPU, * C_GPU, * C_sm_stat, * C_sm_dyn;
	A = (float*)malloc(sizeof(float) * DIM_A);
	B = (float*)malloc(sizeof(float) * DIM_B);
	C_CPU = (float*)malloc(sizeof(float) * DIM_C);
	C_GPU = (float*)malloc(sizeof(float) * DIM_C);
	C_sm_stat = (float*)malloc(sizeof(float) * DIM_C);
	C_sm_dyn = (float*)malloc(sizeof(float) * DIM_C);

	/*Inizialiazzo le matrici nell'host*/
	for (int i = 0; i < DIM_A; ++i) {
		A[i] = i;
	}
	for (int i = 0; i < DIM_B; ++i) {
		B[i] = i;
	}
	memset(C_CPU, 0.0f, DIM_C);
	memset(C_GPU, 0.0f, DIM_C);
	memset(C_sm_stat, 0.0f, DIM_C);
	memset(C_sm_dyn, 0.0f, DIM_C);

	/*Creo le matrici nel device*/
	float* d_A, * d_B, * d_C;
	CHECK(cudaMalloc(&d_A, sizeof(float) * DIM_A));
	CHECK(cudaMalloc(&d_B, sizeof(float) * DIM_B));
	CHECK(cudaMalloc(&d_C, sizeof(float) * DIM_C));

	/*Inizialiazzo le matrici nel device*/
	CHECK(cudaMemcpy(d_A, A, sizeof(float) * DIM_A, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpy(d_B, B, sizeof(float) * DIM_B, cudaMemcpyHostToDevice));
	CHECK(cudaMemset(d_C, 0.0f, sizeof(float) * DIM_C));

	/*******************CPU*********************/
	double start = seconds();
	multCPU(A, B, C_CPU);
	printf("\nTime CPU: \t\t\t\t %.2f sec\n", (seconds() - start));

	/************************GPU**********************************/
	/*Calcolo block e grid*/
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid(CEIL(N_COL_B, block.x), CEIL(N_RIG_A, block.y));

	/*Faccio partire la versione "classica" del prodotto riga colonna sul device*/
	start = seconds();
	mult << <grid, block >> > (d_A, d_B, d_C);
	CHECK(cudaDeviceSynchronize());
	printf("Time GPU no SMEM: \t\t\t %.2f sec --> ", (seconds() - start));

	/*Copio la matrice risultato sull'host e controllo il risultato*/
	CHECK(cudaMemcpy(C_GPU, d_C, sizeof(float) * DIM_C, cudaMemcpyDeviceToHost));
	compare(C_CPU, C_GPU);

	/************************SHARED MEMORY STATICA**********************************/
	/*Resetto la matrice risultato sul device*/
	CHECK(cudaMemset(d_C, 0.0f, sizeof(float) * DIM_C));

	/*Faccio partire la versione con shared memory del prodotto riga colonna sul device*/
	start = seconds();
	multSharedMem << <grid, block >> > (d_A, d_B, d_C);
	CHECK(cudaDeviceSynchronize());	
	printf("Time GPU with static SMEM: \t\t %.2f sec --> ", (seconds() - start));

	/*Copio la matrice risultato sull'host e controllo il risultato*/
	CHECK(cudaMemcpy(C_sm_stat, d_C, sizeof(float) * DIM_C, cudaMemcpyDeviceToHost));
	compare(C_CPU, C_sm_stat);

	/************************SHARED MEMORY DINAMICA**********************************/
	/*Setto la dimensione di L1 e SMEM*/
	CHECK(cudaDeviceSetCacheConfig(cudaFuncCachePreferShared));
	/*Dimensioni del block*/
	uint sizes[] = { 8,16,32 };
	for (int i = 0; i < 3; i++) {
		/*Resetto la matrice risultato sul device*/
		CHECK(cudaMemset(d_C, 0.0f, sizeof(float) * DIM_C));

		/*Setto block e grid*/
		block.x = sizes[i];
		block.y = sizes[i];
		grid.x = CEIL(N_COL_B, block.x);
		grid.y = CEIL(N_RIG_A, block.y);

		/*Calcolo la dimensioni della SMEM*/
		uint SMEMsize = sizes[i] * sizes[i];
		uint SMEMbyte = 2 * SMEMsize * sizeof(float);
		
		/*Faccio partire la versione con shared memory del prodotto riga colonna sul device*/
		start = seconds();
		multSharedMemDyn << <grid, block, SMEMbyte >> > (d_A, d_B, d_C, SMEMsize);
		CHECK(cudaDeviceSynchronize());
		printf("Time GPU with dynamic SMEM size %d: \t %.2f sec --> ", sizes[i], (seconds() - start));

		/*Copio la matrice risultato sull'host e controllo il risultato*/
		CHECK(cudaMemcpy(C_sm_dyn, d_C, sizeof(float) * DIM_C, cudaMemcpyDeviceToHost));
		compare(C_CPU, C_sm_dyn);
	}

	return 0;
}
