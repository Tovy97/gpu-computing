#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cublas_v2.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../../Lib/common.h"

int main() {
	int rowsA = 30;
	int colsA = 30;
	float* A = (float*)malloc(rowsA * colsA * sizeof(float));

	for (int i = 0; i < rowsA * colsA; ++i) {
		A[i] = i;
	}

	for (int i = 0; i < rowsA; ++i) {
		for (int j = 0; j < colsA; ++j) {
			printf("%03.0f ", A[j * rowsA + i]);
		}
		printf("\n");
	}


	int minRowA = 10;
	int maxRowA = 30;
	int minColA = 20;
	int maxColA = 30;
	int rowsB = maxRowA - minRowA; // = 90
	int colsB = maxColA - minColA; // = 10
	// Allocate the device matrix
	float* dB = NULL;
	cudaMalloc(&dB, rowsB * colsB * sizeof(float));
	// pointers and sizes
	float* subA = A + (minRowA + minColA * rowsA);
	cublasSetMatrix(rowsB, colsB, sizeof(float), subA, rowsA, dB, rowsB);
	CHECK(cudaDeviceSynchronize());

	float* B = (float*)malloc(rowsB * colsB * sizeof(float));

	CHECK(cudaMemcpy(B, dB, rowsB * colsB * sizeof(float), cudaMemcpyDeviceToHost));

	printf("\n\n");

	for (int i = 0; i < rowsB; ++i) {
		for (int j = 0; j < colsB; ++j) {
			printf("%03.0f ", B[j * rowsB + i]);
		}
		printf("\n");
	}

	return 0;
}