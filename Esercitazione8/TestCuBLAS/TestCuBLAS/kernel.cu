#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "cublas_v2.h"
#include "../../../Lib/common.h"

#define IDX(i,j,n) (i*n+j)
#define BLOCK_SIZE 16
#define M (1<<4)
#define N (1<<4)
#define P (1<<4)

void generate_random_vector(int, float**);
void generate_random_dense_matrix(int, int, float**);

/*
 * Kernel for matrix product with static SMEM
 */
__global__ void matProdSMEM(float* A, float* B, float* C) {	
	// indexes
	uint row = blockIdx.y * blockDim.y + threadIdx.y;
	uint col = blockIdx.x * blockDim.x + threadIdx.x;	
	// target: compute the right sum for the given row and col
	float sum = 0.0;

	// static shared memory
	__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

	// loop over blocks from block row of matrix A
	// and block column of matrix B
	uint numBlocks = (P + BLOCK_SIZE - 1) / BLOCK_SIZE;
	for (uint m = 0; m < numBlocks; m++) {

		// copy block from matrix to shared memory
		uint r = m * BLOCK_SIZE + threadIdx.y;
		uint c = m * BLOCK_SIZE + threadIdx.x;
		As[threadIdx.y][threadIdx.x] = A[IDX(row, c, P)];
		Bs[threadIdx.y][threadIdx.x] = B[IDX(r, col, M)];

		//---------------------------------------------------------------
		__syncthreads();  //  BARRIER SYNC on SMEM loading

		uint K = BLOCK_SIZE;
		if (m == numBlocks - 1) K = P - m * BLOCK_SIZE; // tune last block

		// compute this part of row-column product
		for (uint k = 0; k < K; k++)
			sum += As[threadIdx.y][k] * Bs[k][threadIdx.x];

		//---------------------------------------------------------------
		__syncthreads();
	}

	// store computed element in matrix C
	if (row < N && col < M)
		C[row * M + col] = sum;
}

/*
 * Generate a vector of length N with random single-precision floating-point
 * values between 0 and 100.
 */
void generate_random_vector(int n, float** x) {
	int i;
	float* z = (float*)malloc(sizeof(float) * n);

	for (i = 0; i < n; i++) {
		z[i] = 1; //(float)(rand() / RAND_MAX) * 100.0;
	}
	*x = z;
}

/*
 * Generate a matrix with M rows and N columns in column-major order. The matrix
 * will be filled with random single-precision floating-point values between 0
 * and 100.
 */
void generate_random_dense_matrix(int m, int n, float** A) {
	float* a = (float*)malloc(sizeof(float) * m * n);

	// For each column
	for (int j = 0; j < n; j++) {
		// For each row
		for (int i = 0; i < m; i++) {
			double dr = (double)rand();
			a[j * m + i] = 1.0; //(float)(rand() / RAND_MAX) * 100.0;
		}
	}
	*A = a;
}

/*
 * confronto tra prodotti matriciali con kernel standard e cuBLAS
 */
int main(int argc, char** argv) {
	int i, n = N, m = M, p = P;
	float* A, * d_A;  // matrice M x N  (row M, col N)
	float* B, * d_B;  // matrice N x P  (row N, col P)
	float* C, * d_C;  // matrice M x P, C = A*B
	float* x, * d_x;
	float* y, * d_y;
	float beta;
	float alpha;
	float milliseconds = 0;
	cublasHandle_t handle;
	device_name();

	unsigned long Asize = n * m * sizeof(float);
	unsigned long Bsize = n * p * sizeof(float);
	unsigned long Csize = m * p * sizeof(float);
	unsigned long xsize = n * sizeof(float);
	unsigned long ysize = m * sizeof(float);
	printf("**  C(%d x %d) = A(%d x %d) * B(%d x %d)\n", m, p, m, n, n, p);

	// events to measure time
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// Generate inputs
	srand(9384);
	generate_random_vector(n, &x);
	generate_random_vector(n, &y);
	C = (float*)malloc(Csize);
	A = (float*)malloc(Asize);
	B = (float*)malloc(Bsize);

	for (i = 0; i < N * M; i++) {
		A[i] = 1; //(float)(rand() / RAND
	}
	for (i = 0; i < N * P; i++) {
		B[i] = 1; //(float)(rand() / RAND
	}


	// Allocate device memory
	CHECK(cudaMalloc((void**)&d_A, Asize));
	CHECK(cudaMalloc((void**)&d_B, Bsize));
	CHECK(cudaMalloc((void**)&d_C, Csize));
	CHECK(cudaMalloc((void**)&d_x, xsize));
	CHECK(cudaMalloc((void**)&d_y, ysize));

	/***********************************************************/
	/*              GPU matProdSMEM static SMEM               */
	/***********************************************************/
	printf("\n**  Prodotto matrice-matrice con kernel ad-hoc\n");

	CHECK(cudaMemcpy(d_A, A, Asize, cudaMemcpyHostToDevice));
	CHECK(cudaMemcpy(d_B, B, Bsize, cudaMemcpyHostToDevice));

	// grid block dims = shared mem dims = BLOCK_SIZE
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid((M + block.x - 1) / block.x, (P + block.y - 1) / block.y);	
	CHECK(cudaEventRecord(start));
	matProdSMEM << < grid, block >> > (d_A, d_B, d_C);	
	CHECK(cudaEventRecord(stop));
	CHECK(cudaEventSynchronize(stop));
	CHECK(cudaEventElapsedTime(&milliseconds, start, stop));
	printf("    elapsed time: %.5f (sec)\n", milliseconds / 1000.0);

	// copy the array 'C' back from the GPU to the CPU
	CHECK(cudaMemcpy(C, d_C, Csize, cudaMemcpyDeviceToHost));

	/***************************************************
	 *      Moltiplicazione matrix-vector CUBLAS       *
	 ***************************************************/
	printf("\n**  Prodotto matrice-vettore CUBLAS\n");
	alpha = 1.0f;
	beta = 1.0f;

	// Create the cuBLAS handle
	CHECK_CUBLAS(cublasCreate(&handle));
	int version;
	CHECK_CUBLAS(cublasGetVersion(handle, &version));
	printf("\nUsing CUBLAS Version: %d\n", version);

	// Transfer inputs to the device
	CHECK_CUBLAS(cublasSetMatrix(m, n, sizeof(float), A, m, d_A, m));
	CHECK_CUBLAS(cublasSetMatrix(n, p, sizeof(float), B, n, d_B, n));
	CHECK_CUBLAS(cublasSetMatrix(m, p, sizeof(float), C, m, d_C, m));
	CHECK_CUBLAS(cublasSetVector(n, sizeof(float), x, 1, d_x, 1));
	CHECK_CUBLAS(cublasSetVector(m, sizeof(float), y, 1, d_y, 1));

	cudaEventRecord(start);
	CHECK_CUBLAS(cublasSgemv(handle, CUBLAS_OP_N, m, n, &alpha, d_A, m, d_x, 1, &beta, d_y, 1));
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);

	cudaEventElapsedTime(&milliseconds, start, stop);
	printf("    elapsed time: %.5f (sec)\n", milliseconds / 1000);

	// Retrieve the output vector from the device
	CHECK_CUBLAS(cublasGetVector(m, sizeof(float), d_y, 1, y, 1));

	for (i = 0; i < 10; i++)
		printf("%2.2f\n", y[i]);
	printf("...\n");

	/**********************************************
	 *  Moltiplicazione matrix-matrix CUBLAS
	 **********************************************/
	printf("Prodotto matrice-matrice CUBLAS\n");
	alpha = 1.0f;
	beta = 0.0f;
	CHECK(cudaMemset(d_C, 0, m * p * sizeof(float)));
	CHECK(cudaEventRecord(start));
	CHECK_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, p, n, &alpha, d_A, m, d_B, n, &beta, d_C, m));
	CHECK(cudaEventRecord(stop));
	CHECK(cudaEventSynchronize(stop));
	CHECK(cudaEventElapsedTime(&milliseconds, start, stop));
	printf("    elapsed time: %.5f (sec)\n", milliseconds / 1000);

	// Retrieve the output vector from the device
	CHECK_CUBLAS(cublasGetMatrix(m, p, sizeof(float), d_C, m, C, m));

	for (i = 0; i < 10; i++)
		printf("%2.2f\n", C[i]);
	printf("...\n");

	// free memory
	cudaFree(d_A);
	cudaFree(d_B);
	cudaFree(d_C);
	cudaFree(d_x);
	cudaFree(d_y);
	CHECK_CUBLAS(cublasDestroy(handle));
	free(A);
	free(B);
	free(C);
	free(x);
	free(y);
	return EXIT_SUCCESS;
}