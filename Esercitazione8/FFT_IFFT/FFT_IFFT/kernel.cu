#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <cufft.h>
#include "../../../Lib/common.h"

#define UN_QUARTO 0.25
#define UN_MEZZO 0.5
#define TRE_QUARTI 0.75

void rect(const int N, cufftReal** out, const float durata) {
	float* result = (float*)malloc(sizeof(float) * N);
	for (int i = 0; i < N; ++i) {
		if (i < durata * N) {
			result[i] = 1.0;
		}
		else {
			result[i] = 0.0;
		}
	}
	*out = result;
}

void checkEqual(float* in, float* out, const int N) {
	bool err = false;
	for (int i = 0; i < N; i++) {
		if (in[i] != out[i]) {
			printf("ERRORE elemento %d --> in = %f, out = %f\n", i, in[i], out[i]);
			err = true;
		}
	}
	if (!err) {
		printf("OK\n\n");
	}
	else {
		printf("\nNO\n\n");
	}
}

int main(int argc, char** argv) {
	int N = 1 << 27;
	cufftHandle plan = 0;
	cufftReal* samples;
	cufftReal* samplesOut;
	cufftComplex* dComplexSamples;
	cufftReal* dSamples;

	for (int i = 0; i < 3; ++i) {
		float durata;
		switch (i) {
		case 0: durata = UN_QUARTO; break;
		case 1: durata = UN_MEZZO; break;
		case 2: durata = TRE_QUARTI; break;
		}
		// Input Generation
		rect(N, &samples, durata);

		// Allocate device memory
		CHECK(cudaMalloc((void**)&dComplexSamples, sizeof(cufftComplex) * N));
		CHECK(cudaMalloc((void**)&dSamples, sizeof(cufftReal) * N));

		// Transfer inputs into device memory
		CHECK(cudaMemcpy(dSamples, samples, sizeof(cufftReal) * N, cudaMemcpyHostToDevice));

		//Setup the cuFFT plan
		CHECK_CUFFT(cufftPlan1d(&plan, 1, CUFFT_R2C, 1));

		// Execute a real-to-complex 1D FFT
		CHECK_CUFFT(cufftExecR2C(plan, dSamples, dComplexSamples));

		//Destroy plan
		CHECK_CUFFT(cufftDestroy(plan));

		//Setup the cuFFT plan
		CHECK_CUFFT(cufftPlan1d(&plan, 1, CUFFT_C2R, 1));

		// Execute a complex-to-real 1D IFFT
		CHECK_CUFFT(cufftExecC2R(plan, dComplexSamples, dSamples));

		//Destroy plan
		CHECK_CUFFT(cufftDestroy(plan));

		//Allocate host memory for result
		samplesOut = (cufftReal*)malloc(sizeof(cufftReal) * N);

		// Retrieve the results into host memory
		CHECK(cudaMemcpy(samplesOut, dSamples, sizeof(cufftReal) * N, cudaMemcpyDeviceToHost));

		//check that input is equal to output
		checkEqual(samples, samplesOut, N);

		//free host memory
		free(samples);
		free(samplesOut);

		//free device memory
		CHECK(cudaFree(dComplexSamples));
		CHECK(cudaFree(dSamples));
	}

	return EXIT_SUCCESS;
}