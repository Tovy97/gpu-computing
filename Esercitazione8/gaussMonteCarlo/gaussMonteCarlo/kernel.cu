#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <curand_kernel.h>
#include "../../../Lib/common.h"

#define TRIALS_PER_THREAD 90000
#define BLOCKS  264
#define THREADS 264
#define PI 3.1415926535 // known value of pi
#define RIS 0.8185946141

float gauss_MC_cpu(long trials) {
	const float max = sqrt(1.0 / (2.0 * PI));
	int s = 0;
	for (long i = 0; i < trials; i++) {
		float u = ((rand() / (float)RAND_MAX) * 3) - 1;
		float v = (rand() / (float)RAND_MAX) * max;
		float value = max * exp(-(u * u) / 2);
		s += (v < value);
	}
	return ((3 * max) * s) / trials;
}

__global__ void gauss_MC(float* estimate, curandState* states) {
	unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x;
	int s = 0;
	const float max = sqrtf(1.0 / (2.0 * PI));
	curand_init(tid, 0, 0, &states[tid]);
	for (int i = 0; i < TRIALS_PER_THREAD; i++) {
		float u = (curand_uniform(&states[tid]) * 3) - 1;
		float v = curand_uniform(&states[tid]) * max;
		float value = max * expf(-(u * u) / 2);
		s += (v < value);
	}
	estimate[tid] = ((3 * max) * s) / TRIALS_PER_THREAD;
}

int main(int argc, char* argv[]) {

	float host[BLOCKS * THREADS];
	float* dev;

	// events to measure time
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	// CPU procedure
	double iStart = seconds();
	float gauss_cpu = gauss_MC_cpu(THREADS * BLOCKS * TRIALS_PER_THREAD);
	double iElaps = seconds() - iStart;
	printf("CPU elapsed time (rand Monte Carlo): %.5f (sec)\n", iElaps);
	printf("CPU estimate of Gauss = %f [error of %f]\n", gauss_cpu, ABS(gauss_cpu, RIS));
	
	// GPU procedure
	curandState* devStates;
	cudaMalloc((void**)&dev, BLOCKS * THREADS * sizeof(float));
	cudaMalloc((void**)&devStates, BLOCKS * THREADS * sizeof(curandState));
	cudaEventRecord(start);
	gauss_MC << <BLOCKS, THREADS >> > (dev, devStates);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaMemcpy(host, dev, BLOCKS * THREADS * sizeof(float), cudaMemcpyDeviceToHost);
	float gauss_gpu = 0.0;
	for (int i = 0; i < BLOCKS * THREADS; i++) {
		gauss_gpu += host[i];
	}
	gauss_gpu /= (BLOCKS * THREADS);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);
	printf("GPU elapsed time (curand Monte Carlo): %.5f (sec)\n", milliseconds / 1000);
	printf("GPU estimate of Gauss = %f [error of %f]\n", gauss_gpu, ABS(gauss_gpu, RIS));

	cudaFree(dev);
	cudaFree(devStates);
	return 0;
}