#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cublas_v2.h>
#include <stdio.h>
#include <stdlib.h>
#include "../../../Lib/common.h"
#include "../../../Lib/mqdb.c"

int main(){
	int n = 1<<13;
	int k = 10;
	// mqdb host matrices
	mqdb A, B, C, C1;

	// mqdb device matrices
	mqdb d_A, d_B, d_C;

	// fill in
	A = mqdbConst(n, k, 10, 1);
	B = mqdbConst(n, k, 10, 1);
	C = mqdbConst(n, k, 10, 1);
	C1 = mqdbConst(n, k, 10, 1);	

	ulong nBytes = n * n * sizeof(float);
	ulong kBytes = k * sizeof(uint);

	printf("Memory size required = %.1f (MB)\n", (float)nBytes / (1024.0 * 1024.0));

	// malloc and copy on device memory
	d_A.nBlocks = A.nBlocks;
	CHECK(cudaMalloc(&d_A.blkSize, kBytes));
	CHECK(cudaMemcpy(d_A.blkSize, A.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_A.elem, nBytes));
	CHECK(cudaMemcpy(d_A.elem, A.elem, nBytes, cudaMemcpyHostToDevice));
	d_B.nBlocks = B.nBlocks;
	CHECK(cudaMalloc((void**)&d_B.blkSize, kBytes));
	CHECK(cudaMemcpy(d_B.blkSize, B.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_B.elem, nBytes));
	CHECK(cudaMemcpy(d_B.elem, B.elem, nBytes, cudaMemcpyHostToDevice));
	d_C.nBlocks = C.nBlocks;
	CHECK(cudaMalloc((void**)&d_C.blkSize, kBytes));
	CHECK(cudaMemcpy(d_C.blkSize, C.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_C.elem, nBytes));
	CHECK(cudaMemset(d_C.elem, 0.0f, nBytes));

	/***********************************************************/
	/*                    CPU MQDB product                     */
	/***********************************************************/
	
	printf("CPU MQDB product...\n");
	double start = seconds();
	mqdbProd(A, B, C);
	double CPUTime = seconds() - start;
	printf("   CPU elapsed time:                    %.5f (sec)\n\n", CPUTime);

	/*CUBLAS matrix*/

	float milliseconds = 0;
	cudaEvent_t start1, stop1;
	cudaEventCreate(&start1);
	cudaEventCreate(&stop1);

	cublasHandle_t handle;
	CHECK_CUBLAS(cublasCreate(&handle));
	printf("Prodotto matrice-matrice CUBLAS\n");
	float alpha = 1.0f;
	float beta = 0.0f;
	
	CHECK(cudaEventRecord(start1));
	CHECK_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, n, n, n, &alpha, d_A.elem, n, d_B.elem, n, &beta, d_C.elem, n));
	CHECK(cudaEventRecord(stop1));
	CHECK(cudaEventSynchronize(stop1));
	CHECK(cudaEventElapsedTime(&milliseconds, start1, stop1));
	printf("    elapsed time: %.5f (sec)\n", milliseconds / 1000);

	// Retrieve the output vector from the device
	CHECK_CUBLAS(cublasGetMatrix(n, n, sizeof(float), d_C.elem, n, C1.elem, n));

	checkResult(C, C1);


	/*CUBLAS blocks*/
	CHECK(cudaMemset(d_C.elem, 0.0f, nBytes));
	memset(C1.elem, 0.0f, nBytes);
	milliseconds = 0;
	
	printf("Prodotto blocco-blocco CUBLAS\n");
	alpha = 1.0f;
	beta = 0.0f;
	
	CHECK(cudaEventRecord(start1));
	int offset = 0;
	for (int i = 0; i < k; ++i) {
		int size = C.blkSize[i];
		CHECK_CUBLAS(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, size, size, size, &alpha, d_A.elem + offset, n, d_B.elem + offset, n, &beta, d_C.elem + offset, n));
		offset += size * n + size;
	}
	CHECK(cudaEventRecord(stop1));
	CHECK(cudaEventSynchronize(stop1));
	CHECK(cudaEventElapsedTime(&milliseconds, start1, stop1));
	printf("    elapsed time: %.5f (sec)\n", milliseconds / 1000);

	// Retrieve the output vector from the device
	CHECK_CUBLAS(cublasGetMatrix(n, n, sizeof(float), d_C.elem, n, C1.elem, n));

	checkResult(C, C1);

	cublasDestroy(handle);

	CHECK(cudaFree(d_A.elem));
	CHECK(cudaFree(d_B.elem));
	CHECK(cudaFree(d_C.elem));

	CHECK(cudaFree(d_A.blkSize));
	CHECK(cudaFree(d_B.blkSize));
	CHECK(cudaFree(d_C.blkSize));

	CHECK(cudaDeviceReset());

    return EXIT_SUCCESS;
}