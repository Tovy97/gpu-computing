#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <curand.h>
#include "../../../Lib/common.h"

int main(int argc, char* argv[])
{
	size_t n = 100;
	size_t i;
	curandGenerator_t gen;
	float* devData, * hostData;

	/* Allocate n floats on host */
	hostData = (float*)calloc(n, sizeof(float));

	/* Allocate n floats on device */
	CHECK(cudaMalloc((void**)&devData, n * sizeof(float)));

	/* Create pseudo-random number generator */
	CHECK_CURAND(curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT));

	/* Set seed */
	CHECK_CURAND(curandSetPseudoRandomGeneratorSeed(gen, 1234ULL));

	/* Generate n floats on device */
	CHECK_CURAND(curandGenerateUniform(gen, devData, n));

	/* Copy device memory to host */
	CHECK(cudaMemcpy(hostData, devData, n * sizeof(float), cudaMemcpyDeviceToHost));

	/* Show result */
	for (i = 0; i < n; i++) {
		printf("%1.4f ", hostData[i]);
	}
	printf("\n");

	/* Cleanup */
	CHECK_CURAND(curandDestroyGenerator(gen));
	CHECK(cudaFree(devData));
	free(hostData);
	return EXIT_SUCCESS;
}
