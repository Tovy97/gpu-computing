#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/mqdb.h"
#include "../../../Lib/mqdb.c"
#include "../../../Lib/common.h"

#define BLOCK_SIZE 32     // block size
#define DELIMITER ';'

struct tms {
	double CPUtms;
	double GPUtmsNaive;
	double GPUtmsMQDB;
	double GPUtmsMQDBDynPar1;
	double GPUtmsMQDBDynPark;
	float density;
};

/*
 * Kernel for standard (naive) matrix product
 */
__global__ void matProd(mqdb A, mqdb B, mqdb C, int n) {
	// row & col indexes
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;

	// each thread computes an entry of the product matrix
	if ((row < n) && (col < n)) {
		float val = 0;
		for (int k = 0; k < n; k++)
			val += A.elem[row * n + k] * B.elem[k * n + col];
		C.elem[row * n + col] = val;
	}
}

/*
 * Kernel for block sub-matrix product of mqdb
 */
__global__ void mqdbBlockProd(mqdb mA, mqdb mB, mqdb mC, int D, int N, int offset = 0) {
	float* A = mA.elem;
	float* B = mB.elem;
	float* C = mC.elem;	
	uint Row = blockIdx.y * blockDim.y + threadIdx.y;
	uint Col = blockIdx.x * blockDim.x + threadIdx.x;		
	if (Row < N && Col < N) {
		float val = 0;
		for (int k = 0; k < N; k++) {
			val += A[(Row + offset) * D + k + offset] * B[(k + offset )* D + Col + offset];
		}		
		C[(Row + offset) * D + Col + offset] = val;
	}	
}

/*
 * Kernel for block sub-matrix product of mqdb: parent grid (version grid 1)
 */
__global__ void mqdbProd(mqdb d_A, mqdb d_B, mqdb d_C, int n, int k, int* offsets) {
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	for (int i = 0; i < k; ++i) {
		dim3 grid(ceil(d_A.blkSize[i] / (float)(block.x)), ceil(d_A.blkSize[i] / (float)(block.y)));
		mqdbBlockProd << <grid, block >> > (d_A, d_B, d_C, n, d_A.blkSize[i], offsets[i]);		
	}
}

/*
 * Kernel for block sub-matrix product of mqdb: parent grid (version grid k)
 */
__global__ void mqdbProdK(mqdb d_A, mqdb d_B, mqdb d_C, int n, int* offsets) {
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid(ceil(n / (float)(block.x)), ceil(n / (float)(block.y)));	
	mqdbBlockProd << <grid, block >> > (d_A, d_B, d_C, n, d_A.blkSize[threadIdx.x], offsets[threadIdx.x]);
}

/*
 * Test on MQDB kernels
 */
void testKernelsMQDB(const uint n, const uint k, struct tms* times) {

	// mqdb host matrices
	mqdb A, B, C, C1;

	// mqdb device matrices
	mqdb d_A, d_B, d_C;

	// fill in
	A = mqdbConst(n, k, 10, 1);
	B = mqdbConst(n, k, 10, 1);
	C = mqdbConst(n, k, 10, 1);
	C1 = mqdbConst(n, k, 10, 1);

	//offsets
	int* offsets = (int*)malloc(sizeof(int) * k);
	offsets[0] = 0;
	for (int i = 1; i < k; ++i) {
		offsets[i] = offsets[i - 1] + A.blkSize[i - 1];
	}

	int* d_offsets;
	CHECK(cudaMalloc(&d_offsets, sizeof(int) * k));
	CHECK(cudaMemcpy(d_offsets, offsets, sizeof(int) * k, cudaMemcpyHostToDevice));

	ulong nBytes = n * n * sizeof(float);
	ulong kBytes = k * sizeof(uint);
	printf("Memory size required = %.1f (MB)\n", (float)nBytes / (1024.0 * 1024.0));

	// malloc and copy on device memory
	d_A.nBlocks = A.nBlocks;
	CHECK(cudaMalloc(&d_A.blkSize, kBytes));
	CHECK(cudaMemcpy(d_A.blkSize, A.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_A.elem, nBytes));
	CHECK(cudaMemcpy(d_A.elem, A.elem, nBytes, cudaMemcpyHostToDevice));
	d_B.nBlocks = B.nBlocks;
	CHECK(cudaMalloc((void**)&d_B.blkSize, kBytes));
	CHECK(cudaMemcpy(d_B.blkSize, B.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_B.elem, nBytes));
	CHECK(cudaMemcpy(d_B.elem, B.elem, nBytes, cudaMemcpyHostToDevice));
	d_C.nBlocks = C.nBlocks;
	CHECK(cudaMalloc((void**)&d_C.blkSize, kBytes));
	CHECK(cudaMemcpy(d_C.blkSize, C.blkSize, kBytes, cudaMemcpyHostToDevice));
	CHECK(cudaMalloc((void**)&d_C.elem, nBytes));
	CHECK(cudaMemset(d_C.elem, 0.0, nBytes));

	/***********************************************************/
	/*                    CPU MQDB product                     */
	/***********************************************************/
	printf("CPU MQDB product...\n");
	double start = seconds();
	mqdbProd(A, B, C);
	double CPUTime = seconds() - start;
	printf("   CPU elapsed time:                    %.5f (sec)\n\n", CPUTime);

	/***********************************************************/
	/*                     GPU mat product                     */
	/***********************************************************/
	printf("Kernel (naive) mat product...\n");
	dim3 block(BLOCK_SIZE, BLOCK_SIZE);
	dim3 grid((n + block.x - 1) / block.x, (n + block.y - 1) / block.y);
	start = seconds();
	matProd << <grid, block >> > (d_A, d_B, d_C, n);
	CHECK(cudaDeviceSynchronize());
	double GPUtime1 = seconds() - start;
	printf("   elapsed time:                        %.2f (sec)\n", GPUtime1);
	printf("   speedup vs CPU MQDB         product: %.2f\n", CPUTime / GPUtime1);
	CHECK(cudaMemcpy(C1.elem, d_C.elem, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaMemset(d_C.elem, 0.0, nBytes));
	checkResult(C1, C);
	//	mqdbDisplay(C1);

	/***********************************************************/
	/*                     GPU MQDB product                    */
	/***********************************************************/
	printf("Kernel MQDB product...\n");
	start = seconds();
	for (int i = 0; i < k; ++i) {	
		grid = dim3(ceil(A.blkSize[i] / (float)(block.x)), ceil(A.blkSize[i] / (float)(block.y)));		
		mqdbBlockProd << <grid, block >> > (d_A, d_B, d_C, n, A.blkSize[i], offsets[i]);				
	}
	CHECK(cudaDeviceSynchronize());
	double GPUtime2 = seconds() - start;
	printf("   elapsed time:                        %.2f (sec)\n", GPUtime2);
	printf("   speedup vs CPU MQDB         product: %.2f\n", CPUTime / GPUtime2);
	printf("   speedup vs GPU mat          product: %.2f\n", GPUtime1 / GPUtime2);
	CHECK(cudaMemcpy(C1.elem, d_C.elem, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaMemset(d_C.elem, 0.0, nBytes));
	checkResult(C1, C);
	//mqdbDisplay(C1);
	//mqdbDisplay(C);
	
	/***********************************************************/
	/*                     GPU MQDB GRID(1) product            */
	/***********************************************************/

	printf("Kernel MQDB GRID(1) product...\n");
	start = seconds();
	mqdbProd << <1, 1 >> > (d_A, d_B, d_C, n, k, d_offsets);
	CHECK(cudaDeviceSynchronize());
	double GPUtime3 = seconds() - start;
	printf("   elapsed time:                        %.2f (sec)\n", GPUtime3);
	printf("   speedup vs CPU MQDB         product: %.2f\n", CPUTime / GPUtime3);
	printf("   speedup vs GPU mat          product: %.2f\n", GPUtime1 / GPUtime3);
	printf("   speedup vs GPU MQDB         product: %.2f\n", GPUtime2 / GPUtime3);
	CHECK(cudaMemcpy(C1.elem, d_C.elem, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaMemset(d_C.elem, 0.0, nBytes));
	checkResult(C1, C);

	/***********************************************************/
	/*                     GPU MQDB GRID(k) product            */
	/***********************************************************/

	printf("Kernel MQDB GRID(k) product...\n");
	start = seconds();
	mqdbProdK << <1, k >> > (d_A, d_B, d_C, n, d_offsets);
	CHECK(cudaDeviceSynchronize());
	double GPUtime4 = seconds() - start;
	printf("   elapsed time:                        %.2f (sec)\n", GPUtime4);
	printf("   speedup vs CPU MQDB         product: %.2f\n", CPUTime / GPUtime4);
	printf("   speedup vs GPU mat          product: %.2f\n", GPUtime1 / GPUtime4);
	printf("   speedup vs GPU MQDB         product: %.2f\n", GPUtime2 / GPUtime4);
	printf("   speedup vs GPU MQDB Grid(1) product: %.2f\n", GPUtime3 / GPUtime4);
	CHECK(cudaMemcpy(C1.elem, d_C.elem, nBytes, cudaMemcpyDeviceToHost));
	CHECK(cudaMemset(d_C.elem, 0.0, nBytes));
	checkResult(C1, C);

	/***********************************************************************/

	CHECK(cudaFree(d_A.elem));
	CHECK(cudaFree(d_B.elem));
	CHECK(cudaFree(d_C.elem));

	// collect times
	times->CPUtms = CPUTime;
	times->GPUtmsNaive = GPUtime1;
	times->GPUtmsMQDB = GPUtime2;
	times->GPUtmsMQDBDynPar1 = GPUtime3;
	times->GPUtmsMQDBDynPark = GPUtime4;

	float den = 0;
	for (uint j = 0; j < k; j++)
		den += A.blkSize[j] * A.blkSize[j];
	times->density = den / (n * n);
}

/*
 * main function
 */
int main(int argc, char* argv[]) {
	const uint n = 11 * 1024;      // matrix size --> 11 * 1024
	const uint min_k = 30;       // min num of blocks
	const uint max_k = 50;       // max num of blocks

	struct tms times[max_k - min_k + 1];

	// multiple tests on kernels
	for (uint k = min_k; k <= max_k; k ++) {
		printf("\n*****   k = %d --- (avg block size = %f)\n", k, (float)n / k);
		testKernelsMQDB(n, k, &times[k - min_k]);
	}

	FILE* fd;
	fd = fopen("res.csv", "w");
	if (fd == NULL) {
		perror("file error!\n");
		exit(1);
	}

	// write results on file
	fprintf(fd, "num blocks,");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%d%c", j + min_k, DELIMITER);

	fprintf(fd, "\nCPU MQDB product,");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].CPUtms, DELIMITER);

	fprintf(fd, "\nKernel mat product naive,");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].GPUtmsNaive, DELIMITER);

	fprintf(fd, "\nKernel MQDB product,");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].GPUtmsMQDB, DELIMITER);

	fprintf(fd, "\nKernel MQDB product with dynamic parall. GRID(1),");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].GPUtmsMQDBDynPar1, DELIMITER);

	fprintf(fd, "\nKernel MQDB product with dynamic parall. GRID(k),");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].GPUtmsMQDBDynPark, DELIMITER);

	fprintf(fd, "\ndensity,");
	for (uint j = 0; j <= max_k - min_k; j++)
		fprintf(fd, "%.4f%c", times[j].density, DELIMITER);

	fclose(fd);

	return 0;
}
