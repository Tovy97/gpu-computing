#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

int main() {
    int numDevices = 0;
    cudaGetDeviceCount(&numDevices);
    if (numDevices > 0) {
        int maxMultiprocessors = 0, maxDevice = 0;
        for (int device = 0; device < numDevices; device++) {
            cudaDeviceProp props;
            cudaGetDeviceProperties(&props, device);
            if (maxMultiprocessors < props.multiProcessorCount) {
                maxMultiprocessors = props.multiProcessorCount;
                maxDevice = device;
            }
        }
        cudaSetDevice(maxDevice);
    }

    numDevices = 0;
    cudaGetDeviceCount(&numDevices);

    printf("Selected device #%d", numDevices);

    return 0;
}

