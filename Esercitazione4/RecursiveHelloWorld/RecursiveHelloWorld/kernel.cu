#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define SIZE 32

__global__ void child(int * data) {
	data[threadIdx.x] += 1;	
}

__global__ void parent(int* data) {
	data[threadIdx.x + blockIdx.x * blockDim.x] = 1;
	__syncthreads();

	if (threadIdx.x == 0) {				
		child << <1, SIZE >> > (data + SIZE * blockIdx.x);
		cudaDeviceSynchronize();
	}

	__syncthreads();
}

__global__ void hello(int c, int d) {
	printf("Hello by %d - %d in deep %d\n", blockIdx.x, threadIdx.x, d);
	if (c == 1) {
		return;
	}
	c /= 2;
	if (threadIdx.x == 0 && c > 0) {
		printf("Go children\n");
		hello << < gridDim, c>> > (c, d + 1);
	}
}

void adder() {
	int* data;
	CHECK(cudaMalloc(&data, 2 * SIZE * sizeof(int)));
	cudaMemset(data, 0, 2 * SIZE * sizeof(int));

	parent << <2, SIZE >> > (data);

	cudaDeviceSynchronize();

	int d[2 * SIZE];
	CHECK(cudaMemcpy(d, data, sizeof(int) * SIZE * 2, cudaMemcpyDeviceToHost));

	for (int i = 0; i < SIZE * 2; ++i) {
		printf("%d\n", d[i]);
	}
}

void sayHello() {
	int G = 2;
	int B = 8;
	hello << <G, B >> > (B, 0);
}

int main() {
	sayHello();
	
}