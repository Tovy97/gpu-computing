
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct Pixel {
	unsigned char R, G, B;
} Pixel;

typedef Pixel * Image;

void printImage(Image img, unsigned int width, unsigned int height) {
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			printf("[%3d, %3d, %3d]", img[i * width + j].R, img[i * width + j].G, img[i * width + j].B);
		}
		printf("\n");
	}
	printf("\n");
}

__global__ void flipH(Image img, Image cpy, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = idx % width;

	if (idx < width * height) {
		cpy[(height - row - 1) * width + col].R = img[row * width + col].R;
		cpy[(height - row - 1) * width + col].G = img[row * width + col].G;
		cpy[(height - row - 1) * width + col].B = img[row * width + col].B;
	}
}

__global__ void flipV(Image img, Image cpy, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = idx % width;

	if (idx < width * height) {
		cpy[row * width + (width - col - 1)].R = img[row * width + col].R;
		cpy[row * width + (width - col - 1)].G = img[row * width + col].G;
		cpy[row * width + (width - col - 1)].B = img[row * width + col].B;
	}
}

int main()
{
	srand(time(NULL));
	unsigned int width, height;
	printf("Width: ");
	scanf("%d", &width);
	printf("Height: ");
	scanf("%d", &height);

	Image img = (Image)malloc(width * height * sizeof(Pixel));

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			img[i * width + j].R = rand() % 256;
			img[i * width + j].G = rand() % 256;
			img[i * width + j].B = rand() % 256;
		}
	}

	printImage(img, width, height);

	int flip;
	do {
		printf("Flip [0 = V, 1 = H]: ");
		scanf("%d", &flip);
	} while (flip != 0 && flip != 1);

	unsigned int block;
	printf("Block size: ");
	scanf("%d", &block);

	unsigned int grid = ceil((width * height) / block);

	Image dev_img, dev_cpy;
	cudaMalloc(&dev_img, width * height * sizeof(Pixel));
	cudaMalloc(&dev_cpy, width * height * sizeof(Pixel));
	cudaMemcpy(dev_img, img, width * height * sizeof(Pixel), cudaMemcpyHostToDevice);	

	if (flip == 1) {
		flipH << <grid, block >> > (dev_img, dev_cpy, width, height);
	}
	else {
		flipV << <grid, block >> > (dev_img, dev_cpy, width, height);
	}

	cudaDeviceSynchronize();
	cudaMemcpy(img, dev_cpy, width * height * sizeof(Pixel), cudaMemcpyDeviceToHost);

	printf("\n");

	printImage(img, width, height);

	cudaFree(dev_img);

	cudaDeviceReset();

	free(img);

	return 0;
}