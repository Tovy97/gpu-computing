#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

void input(const char * str, float* var) { 
	printf("%s", str);
	scanf("%f", var);
}

float float_rand(int min, int max) {
	return ((((float)rand()) / RAND_MAX) * (max - min)) + min;
}

__global__ void sum(float * imm1, float *imm2, float *imm3, int w, int h) {	
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int idx = row * w + col;

	if ((row < w) && (col < h)) {		
		imm3[idx] = imm1[idx] + imm2[idx];
		if (imm3[idx] > 1.0f) {
			imm3[idx] = 1.0f;
		}
	}
}

void printImg(float* imm, int h, int w) {
	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j) {
			int idx = i * w + j;
			printf("%1.2f ", imm[idx]);
		}		
		printf("\n");
	}
	printf("\n");
}

int main()
{
	srand(time(NULL));
	float h, w;	
	//leggo in input n e m
	input("Width: ", &w);
	input("Height: ", &h);

	const int size = h * w;

	//genero due matrici di float random
	float *imm1, *imm2, *imm3;
	imm1 = (float*)malloc(size * sizeof(float));
	imm2 = (float*)malloc(size * sizeof(float));
	imm3 = (float*)malloc(size * sizeof(float));

	memset(imm3, 0.0f, size * sizeof(float));

	for (int i = 0; i < h; ++i) {
		for (int j = 0; j < w; ++j) {
			int idx = i * w + j;
			imm1[idx] = float_rand(0, 1);
			imm2[idx] = float_rand(0, 1);
		}
	}

	//preparo la memoria nel device
	float *dev_imm1, *dev_imm2, *dev_imm3;
	cudaMalloc(&dev_imm1, size * sizeof(float));
	cudaMalloc(&dev_imm2, size * sizeof(float));
	cudaMalloc(&dev_imm3, size * sizeof(float));

	cudaMemcpy(dev_imm1, imm1, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_imm2, imm2, size * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemset(imm3, 0.0f, size * sizeof(float));

	//le sommo
	dim3 blocchi = dim3(16, 16);		
	dim3 griglia = dim3(ceil(w / 16.0), ceil(h / 16.0));

	sum<<<griglia, blocchi>>>(dev_imm1, dev_imm2, dev_imm3, w , h);
	
	cudaDeviceSynchronize();

	cudaMemcpy(imm3, dev_imm3, size * sizeof(float), cudaMemcpyDeviceToHost);

	printImg(imm1, h, w);
	printImg(imm2, h, w);
	printImg(imm3, h, w);

	cudaFree(dev_imm1);
	cudaFree(dev_imm2);
	cudaFree(dev_imm3);

	cudaDeviceReset();

	free(imm1);
	free(imm2);
	free(imm3);

    return 0;
}
