#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../../../Lib/bmpUtil.h"
#include "../../../Lib/common.h"

/*
 * Kernel 1D that flips the given image vertically
 * each thread only flips a single pixel (R,G,B)
 */
__global__ void Vflip(pel *imgDst, const pel *imgSrc, const uint w, const uint h) {
	uint i = blockIdx.x;
	uint j = threadIdx.x;
	uint b = blockDim.x;

	uint x = b * i + j;	
	uint blockPerRow = (w + b - 1) / b;
	uint row = i / blockPerRow;
	//uint col = x - row * w;				//<--- Versione prof (non funziona)
	//uint col = x - row * b * blockPerRow;	//<-- versione modificata
	uint col = x % (blockPerRow * b);
	
	if (col >= w) {
		return;
	}

	uint newRow = h - 1 - row;
	uint bytesPerRow = (w * 3 + 3) & (~3);
	uint pixel1 = bytesPerRow * row + 3 * col;
	uint pixel2 = bytesPerRow * newRow + 3 * col;

	imgDst[pixel2] = imgSrc[pixel1];
	imgDst[pixel2 + 1] = imgSrc[pixel1 + 1];
	imgDst[pixel2 + 2] = imgSrc[pixel1 + 2];
}

/*
 *  Kernel that flips the given image horizontally
 *  each thread only flips a single pixel (R,G,B)
 */
__global__ void Hflip(pel *imgDst, const pel *imgSrc, const uint w) {
	uint i = blockIdx.x;
	uint j = threadIdx.x;
	uint b = blockDim.x;

	uint x = b * i + j;
	uint blockPerRow = (w + b - 1) / b;
	uint row = i / blockPerRow;
	//uint col = x - row * w;
	//uint col = x - row * b * blockPerRow;
	uint col = x % (blockPerRow * b);

	if (col >= w) {
		return;
	}	
	
	uint newCol = w - 1 - col;
	uint bytesPerRow = (w * 3 + 3) & (~3);
	uint pixel1 = bytesPerRow * row + 3 * col;
	uint pixel2 = bytesPerRow * row + 3 * newCol;
	imgDst[pixel2] = imgSrc[pixel1];
	imgDst[pixel2 + 1] = imgSrc[pixel1 + 1];
	imgDst[pixel2 + 2] = imgSrc[pixel1 + 2];
}

/*
 * MAIN
 */
int main(int argc, char **argv) {
	char flip = 'V';
	uint dimBlock = 256, dimGrid;
	pel *imgSrc, *imgDst;		 // Where images are stored in CPU
	pel *imgSrcGPU, *imgDstGPU;	 // Where images are stored in GPU

	if (argc > 4) {
		dimBlock = atoi(argv[4]);
		flip = argv[3][0];
	}
	else if (argc > 3) {
		flip = argv[3][0];
	}
	else if (argc < 3) {
		printf("\n\nUsage:   imflipG InputFilename OutputFilename [V/H] [dimBlock]");
		exit(EXIT_FAILURE);
	}
	if ((flip != 'V') && (flip != 'H')) {
		printf("Invalid flip option '%c'. Must be 'V','H'... \n", flip);
		exit(EXIT_FAILURE);
	}

	// Create CPU memory to store the input and output images
	imgSrc = ReadBMPlin(argv[1]); // Read the input image if memory can be allocated
	if (imgSrc == NULL) {
		printf("Cannot allocate memory for the input image...\n");
		exit(EXIT_FAILURE);
	}
	imgDst = (pel *)malloc(IMAGESIZE);
	if (imgDst == NULL) {
		free(imgSrc);
		printf("Cannot allocate memory for the input image...\n");
		exit(EXIT_FAILURE);
	}

	// Allocate GPU buffer for the input and output images
	CHECK(cudaMalloc((void**)&imgSrcGPU, IMAGESIZE));
	CHECK(cudaMalloc((void**)&imgDstGPU, IMAGESIZE));

	// Copy input vectors from host memory to GPU buffers.
	CHECK(cudaMemcpy(imgSrcGPU, imgSrc, IMAGESIZE, cudaMemcpyHostToDevice));

	// invoke kernels (define grid and block sizes)
	int rowBlock = (WIDTH + dimBlock - 1) / dimBlock;
	dimGrid = HEIGHT * rowBlock;

	time_t start, stop;  // start time
	time(&start);

	switch (flip) {
	case 'H':
		Hflip << <dimGrid, dimBlock >> > (imgDstGPU, imgSrcGPU, WIDTH);
		break;
	case 'V':
		Vflip << <dimGrid, dimBlock >> > (imgDstGPU, imgSrcGPU, WIDTH, HEIGHT);
		break;
	}
	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	CHECK(cudaDeviceSynchronize());

	time(&stop);  // elapsed time

	// Copy output (results) from GPU buffer to host (CPU) memory.
	CHECK(cudaMemcpy(imgDst, imgDstGPU, IMAGESIZE, cudaMemcpyDeviceToHost));

	// Write the flipped image back to disk
	WriteBMPlin(imgDst, argv[2]);

	printf("\nKernel elapsed time %f sec \n\n", difftime(stop, start));

	// Deallocate CPU, GPU memory and destroy events.
	cudaFree(imgSrcGPU);
	cudaFree(imgDstGPU);

	// cudaDeviceReset must be called before exiting in order for profiling and
	// tracing tools spel as Parallel Nsight and Visual Profiler to show complete traces.
	cudaError_t	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		free(imgSrc);
		free(imgDst);
		exit(EXIT_FAILURE);
	}
	free(imgSrc);
	free(imgDst);
	return (EXIT_SUCCESS);
}

/*
 *  Read a 24-bit/pixel BMP file into a 1D linear array.
 *  Allocate memory to store the 1D image and return its pointer
 */
pel *ReadBMPlin(char* fn) {
	static pel *Img;
	FILE* f = fopen(fn, "rb");
	if (f == NULL) {
		printf("\n\n%s NOT FOUND\n\n", fn);
		exit(EXIT_FAILURE);
	}

	pel HeaderInfo[54];
	size_t nByte = fread(HeaderInfo, sizeof(pel), 54, f); // read the 54-byte header
	// extract image height and width from header
	int width = *(int*)&HeaderInfo[18];
	img.width = width;
	int height = *(int*)&HeaderInfo[22];
	img.height = height;
	int RowBytes = (width * 3 + 3) & (~3);  // row is multiple of 4 pixel
	img.rowByte = RowBytes;
	//save header for re-use
	memcpy(img.headInfo, HeaderInfo, 54);
	printf("\n Input File name: %5s  (%d x %d)   File Size=%lu", fn, img.width,
		img.height, IMAGESIZE);
	// allocate memory to store the main image (1 Dimensional array)
	Img = (pel *)malloc(IMAGESIZE);
	if (Img == NULL)
		return Img;      // Cannot allocate memory
	// read the image from disk
	size_t out = fread(Img, sizeof(pel), IMAGESIZE, f);
	fclose(f);
	return Img;
}

/*
 *  Write the 1D linear-memory stored image into file
 */
void WriteBMPlin(pel *Img, char* fn) {
	FILE* f = fopen(fn, "wb");
	if (f == NULL) {
		printf("\n\nFILE CREATION ERROR: %s\n\n", fn);
		exit(1);
	}
	//write header
	fwrite(img.headInfo, sizeof(pel), 54, f);
	//write data
	fwrite(Img, sizeof(pel), IMAGESIZE, f);
	printf("\nOutput File name: %5s  (%u x %u)   File Size=%lu", fn, img.width,
		img.height, IMAGESIZE);
	fclose(f);
}