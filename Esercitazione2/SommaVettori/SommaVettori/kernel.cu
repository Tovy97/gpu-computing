#include <stdio.h>
#include <time.h>
#include <windows.h> 
#include "cuda_runtime.h"

#define CHECK(call) {															\
	const cudaError_t error = call;												\
	if (error != cudaSuccess) {													\
		printf("Error: %s:%d, ", __FILE__, __LINE__);							\
		printf("code:%d, reason: %s\n", error, cudaGetErrorString(error));		\
		exit(1);																\
	}																			\
}

/*
	RISULTATI
		N					| TIME
		---------------------------------------------------
		1024*1024			| 0.001995
		1024*1024*2			| 0.003989
		1024*1024*3			| 0.004987
		1024*1024*4			| 0.005984
		1024*1024*5			| 0.007980
		1024*1024*6			| 0.008974
		1024*1024*7			| 0.009973
		1024*1024*8			| 0.010971
		1024*1024*9			| 0.011938
		1024*1024*10		| 0.012982
		1024*1024*15		| 0.018950
		1024*1024*20		| 0.023964
		1024*1024*25		| 0.028922
		1024*1024*30		| 0.033909
		1024*1024*35		| 0.039894
		1024*1024*40		| 0.044888
		1024*1024*45		| 0.048904
		1024*1024*50		| 0.055880
		1024*1024*55		| 0.060837
		1024*1024*56		| 0.061911
		1024*1024*57		| 0.062832
		6 * 10^7			| 0.063828
*/

#define N 1024*1024
#define SIZE N * sizeof(int)
#define ThxBl 32

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
#define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

struct timezone
{
	int  tz_minuteswest; 
	int  tz_dsttime;     
};

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
	FILETIME ft;
	unsigned __int64 tmpres = 0;
	static int tzflag;

	if (NULL != tv)
	{
		GetSystemTimeAsFileTime(&ft);

		tmpres |= ft.dwHighDateTime;
		tmpres <<= 32;
		tmpres |= ft.dwLowDateTime;

		/*converting file time to unix epoch*/
		tmpres -= DELTA_EPOCH_IN_MICROSECS;
		tmpres /= 10;  /*convert into microseconds*/
		tv->tv_sec = (long)(tmpres / 1000000UL);
		tv->tv_usec = (long)(tmpres % 1000000UL);
	}

	if (NULL != tz)
	{
		if (!tzflag)
		{
			_tzset();
			tzflag++;
		}
		tz->tz_minuteswest = _timezone / 60;
		tz->tz_dsttime = _daylight;
	}

	return 0;
}

__global__ void sommaVettori(int *a, int *b, int *c) {
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < N) {
		c[idx] = a[idx] + b[idx];
	}
}

double cpuSecond() {
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec*1.e-6);
}

int main()
{
	srand(time(NULL));
	int *a, *b, *c;
	int *dev_a, *dev_b, *dev_c;

	a = (int*)malloc(SIZE);
	b = (int*)malloc(SIZE);
	c = (int*)malloc(SIZE);

	cudaMalloc((void**)&dev_a, SIZE);
	cudaMalloc((void**)&dev_b, SIZE);
	cudaMalloc((void**)&dev_c, SIZE);

	for (int i = 0; i < N; ++i) {
		a[i] = rand() % 10;
		b[i] = rand() % 10;
	}

	cudaMemcpy(dev_a, a, SIZE, cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, SIZE, cudaMemcpyHostToDevice);

	double iStart = cpuSecond();

	sommaVettori<<<N / ThxBl, ThxBl>>>(dev_a, dev_b, dev_c);

	cudaDeviceSynchronize();

	double iElaps = cpuSecond() - iStart;

	cudaMemcpy(c, dev_c, SIZE, cudaMemcpyDeviceToHost);

	/*for (int i = 0; i < N; ++i) {
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
	}*/

	printf("%lf\n", iElaps);
	
	free(a);
	free(b);
	free(c);

	cudaFree(dev_a);
	cudaFree(dev_b);
	cudaFree(dev_c);

	cudaDeviceReset();

	//system("pause");
    return 0;
}