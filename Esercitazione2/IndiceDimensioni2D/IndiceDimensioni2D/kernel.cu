
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

/*
 * Show DIMs & IDs for grid, block and thread
 */
__global__ void checkIndex(void) {	
	if ((threadIdx.x + threadIdx.y) && (threadIdx.x + threadIdx.y) % 5 == 0) {
		printf("threadIdx:(%d, %d, %d) blockIdx:(%d, %d, %d) blockDim:(%d, %d, %d) gridDim:(%d, %d, %d)\n",
			threadIdx.x, threadIdx.y, threadIdx.z,
			blockIdx.x, blockIdx.y, blockIdx.z,
			blockDim.x, blockDim.y, blockDim.z,
			gridDim.x, gridDim.y, gridDim.z);
	}
}

__global__ void checkIndex3D(void) {
	if ((threadIdx.x + threadIdx.y + threadIdx.z) && (threadIdx.x + threadIdx.y + threadIdx.z) % 5 == 0) {
		printf("threadIdx:(%d, %d, %d) blockIdx:(%d, %d, %d) blockDim:(%d, %d, %d) gridDim:(%d, %d, %d)\n",
			threadIdx.x, threadIdx.y, threadIdx.z,
			blockIdx.x, blockIdx.y, blockIdx.z,
			blockDim.x, blockDim.y, blockDim.z,
			gridDim.x, gridDim.y, gridDim.z);
	}
}

int main(int argc, char **argv) {

	// grid and block structure
	dim3 block = dim3(7, 6);
	dim3 grid = dim3(2, 2);

	// check for host
	printf("CHECK for host:\n");
	printf("grid.x = %d\t grid.y = %d\t grid.z = %d\n", grid.x, grid.y, grid.z);
	printf("block.x = %d\t block.y = %d\t block.z %d\n", block.x, block.y, block.z);

	// check for device
	printf("CHECK for device:\n");
	checkIndex <<< grid, block>>>();

	cudaDeviceSynchronize();

	printf("\n\n\n\n");

	// reset device
	cudaDeviceReset();

	// grid and block structure
	block = dim3(8, 7, 3);
	grid = dim3(2, 2, 3);

	// check for host
	printf("CHECK for host:\n");
	printf("grid.x = %d\t grid.y = %d\t grid.z = %d\n", grid.x, grid.y, grid.z);
	printf("block.x = %d\t block.y = %d\t block.z %d\n", block.x, block.y, block.z);

	// check for device
	printf("CHECK for device:\n");
	checkIndex3D << < grid, block >> > ();

	// reset device
	cudaDeviceReset();

	return (0);
}