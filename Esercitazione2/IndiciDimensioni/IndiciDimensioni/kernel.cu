#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>

#define CHECK(call) {															\
	const cudaError_t error = call;												\
	if (error != cudaSuccess) {													\
		printf("Error: %s:%d, ", __FILE__, __LINE__);							\
		printf("code:%d, reason: %s\n", error, cudaGetErrorString(error));		\
		exit(1);																\
	}																			\
}


__global__ void checkIndex(void) {
	printf("threadIdx:(%d, %d, %d) blockIdx:(%d, %d, %d) blockDim:(%d, %d, %d) gridDim:(%d, %d, %d)\n",
		threadIdx.x, threadIdx.y, threadIdx.z,
		blockIdx.x, blockIdx.y, blockIdx.z,
		blockDim.x, blockDim.y, blockDim.z,
		gridDim.x, gridDim.y, gridDim.z);
}

int main()
{
	dim3 block(4); //4, 1, 1
	dim3 grid(3); //3, 1, 1

	printf("grid.x = %d  grid.y = %d  grid.z = %d\n", grid.x, grid.y, grid.z);
	printf("block.x = %d  block.y = %d  block.z = %d\n", block.x, block.y, block.z);
	
	checkIndex <<<grid, block>>>();
	CHECK(cudaDeviceSynchronize());
	printf("CIAO");	
	
	cudaDeviceReset();
    return 0;
}