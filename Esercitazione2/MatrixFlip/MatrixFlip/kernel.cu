
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef struct Pixel {
	unsigned char R, G, B;
} Pixel;

typedef Pixel * Image;

void printImage(Image img, unsigned int width, unsigned int height) {
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			printf("[%3d, %3d, %3d]", img[i * width + j].R, img[i * width + j].G, img[i * width + j].B);
		}
		printf("\n");
	}
	printf("\n");
}

__global__ void flipH(Image img, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = idx % width;
	if (row < height / 2) {
		Pixel temp;
		temp.R = img[row * width + col].R;
		temp.G = img[row * width + col].G;
		temp.B = img[row * width + col].B;

		img[row * width + col].R = img[(height - row - 1) * width + col].R;
		img[row * width + col].G = img[(height - row - 1) * width + col].G;
		img[row * width + col].B = img[(height - row - 1) * width + col].B;

		img[(height - row - 1) * width + col].R = temp.R;
		img[(height - row - 1) * width + col].G = temp.G;
		img[(height - row - 1) * width + col].B = temp.B;
	}
}

__global__ void flipV(Image img, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = idx % width;
	if (col < width / 2) {
		Pixel temp;
		temp.R = img[row * width + col].R;
		temp.G = img[row * width + col].G;
		temp.B = img[row * width + col].B;

		img[row * width + col].R = img[row * width + (width - col - 1)].R;
		img[row * width + col].G = img[row * width + (width - col - 1)].G;
		img[row * width + col].B = img[row * width + (width - col - 1)].B;

		img[row * width + (width - col - 1)].R = temp.R;
		img[row * width + (width - col - 1)].G = temp.G;
		img[row * width + (width - col - 1)].B = temp.B;
	}
}

int main()
{
	srand(time(NULL));
	unsigned int width, height;
	printf("Width: ");
	scanf("%d", &width);
	printf("Height: ");
	scanf("%d", &height);

	Image img = (Image)malloc(width * height * sizeof(Pixel));

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
			img[i * width + j].R = rand() % 256;
			img[i * width + j].G = rand() % 256;
			img[i * width + j].B = rand() % 256;
		}
	}

	printImage(img, width, height);

	int flip;
	do {
		printf("Flip [0 = V, 1 = H]: ");
		scanf("%d", &flip);
	} while (flip != 0 && flip != 1);

	unsigned int block;
	printf("Block size: ");
	scanf("%d", &block);

	unsigned int grid = ceil((width * height) / block);

	Image dev_img;	
	cudaMalloc(&dev_img, width * height * sizeof(Pixel));
	cudaMemcpy(dev_img, img, width * height * sizeof(Pixel), cudaMemcpyHostToDevice);

	if (flip == 1) {
		flipH <<<grid, block>>> (dev_img, width, height);
	}
	else {
		flipV <<<grid, block>>> (dev_img, width, height);
	}

	cudaDeviceSynchronize();
	cudaMemcpy(img, dev_img, width * height * sizeof(Pixel), cudaMemcpyDeviceToHost);

	printf("\n");

	printImage(img, width, height);

	cudaFree(dev_img);

	cudaDeviceReset();

	free(img);

	return 0;
}