
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

typedef unsigned char * Image;

void printImage(Image img, unsigned int width, unsigned int height) {
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width * 3; j += 3) {
			printf("[%3d, %3d, %3d]", img[i * width * 3 + j], img[i * width * 3 + j + 1], img[i * width * 3 + j + 2]);
		}
		printf("\n");
	}
	printf("\n");
}

__global__ void flipH(Image img, Image cpy, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = (idx % width) * 3;
	
	if (idx < width * height) {
		cpy[(height - row - 1) * width * 3 + col] = img[row * width * 3 + col];
		cpy[(height - row - 1) * width * 3 + col + 1] = img[row * width * 3 + col + 1];
		cpy[(height - row - 1) * width * 3 + col + 2] = img[row * width * 3 + col + 2];
	}
}

__global__ void flipV(Image img, Image cpy, unsigned int width, unsigned int height) {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int row = idx / width;
	int col = (idx % width) * 3;

	if (idx < width * height) {
		cpy[row * width * 3 + ((width - 1) * 3 - col)] = img[row * width * 3 + col];
		cpy[row * width * 3 + ((width - 1) * 3 - col) + 1] = img[row * width * 3 + col + 1];
		cpy[row * width * 3 + ((width - 1) * 3 - col) + 2] = img[row * width * 3 + col + 2];
	}
}

int main()
{
	srand(time(NULL));
	unsigned int width, height;
	printf("Width: ");
	scanf("%d", &width);
	printf("Height: ");
	scanf("%d", &height);

	Image img = (Image)malloc(width * height * sizeof(Image) * 3);

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width * 3; ++j) {
			img[i * width * 3 + j] = rand() % 256;
			img[i * width * 3 + j + 1] = rand() % 256;
			img[i * width * 3 + j + 2] = rand() % 256;
		}
	}

	printImage(img, width, height);

	int flip;
	do {
		printf("Flip [0 = V, 1 = H]: ");
		scanf("%d", &flip);
	} while (flip != 0 && flip != 1);

	unsigned int block;
	printf("Block size: ");
	scanf("%d", &block);

	unsigned int grid = ceil((width * height) / block);

	Image dev_img, dev_cpy;
	cudaMalloc(&dev_img, width * height * sizeof(Image) * 3);
	cudaMalloc(&dev_cpy, width * height * sizeof(Image) * 3);
	cudaMemcpy(dev_img, img, width * height * sizeof(Image) * 3, cudaMemcpyHostToDevice);

	if (flip == 1) {
		flipH << <grid, block >> > (dev_img, dev_cpy, width, height);
	}
	else {
		flipV << <grid, block >> > (dev_img, dev_cpy, width, height);
	}

	cudaDeviceSynchronize();
	cudaMemcpy(img, dev_cpy, width * height * sizeof(Image) * 3, cudaMemcpyDeviceToHost);

	printf("\n");

	printImage(img, width, height);

	cudaFree(dev_img);
	cudaFree(dev_cpy);

	cudaDeviceReset();

	free(img);

	return 0;
}