#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include "../../../Lib/common.h"

#define NGPUS 1

cudaError_t addWithCuda(int* c, const int* a, const int* b, unsigned int size);

__global__ void addKernel(int* c, const int* a, const int* b)
{
	int i = threadIdx.x;
	c[i] = a[i] + b[i];
}

void initialData(float* A, size_t size) {
	for (int i = 0; i < size; ++i) {
		A[i] = i;
	}
}

__global__ void sum(float* d_A, float* d_B, float* d_C, size_t size) {
	const int idx = threadIdx.x + blockDim.x * blockIdx.x;
	if (idx >= size) {
		return;
	}
	d_C[idx] = d_A[idx] + d_B[idx];
}

void noUVA() {
	float* d_A[NGPUS], * d_B[NGPUS], * d_C[NGPUS];
	float* h_A[NGPUS], * h_B[NGPUS], * hostRef[NGPUS], * gpuRef[NGPUS];
	cudaStream_t stream[NGPUS];

	int size = 1 << 24;
	int iSize = size / NGPUS;
	size_t iBytes = iSize * sizeof(float);
	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		cudaMalloc((void**)&d_A[i], iBytes);
		cudaMalloc((void**)&d_B[i], iBytes);
		cudaMalloc((void**)&d_C[i], iBytes);

		cudaMallocHost((void**)&h_A[i], iBytes);
		cudaMallocHost((void**)&h_B[i], iBytes);

		cudaMallocHost((void**)&hostRef[i], iBytes);
		cudaMallocHost((void**)&gpuRef[i], iBytes);

		cudaStreamCreate(&stream[i]);
	}

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		initialData(h_A[i], iSize);
		initialData(h_B[i], iSize);
	}

	/*CPU*/
	for (int g = 0; g < NGPUS; ++g) {
		for (int i = 0; i < iSize; ++i) {
			hostRef[g][i] = h_A[g][i] + h_B[g][i];
		}
	}

	/*GPU*/

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		cudaMemcpyAsync(d_A[i], h_A[i], iBytes, cudaMemcpyHostToDevice, stream[i]);
		cudaMemcpyAsync(d_B[i], h_B[i], iBytes, cudaMemcpyHostToDevice, stream[i]);
		dim3 block = 1024;
		dim3 grid = CEIL(iSize, block.x);
		sum << <grid, block, 0, stream[i] >> > (d_A[i], d_B[i], d_C[i], iSize);
		cudaMemcpyAsync(gpuRef[i], d_C[i], iBytes, cudaMemcpyDeviceToHost, stream[i]);
	}
	cudaDeviceSynchronize();

	/*check*/
	bool err = false;
	for (int g = 0; g < NGPUS; ++g) {
		for (int i = 0; i < iSize; ++i) {
			if (gpuRef[g][i] != hostRef[g][i]) {
				err = true;
				break;
			}
		}
	}
	if (err) {
		printf("ERRORE\n");
	}
	else {
		printf("OK\n");
	}

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		cudaFree(d_A[i]);
		cudaFree(d_B[i]);
		cudaFree(d_C[i]);
		cudaFreeHost(h_A[i]);
		cudaFreeHost(h_B[i]);
		cudaFreeHost(hostRef[i]);
		cudaFreeHost(gpuRef[i]);
		cudaStreamDestroy(stream[i]);
		cudaDeviceReset();
	}
}

void siUVA() {
	float* d_A[NGPUS], * d_B[NGPUS];
	float* hostRef[NGPUS], * gpuRef[NGPUS];
	cudaStream_t stream[NGPUS];

	int size = 1 << 24;
	int iSize = size / NGPUS;
	size_t iBytes = iSize * sizeof(float);

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		cudaMallocManaged((void**)&d_A[i], iBytes);
		cudaMallocManaged((void**)&d_B[i], iBytes);

		cudaMallocManaged((void**)&hostRef[i], iBytes);
		cudaMallocManaged((void**)&gpuRef[i], iBytes);

		cudaStreamCreate(&stream[i]);
	}

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		initialData(d_A[i], iSize);
		initialData(d_B[i], iSize);
	}

	/*CPU*/
	for (int g = 0; g < NGPUS; ++g) {
		for (int i = 0; i < iSize; ++i) {
			hostRef[g][i] = d_A[g][i] + d_B[g][i];
		}
	}

	/*GPU*/
	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);		
		dim3 block = 1024;
		dim3 grid = CEIL(iSize, block.x);
		sum << <grid, block, 0, stream[i] >> > (d_A[i], d_B[i], gpuRef[i], iSize);		
	}
	cudaDeviceSynchronize();

	/*check*/
	bool err = false;
	for (int g = 0; g < NGPUS; ++g) {
		for (int i = 0; i < iSize; ++i) {
			if (gpuRef[g][i] != hostRef[g][i]) {
				err = true;
				break;
			}
		}
	}
	if (err) {
		printf("ERRORE\n");
	}
	else {
		printf("OK\n");
	}

	for (int i = 0; i < NGPUS; i++) {
		cudaSetDevice(i);
		cudaFree(d_A[i]);
		cudaFree(d_B[i]);		
		cudaFree(hostRef[i]);
		cudaFree(gpuRef[i]);
		cudaStreamDestroy(stream[i]);
		cudaDeviceReset();
	}
}

int main() {
	noUVA();

	siUVA();

	return 0;
}