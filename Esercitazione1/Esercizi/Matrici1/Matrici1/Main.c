#include <stdio.h>
#include <stdlib.h>

#define DIM 3

int dimension[DIM] = { 2, 1, 2 };

void printMatrix(int* matrix, int size);
void printMatrixNoZero(int* matrix, int size);
void printBlockMatrixNoZero(int* matrix, int size);
void mult(int* A, int* B, int* C, int size);
void kroneker(int* A, int* B, int* D, int size);

void mult_scalare(int V, int* B, int* D, int size);

int main() {
	int size = 0;
	for (int i = 0; i < DIM; ++i) {
		size += dimension[i];
	}

	int * A = (int*)malloc(size * size * sizeof(int));
	int * B = (int*)malloc(size * size * sizeof(int));
	int * C = (int*)malloc(size * size * sizeof(int));
	int * D = (int*)malloc(size * size * size * size * sizeof(int));

	memset(A, 0, size * size * sizeof(int));
	memset(B, 0, size * size * sizeof(int));
	memset(C, 0, size * size * sizeof(int));
	memset(D, 0, size * size * size * size * sizeof(int));

	int rowOffset = 0;
	int colOffset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < dimension[i]; ++k) {
				A[rowOffset + colOffset + k] = dimension[i];
			}
			rowOffset += size;
		}
		colOffset += dimension[i];
	}
	memcpy(B, A, size * size * sizeof(int));

	printBlockMatrixNoZero(A, size);
	printMatrix(B, size);

	mult(A, B, C, size);

	printMatrix(C, size);

	kroneker(A, B, D, size);

	printMatrixNoZero(D, size * size);
}

void kroneker(int* A, int* B, int* D, int size) {
	int rowOffset = 0;
	int colOffset = 0;
	int dRowOffset = 0;
	int dColOffset = 0;
	int oldDColOffset = 0;;	
	for (int i = 0; i < DIM; ++i) {		
		for (int j = 0; j < dimension[i]; ++j) {			
			for (int k = 0; k < dimension[i]; ++k) {
				mult_scalare(A[rowOffset + colOffset + k], B, D + dColOffset + dRowOffset, size);
				dColOffset += size;
			}
			dRowOffset += size * size * size;
			dColOffset = oldDColOffset;
			rowOffset += size;
		}		
		dColOffset += dimension[i] * size;
		oldDColOffset = dColOffset;
		colOffset += dimension[i];
	}
}

void mult_scalare(int V, int* B, int* D, int size) {
	int rowOffset = 0;
	int colOffset = 0;
	int dOffdet = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < dimension[i]; ++k) {
				D[dOffdet + colOffset + k] = V * B[rowOffset + colOffset + k];
			}
			rowOffset += size;
			dOffdet += size * size;
		}
		colOffset += dimension[i];
	}
}

void mult(int* A, int* B, int* C, int size) {
	int rowOffset = 0;
	int colOffset = 0; 
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < dimension[i]; ++k) {
				for (int h = 0; h < dimension[i]; ++h) {					
					C[rowOffset + size * j + colOffset + k] += A[rowOffset + size * j + colOffset + h] * B[rowOffset + size * h + colOffset + k];
				}
			}
		}
		rowOffset += size * dimension[i];
		colOffset += dimension[i];
	}
}

void printMatrix(int* matrix, int size) {
	int rowOffset = 0;
	int colOffset = 0;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			printf("%2d ", matrix[i * size + j]);			
		}
		printf("\n");
	}
	printf("\n");
}

void printMatrixNoZero(int* matrix, int size) {
	int rowOffset = 0;
	int colOffset = 0;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (matrix[i * size + j] == 0) {
				printf("   ");
			}
			else {
				printf("%2d ", matrix[i * size + j]);
			}
		}
		printf("\n");
	}
	printf("\n");
}

void printBlockMatrixNoZero(int* matrix, int size) {
	int rowOffset = 0;
	int colOffset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < colOffset; ++k) {
				printf("   ");
			}
			for (int k = 0; k < dimension[i]; ++k) {
				printf("%2d ", matrix[rowOffset + colOffset + k]);
			}
			rowOffset += size;
			printf("\n");
		}
		colOffset += dimension[i];		
	}
	printf("\n");
}
