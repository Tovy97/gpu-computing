
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>

#define DIM 4

__global__ void mult(int dim, int offset, int *matrixA, int *matrixB, int *result) {
	int id = threadIdx.x;
	int x = id / dim;
	int y = id % dim;
	result[offset + x * dim + y] = 0;
	for (int i = 0; i < dim; ++i) {
		result[offset + x * dim + y] += matrixA[offset + x * dim + i] * matrixB[offset + i * dim + y];
	}
}

__global__ void kronecker(int offset, int *matrixA, int *matrixB, int *result, int offsetI, int offsetK, int j) {
	int id = threadIdx.x;
	result[offset + id] = matrixA[j + offsetI] * matrixB[id + offsetK];
}

void printMatrix(int *matrix, int* dimension, int initialOffet = 0) {
	int size = 0;
	//int zero = 0;
	for (int i = 0; i < DIM; ++i) {
		size += dimension[i];
	}
	int offset = 0;
	int leftOffset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < leftOffset; ++k) {
				printf("   ");
				//printf("%02d ", zero);
			}
			for (int k = 0; k < dimension[i]; ++k) {
				printf("%02d ", matrix[initialOffet + offset + j * dimension[i] + k]);
			}
			for (int k = 0; k < (size - leftOffset) - dimension[i]; ++k) {
				printf("   ");
				//printf("%02d ", zero);
			}
			printf("\n");
		}
		leftOffset += dimension[i];
		offset += dimension[i] * dimension[i];
	}
	printf("\n");
}

void prodottoRigaColonna() {
	//creo il vettore delle dimensioni nell'host
	int dimension[DIM] = { 1, 2, 1 };

	//calcolo la dimensione della matrice e creo la matrice (linearizzata) nell'host
	int size = 0;
	for (int i = 0; i < DIM; ++i) {
		size += dimension[i] * dimension[i];
	}
	int *matrix = (int*)malloc(size * sizeof(int));

	//riempo la matrice dell'host
	int offset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < dimension[i]; ++k) {
				matrix[offset + j * dimension[i] + k] = dimension[i];
			}
		}
		offset += dimension[i] * dimension[i];
	}

	//stampo la matrice dell'host
	printf("Matrice A:\n");
	printMatrix(matrix, dimension);
	printf("Matrice B:\n");
	printMatrix(matrix, dimension);

	//creo la matrice per il device e vi copio il contenuto della matrice dell'host
	int *devMatrixs;
	cudaMalloc((void**)&devMatrixs, size * sizeof(int));
	cudaMemcpy(devMatrixs, matrix, size * sizeof(int), cudaMemcpyHostToDevice);

	//creo la matrice risultato del device
	int *devResult;
	cudaMalloc((void**)&devResult, size * sizeof(int));

	//per ogni blocco uso la GPU per computare la matrice risultato
	offset = 0;
	for (int i = 0; i < DIM; ++i) {
		mult << < 1, dimension[i] * dimension[i] >> > (dimension[i], offset, devMatrixs, devMatrixs, devResult);
		offset += dimension[i] * dimension[i];
	}

	//creo la matrice risultato dell'host e vi copio il contenuto della matrice risultato del device
	int * result = (int*)malloc(size * sizeof(int));
	cudaMemcpy(result, devResult, size * sizeof(int), cudaMemcpyDeviceToHost);

	//stampo la matrice risultato dell'host
	printf("Matrice risultato:\n");
	printMatrix(result, dimension);
}

void prodottoDiKronecker() {
	//creo il vettore delle dimensioni nell'host
	int dimension[DIM] = { 1,2,1 };

	//calcolo la dimensione della matrice e creo la matrice (linearizzata) nell'host
	int size = 0;
	for (int i = 0; i < DIM; ++i) {
		size += dimension[i] * dimension[i];
	}
	int *matrix = (int*)malloc(size * sizeof(int));

	//riempo la matrice dell'host
	int offset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < dimension[i]; ++k) {
				matrix[offset + j * dimension[i] + k] = dimension[i];
			}
		}
		offset += dimension[i] * dimension[i];
	}

	//stampo la matrice dell'host
	printf("Matrice A:\n");
	printMatrix(matrix, dimension);
	printf("Matrice B:\n");
	printMatrix(matrix, dimension);

	//creo la matrice per il device e vi copio il contenuto della matrice dell'host
	int *devMatrixs;
	cudaMalloc((void**)&devMatrixs, size * sizeof(int));
	cudaMemcpy(devMatrixs, matrix, size * sizeof(int), cudaMemcpyHostToDevice);

	//creo la matrice risultato del device
	int *devResult;
	cudaMalloc((void**)&devResult, size * size * sizeof(int));

	//per ogni blocco uso la GPU per computare la matrice risultato	
	int offsetI = 0;
	offset = 0;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i] * dimension[i]; ++j) {
			int offsetK = 0;
			for (int k = 0; k < DIM; ++k) {
				kronecker << <1, dimension[k] * dimension[k] >> > (offset, devMatrixs, devMatrixs, devResult, offsetI, offsetK, j);
				offset += dimension[k] * dimension[k];
				offsetK += dimension[k] * dimension[k];
			}
		}
		offsetI += dimension[i] * dimension[i];
	}

	//creo la matrice risultato dell'host e vi copio il contenuto della matrice risultato del device
	int * result = (int*)malloc(size * size * sizeof(int));
	cudaMemcpy(result, devResult, size * size * sizeof(int), cudaMemcpyDeviceToHost);

	//stampo la matrice risultato dell'host
	int a = 0, e = 0;
	printf("Matrice risultato:\n");
	for (int i = 0; i < size * size; i += size) {
		printf("Prodotto dell'elemento %02d del blocco %02d di A con B:\n", (e + 1), (a + 1));
		printMatrix(result, dimension, i);
		if ((e + 1) == dimension[a] * dimension[a]) {
			++a;
			e = 0;
		}
		else {
			++e;
		}
	}
}

int main()
{
	printf("Prodotto riga-colonna\n\n");
	prodottoRigaColonna();
	printf("\n\n\nProdotto di Kronecker\n\n");
	prodottoDiKronecker();

	return 0;
}