#include <stdio.h>
#include <stdlib.h>

#define DIM 3

int dimension[DIM] = { 1, 2, 1 };
int offset[DIM];
int size;

void initOffset() {
	int temp = 0;
	offset[0] = 0;
	for (int i = 1; i < DIM; ++i) {
		offset[i] = offset[i - 1] + dimension[i - 1] * dimension[i - 1];
	}
}

int readRC(int blocco, int*matrix, int riga, int colonna) {
	return matrix[offset[blocco] + riga * dimension[blocco] + colonna];
}

int read(int blocco, int*matrix, int pos) {
	return readRC(blocco, matrix, pos / dimension[blocco], pos % dimension[blocco]);
}

void writeRC(int blocco, int*matrix, int riga, int colonna, int value) {
	matrix[offset[blocco] + riga * dimension[blocco] + colonna] = value;
}

void write(int blocco, int*matrix, int pos, int value) {
	writeRC(blocco, matrix, pos / dimension[blocco], pos % dimension[blocco], value);
}

void printMatrix(int *matrix, int initialOffet) {
	int leftOffset = 0;
	matrix += initialOffet;
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i]; ++j) {
			for (int k = 0; k < leftOffset; ++k) {
				printf("   ");
			}
			for (int k = 0; k < dimension[i]; ++k) {
				printf("%02d ", readRC(i, matrix, j, k));
			}
			for (int k = 0; k < (size - leftOffset) - dimension[i]; ++k) {
				printf("   ");
			}
			printf("\n");
		}
		leftOffset += dimension[i];
	}
	printf("\n");
}

void prodottoRigaColonna(int *matrixA, int* matrixB) {
	//creo la matrice risultato
	int * result = (int*)malloc(size * sizeof(int));

	//per ogni blocco computo la matrice risultato
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i] * dimension[i]; ++j) {
			int value = 0;
			for (int k = 0; k < dimension[i]; ++k) {
				value += readRC(i, matrixA, j / dimension[i], k) * readRC(i, matrixB, k, j % dimension[i]);
			}
			write(i, result, j, value);
		}
	}

	//stampo la matrice risultato	
	printMatrix(result, 0);

	//libero la matrice risultato
	free(result);
}

void prodottoDiKronecker(int *matrixA, int* matrixB) {
	//creo la matrice risultato
	int * result = (int*)malloc(size * size * sizeof(int));
	int * temp = result;

	//per ogni blocco computto la matrice risultato	
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i] * dimension[i]; ++j) {
			for (int k = 0; k < DIM; ++k) {
				for (int h = 0; h < dimension[k] * dimension[k]; ++h) {
					write(k, temp, h, read(i, matrixA, j) * read(k, matrixB, h));
				}
			}
			temp += size;
		}
	}

	//stampo la matrice risultato
	int a = 0, e = 0;
	for (int i = 0; i < size * size; i += size) {
		printf("Prodotto dell'elemento %02d del blocco %02d di A con B:\n", (e + 1), (a + 1));
		printMatrix(result, i);
		if ((e + 1) == dimension[a] * dimension[a]) {
			++a;
			e = 0;
		}
		else {
			++e;
		}
	}

	//libero la matrice risultato
	free(result);
}

int main() {
	//calcolo la dimensione della matrice, l'offset e creo le matrici (linearizzata) 
	size = 0;
	for (int i = 0; i < DIM; ++i) {
		size += dimension[i] * dimension[i];
	}
	initOffset();
	int *matrixA = (int*)malloc(size * sizeof(int));
	int *matrixB = (int*)malloc(size * sizeof(int));

	//riempo le matrici
	for (int i = 0; i < DIM; ++i) {
		for (int j = 0; j < dimension[i] * dimension[i]; ++j) {
			write(i, matrixA, j, dimension[i]);
		}
	}
	memcpy(matrixB, matrixA, size * sizeof(int));

	//stampo la matrice
	printf("Matrice A:\n");
	printMatrix(matrixA, 0);
	printf("Matrice B:\n");
	printMatrix(matrixB, 0);

	//calcolo il prodotto riga-colonna
	printf("\nProdotto riga-colonna:\n");
	prodottoRigaColonna(matrixA, matrixB);
	//calcolo il prodotto di Kronecker
	printf("\nProdotto di Kronecker:\n");
	prodottoDiKronecker(matrixA, matrixB);

	//libero le matrici
	free(matrixA);
	free(matrixB);

	return 0;
}