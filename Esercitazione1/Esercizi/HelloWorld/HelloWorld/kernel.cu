#include "cuda_runtime.h"

#include <stdio.h>
#include <iostream> 

__global__ void kernel()
{
	printf("Hello from GPU!!!\n");
}

__global__ void add(int a, int b, int *c) {
	*c = a + b;
}

int main()
{
	printf("Hello from CPU!!!\n");
	kernel <<< 1, 10 >>> ();

	int c;
	int *dev_c;

	cudaMalloc((void**)&dev_c, sizeof(int));
	add <<< 1, 1 >>> (2, 7, dev_c);
	cudaDeviceSynchronize();
	cudaMemcpy(&c, dev_c, sizeof(int), cudaMemcpyDeviceToHost);

	//printf("2 + 7 = %d\n", *dev_c); RUNTIME EXCEPTION
	printf("2 + 7 = %d\n", c);
	cudaFree(dev_c);
/*
	int count;
	cudaGetDeviceCount(&count);
	printf("GPUS: %d\n", count);
	for (int i = 0; i < count; i++) {
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		printf(" --- General Information for device %d ---\n", i);
		printf("Name: %s\n", prop.name);
		printf("Compute capability: %d.%d\n", prop.major, prop.minor);
		printf("Clock rate: %d\n", prop.clockRate);
		printf("Device copy overlap: ");
		if (prop.deviceOverlap)
			printf("Enabled\n");
		else
			printf("Disabled\n");
		printf("Kernel execition timeout : ");
		if (prop.kernelExecTimeoutEnabled)
			printf("Enabled\n");
		else
			printf("Disabled\n");
		printf(" --- Memory Information for device %d ---\n", i);
		printf("Total global mem: %ld\n", prop.totalGlobalMem);
		printf("Total constant Mem: %ld\n", prop.totalConstMem);
		printf("Max mem pitch: %ld\n", prop.memPitch);
		printf("Texture Alignment: %ld\n", prop.textureAlignment);
		printf(" --- MP Information for device %d ---\n", i);
		printf("Multiprocessor count: %d\n",
			prop.multiProcessorCount);
		printf("Shared mem per mp: %ld\n", prop.sharedMemPerBlock);
		printf("Registers per mp: %d\n", prop.regsPerBlock);
		printf("Threads in warp: %d\n", prop.warpSize);
		printf("Max threads per block: %d\n",
			prop.maxThreadsPerBlock);
		printf("Max thread dimensions: (%d, %d, %d)\n",
			prop.maxThreadsDim[0], prop.maxThreadsDim[1],
			prop.maxThreadsDim[2]);
		printf("Max grid dimensions: (%d, %d, %d)\n",
			prop.maxGridSize[0], prop.maxGridSize[1],
			prop.maxGridSize[2]);
		printf("\n");
	}


	cudaDeviceProp prop;
	int dev;
	cudaGetDevice(&dev);
	printf("ID of current CUDA device: %d\n", dev);
	memset(&prop, 0, sizeof(cudaDeviceProp));
	prop.major = 1;
	prop.minor = 3;
	cudaChooseDevice(&dev, &prop);
	printf("ID of CUDA device closest to revision 1.3: %d\n", dev);
	cudaSetDevice(dev);
	cudaGetDevice(&dev);
	printf("ID of current CUDA device: %d\n", dev);*/
    return 0;
}
