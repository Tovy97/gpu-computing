/*
 *  Prodotto di matrici diagonali a blocchi
 */

#include <stdio.h>
#include <stdlib.h>

void block_mat_prod(int *, int, int, int *, int *, int *);
void block_prod(int, int, int *, int *, int *);
void print_mat(int, int *);

int main(void) {
	// dati iniziali del problema
	int d[] = { 4, 2, 1, 3 };
	int N = 4, n = 10;
	int *A, *B, *C;

	A = (int *)malloc(n * n * sizeof(int));
	B = (int *)malloc(n * n * sizeof(int));
	C = (int *)malloc(n * n * sizeof(int));
	memset(A, 0, n * n * sizeof(int));
	memset(B, 0, n * n * sizeof(int));
	memset(C, 0, n * n * sizeof(int));

	// matrici di esempio: carica in ogni blocco la dim del blocco stesso
	int row_offset = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < d[i]; j++) {
			for (int k = 0; k < d[i]; k++) {				
				A[row_offset * n + j * n + k + row_offset] = d[i];
				B[row_offset * n + j * n + k + row_offset] = d[i];
			}
		}
		row_offset += d[i];
	}

	// calcola prodotto
	block_mat_prod(d, N, n, A, B, C);

	// stampa matrici
	print_mat(n, A);
	print_mat(n, B);
	print_mat(n, C);

	// libera spazio
	free(A);
	free(B);
	free(C);

	return 0;
}

/*
 * prodotto di matrici a blocchi
 */
void block_mat_prod(int *d, int N, int n, int *A, int *B, int *C) {
	int offset = 0;

	for (int i = 0; i < N; i++) {
		block_prod(d[i], n, A + offset, B + offset, C + offset);
		offset += d[i] * (n + 1);
	}
}

/*
 * prodotto di due blocchi (sottomatrici)
 */
void block_prod(int d, int n, int *X, int *Y, int *Z) {
	int s, idx, idy;

	for (int i = 0; i < d; i++) {
		for (int j = 0; j < d; j++) {
			s = 0;
			for (int k = 0; k < d; k++) {
				idx = i * n + k;        // scandisce le colonne
				idy = k * n + j;        // scandisce le righe
				s += X[idx] * Y[idy];
			}
			Z[i * n + j] = s;
		}
	}
}

// stampa matrice
void print_mat(int n, int *M) {
	for (int i = 0; i < n * n; i++) {
		if (i % n == 0)
			printf("\n");
		if (M[i] == 0)
			printf("%s", "  ");
		else
			printf("%3d", M[i]);
	}
	printf("\n");
}