#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../../../Lib/common.h"

#define ARRAY_SIZE 512
// Used in qsort, to stop it when recursion is too deep or there is not enough elements
// unsorted to justify another stream, according to NVIDIA's simple quick sort sample.
#define MAX_DEPTH       16
#define INSERTION_SORT  16

void quickSort(int*, unsigned, unsigned);
void insertSort(int*, unsigned);
void random_array(int*, unsigned);
void quickSort(int*, unsigned, unsigned);
void printArray(int*, int);

clock_t start, end;
double elapsed_time;
int numberOfComparisons;
int numberOfSwaps;

/**
 * NVIDIA's Kernel: Selection sort used when depth gets too big or
 * the number of elements drops below a threshold
 */
__device__ void selectionSort(int* data, int left, int right) {
	for (int i = left; i <= right; ++i) {
		int min_val = data[i];
		int min_idx = i;

		// Find the smallest value in the range [left, right].
		for (int j = i + 1; j <= right; ++j) {
			int val_j = data[j];
			if (val_j < min_val) {
				min_idx = j;
				min_val = val_j;
			}
		}

		// Swap the values.
		if (i != min_idx) {
			data[min_idx] = data[i];
			data[i] = min_val;
		}
	}
}

/**
 * Kernel: basic quicksort algorithm, recursively launching the next level
 */
__global__ void quickSortStream(int* data, int left, int right, int depth) {
	//If we're too deep or there are few elements left, we use an insertion sort...
	if (depth >= MAX_DEPTH || right - left <= INSERTION_SORT) {
		selectionSort(data, left, right);
		return;
	}

	printf("passat0!!!!!  %d", depth);
	cudaStream_t streamL, streamR;
	int* lptr = data + left;
	int* rptr = data + right;
	int pivot = data[(left + right) / 2];

	int lval;
	int rval;

	int nright, nleft;

	// Do the partitioning
	while (lptr <= rptr) {
		// Find the next left- and right-hand values to swap
		lval = *lptr;
		rval = *rptr;

		// Move the left pointer as long as the pointed element is smaller than the pivot.
		while (lval < pivot && lptr < data + right) {
			lptr++;
			lval = *lptr;
		}

		// Move the right pointer as long as the pointed element is larger than the pivot.
		while (rval > pivot && rptr > data + left) {
			rptr--;
			rval = *rptr;
		}

		// If the swap points are valid, do the swap!
		if (lptr <= rptr) {
			*lptr = rval;
			*rptr = lval;
			lptr++;
			rptr--;
		}
	}

	// Now the recursive part
	nright = rptr - data;
	nleft = lptr - data;

	// Launch a new block to sort the left part.
	if (left < (rptr - data)) {
		cudaStreamCreateWithFlags(&streamL, cudaStreamNonBlocking);
		quickSortStream << <1, 1, 0, streamL >> > (data, left, nright, depth + 1);
		cudaStreamDestroy(streamL);
	}

	// Launch a new block to sort the right part.
	if ((lptr - data) < right) {
		cudaStreamCreateWithFlags(&streamR, cudaStreamNonBlocking);
		quickSortStream << <1, 1, 0, streamR >> > (data, nleft, right, depth + 1);
		cudaStreamDestroy(streamR);
	}
}

/**
 * MAIN
 */
int main(void) {

	// Create the vector with the specified size and situation
	int* array;
	unsigned long N = ARRAY_SIZE;

	// managed memory
	cudaMallocManaged((void**)&array, N * sizeof(int));
	// random instance
	random_array(array, N);

	// CPU qsort
	quickSort(array, 0, N - 1);

	printArray(array, N);

	random_array(array, N);
	// Launch on device
	quickSortStream << < 1, 1 >> > (array, 0, N - 1, 0);
	cudaDeviceSynchronize();
	printArray(array, N);

	cudaFree(array);
	return 0;
}


// A utility function to swap two elements
void swap(int* a, int* b) {
	int t = *a;
	*a = *b;
	*b = t;
}

/* This function takes last element as pivot, places the pivot element at
 * its correct position in sorted array, and places all smaller (smaller
 * than pivot) to left of pivot and all greater elements to right of pivot */
int partition(int arr[], int low, int high) {
	int pivot = arr[high]; // pivot
	int i = (low - 1); // Index of smaller element

	for (int j = low; j <= high - 1; j++) {
		// If current element is smaller than or
		// equal to pivot
		if (arr[j] <= pivot) {
			i++; // increment index of smaller element
			swap(&arr[i], &arr[j]);
		}
	}
	swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}

/* The main function that implements QuickSort
 * arr[] --> Array to be sorted,
 * low --> Starting index,
 * high --> Ending index
 */
void quickSort(int arr[], unsigned low, unsigned high) {
	if (low < high) {
		// pi is partitioning index,
		// arr[p] is now at right place
		int pi = partition(arr, low, high);

		// Separately sort elements before
		// partition and after partition
		quickSort(arr, low, pi - 1);
		quickSort(arr, pi + 1, high);
	}
}

/*
 * Function to print an array
 */
void printArray(int arr[], int size) {
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("n");
}

/**
 * InsertionSort
 */
void insertSort(int* array, unsigned size) {
	int i, j;
	int selected;
	for (i = 1; i < size; i++) {
		selected = array[i];
		j = i - 1;
		while ((j >= 0) && (selected < array[j])) {
			array[j + 1] = array[j];
			j--;
			numberOfComparisons++;
		}
		numberOfSwaps++;
		array[j + 1] = selected;
	}
}

/**
 * Function that fills an array with random integers
 * @param int* array Reference to the array that will be filled
 * @param int  size  Number of elements
 */
void random_array(int* array, unsigned size) {
	srand(time(NULL));
	int i;
	for (i = 0; i < size; i++)
		array[i] = rand() % size;
}

/**
 * Function that fills an array with integers in ascending order
 * @param int* array Reference to the array that will be filled
 * @param int  size  Number of elements
 */
void ascending_array(int* array, unsigned size) {
	int i;
	for (i = 1; i <= size; i++)
		array[i] = i;
}

/**
 * Function that fills an array with integers in ascending order
 * @param int* array Reference to the array that will be filled
 * @param int  size  Number of elements
 */
void descending_array(int* array, unsigned size) {
	int i, j;
	for (i = 0, j = size; i < size; i++, j--)
		array[i] = j;
}