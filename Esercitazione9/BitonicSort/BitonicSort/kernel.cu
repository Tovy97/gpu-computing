#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>
#include "../../../Lib/common.h"

__global__ void bitonic_sort(int* in) {
	int tid = threadIdx.x;
	int offset = blockDim.x * blockIdx.x * 4;

	__shared__ int tile[128];

	tile[tid] = in[tid + offset];
	tile[tid + 32] = in[tid + offset + 32];
	tile[tid + 64] = in[tid + offset + 64];
	tile[tid + 96] = in[tid + offset + 96];

	for (int i = 2, group_witdh = 1, group = tid % 32; i < 128; i *= 2, group_witdh *= 2, group /= 2) {
		int index = (tid % group_witdh) + group * i * 2;
		for (int j = i / 2, k = group_witdh; j > 0; j /= 2, k /= 2) {
			if (tile[index] > tile[index + j]) {
				int temp = tile[index];
				tile[index] = tile[index + j];
				tile[index + j] = temp;
			}
			int index2 = 127 - index;
			if (tile[index2] > tile[index2 - j]) {
				int temp = tile[index2];
				tile[index2] = tile[index2 - j];
				tile[index2 - j] = temp;
			}
			if ((tid % k) >= k / 2) {
				index = index + k / 2;
			}
		}
	}

	int index = tid;
	for (int j = 64, k = 32; j > 0; j /= 2, k /= 2) {
		if (tile[index] > tile[index + j]) {
			int temp = tile[index];
			tile[index] = tile[index + j];
			tile[index + j] = temp;
		}
		int index2 = index + k;
		if (tile[index2] > tile[index2 + j]) {
			int temp = tile[index2];
			tile[index2] = tile[index2 + j];
			tile[index2 + j] = temp;
		}

		if (k == 1) {
			index = tid * 4;
			k = 4;
		}
		else if ((tid % k) >= k / 2) {
			index = index + k + k / 2;
		}
	}

	in[tid + offset] = tile[tid];
	in[tid + offset + 32] = tile[tid + 32];
	in[tid + offset + 64] = tile[tid + 64];
	in[tid + offset + 96] = tile[tid + 96];
}

__global__ void bitonic_merge(const int* in, int* out, const int size) {
	int tid = threadIdx.x;
	int offset = blockDim.x * blockIdx.x * (size / 32);

	__shared__ int tile[64];

	const int* A = &in[offset];
	const int* B = &in[offset + (size / 2)];

	tile[31 - tid] = A[tid];
	tile[32 + tid] = B[tid];

	int maxA = tile[0];
	int maxB = tile[63];
	int indexA = 32;
	int indexB = 32;
	int indexOut = 0;

	while (true) {
		int index = tid;
		for (int j = 32, k = 32; j > 0; j /= 2, k /= 2) {
			if (tile[index] > tile[index + j]) {
				int temp = tile[index];
				tile[index] = tile[index + j];
				tile[index + j] = temp;
			}
			if ((tid % k) >= k / 2) {
				index = index + k / 2;
			}
		}
		out[offset + indexOut + tid] = tile[tid];

		indexOut += 32;
		if (indexOut >= size - 32) {
			break;
		}
		if ((maxA < maxB && indexA < (size / 2)) || (indexB >= (size / 2))) {
			tile[31 - tid] = A[indexA + tid];
			maxA = tile[0];
			indexA += 32;
		}
		else {
			tile[31 - tid] = B[indexB + tid];
			maxB = tile[0];
			indexB += 32;
		}
	}

	out[offset + indexOut + tid] = tile[tid + 32];
}

int main(int argc, char* argv[]) {

	for (int N = 128, p = 7; N <= (1 << 26); N = N << 1, ++p) {

		printf("------ 2 ^ %d = %d -------\n", p, N);

		srand(time(NULL));

		int* in = (int*)malloc(N * sizeof(int));
		int* d_in, * d_out;

		CHECK(cudaMalloc(&d_in, N * sizeof(int)));
		CHECK(cudaMalloc(&d_out, N * sizeof(int)));

		for (int i = 0; i < N; ++i) {
			in[i] = rand();
		}

		CHECK(cudaMemcpy(d_in, in, N * sizeof(int), cudaMemcpyHostToDevice));

		cudaEvent_t start, stop;

		CHECK(cudaEventCreate(&start));
		CHECK(cudaEventCreate(&stop));

		CHECK(cudaEventRecord(start));

	
		bitonic_sort << < N / 128, 32 >> > (d_in);

		int n = 256;
		int l = N / 256;		

		while (l >= 1 && N % n == 0) {

			bitonic_merge << < l, 32 >> > (d_in, d_out, n);
			
			CHECK(cudaMemcpy(d_in, d_out, sizeof(int) * N, cudaMemcpyDeviceToDevice));
			
			n *= 2;
			l = N / n;			
		}



		CHECK(cudaEventRecord(stop));
		CHECK(cudaEventSynchronize(stop));
		float ms;
		cudaEventElapsedTime(&ms, start, stop);

		printf("Time: %.2f secondi\n", (ms / 1000.0));

		int* out = (int*)malloc(N * sizeof(int));

		CHECK(cudaMemcpy(out, d_out, N * sizeof(int), cudaMemcpyDeviceToHost));

		bool err = false;

		for (int i = 0; i < N - 1; ++i) {
			if (out[i] > out[i + 1]) {
				err = true;
				break;
			}
		}
		if (err) {
			printf("ERRORE\n\n");
		}
		else {
			printf("OK\n\n");
		}

		free(out);
	}
	return 0;
}