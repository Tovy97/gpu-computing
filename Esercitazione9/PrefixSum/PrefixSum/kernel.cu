#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../../../Lib/common.h"

#define BLOCKSIZE 32

__global__ void prefix_sum_not_efficient(const double* x, double* y, const unsigned int N) {
	const unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned int tid = threadIdx.x;

	__shared__ double smem[BLOCKSIZE];

	if (idx < N) {
		smem[tid] = x[idx];

		for (unsigned int stride = 1; stride <= tid; stride *= 2) {
			//__syncthreads(); // --> Da rimettere se BLOCKSIZE > 32
			smem[tid] += smem[tid - stride];
		}

		y[idx] = smem[tid];
	}
}

__global__ void prefix_sum_efficient(const double* x, double* y, const unsigned int N) {
	const unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned int tid = threadIdx.x;

	__shared__ double smem[BLOCKSIZE];

	int limit = (int)log2f((float)BLOCKSIZE);

	if (idx < N) {
		smem[tid] = x[idx];

		for (unsigned int k = 1; k < limit; ++k) {
			__syncthreads();
			int off = (int)powf(2.0f, (float)k);
			if ((tid + 1) % off == 0) {
				smem[tid] += smem[tid - (off / 2)];
			}
		}
		__syncthreads();
		if (tid == 0) {
			if (BLOCKSIZE < N) {
				smem[BLOCKSIZE - 1] = 1.0;
			}
			else {
				smem[N - 1] = 1.0;
			}
		}

		for (unsigned int k = limit; k >= 1; --k) {
			__syncthreads();
			int off = (int)powf(2.0f, (float)k);
			if ((tid + 1) % off == 0) {
				off /= 2;
				double temp = smem[tid];
				smem[tid] += smem[tid - off];
				smem[tid - off] = temp;
			}
		}
		y[idx] = smem[tid];
	}
}

__global__ void prefix_sum_not_efficient_32(const double* x, double* y, const unsigned int N) {
	const unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned int tid = threadIdx.x;

	__shared__ double smem[32];

	if (idx < N) {
		smem[tid] = x[idx];						//32 thread		

		if (tid < 1) {
			y[idx] = smem[tid];
			return;
		}

		__syncthreads();

		smem[tid] += smem[tid - 1];				//31 thread

		if (tid < 2) {
			y[idx] = smem[tid];
			return;
		}

		__syncthreads();

		smem[tid] += smem[tid - 2];				//30 thread

		if (tid < 4) {
			y[idx] = smem[tid];
			return;
		}

		__syncthreads();

		smem[tid] += smem[tid - 4];				//28 thread

		if (tid < 8) {
			y[idx] = smem[tid];
			return;
		}

		__syncthreads();

		smem[tid] += smem[tid - 8];				//24 thread

		if (tid < 16) {
			y[idx] = smem[tid];
			return;
		}

		__syncthreads();

		smem[tid] += smem[tid - 16];				//16 thread

		y[idx] = smem[tid];

	}
}

__global__ void prefix_sum_efficient_32(const double* x, double* y, const unsigned int N) {
	const unsigned int idx = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned int tid = threadIdx.x;

	__shared__ double smem[32];

	if (idx < N) {
		smem[tid] = x[idx];

		__syncthreads();
		if ((tid + 1) % 2 == 0) {
			smem[tid] += smem[tid - 1];
		}
		__syncthreads();
		if ((tid + 1) % 4 == 0) {
			smem[tid] += smem[tid - 2];
		}
		__syncthreads();
		if ((tid + 1) % 8 == 0) {
			smem[tid] += smem[tid - 4];
		}
		__syncthreads();
		if ((tid + 1) % 16 == 0) {
			smem[tid] += smem[tid - 8];
		}

		__syncthreads();
		if (tid == 0) {
			if (32 < N) {
				smem[31] = 1.0;
			}
			else {
				smem[N - 1] = 1.0;
			}
		}

		__syncthreads();
		if ((tid + 1) % 32 == 0) {
			double temp = smem[tid];
			smem[tid] += smem[tid - 16];
			smem[tid - 16] = temp;
		}
		__syncthreads();
		if ((tid + 1) % 16 == 0) {
			double temp = smem[tid];
			smem[tid] += smem[tid - 8];
			smem[tid - 8] = temp;
		}
		__syncthreads();
		if ((tid + 1) % 8 == 0) {
			double temp = smem[tid];
			smem[tid] += smem[tid - 4];
			smem[tid - 4] = temp;
		}
		__syncthreads();
		if ((tid + 1) % 4 == 0) {
			double temp = smem[tid];
			smem[tid] += smem[tid - 2];
			smem[tid - 2] = temp;
		}
		__syncthreads();
		if ((tid + 1) % 2 == 0) {
			double temp = smem[tid];
			smem[tid] += smem[tid - 1];
			smem[tid - 1] = temp;
		}
		__syncthreads();

		y[idx] = smem[tid];
	}
}

void prefix_sum(const double* a, double* b, const unsigned int n) {
	b[0] = a[0];
	for (unsigned int i = 1; i < n; i++) {
		b[i] = b[i - 1] + a[i];
	}
}

bool check(const double* out_cpu, const double* out_gpu, const int N) {
	for (int i = 0; i < N; ++i) {
		if (out_cpu[i] != out_gpu[i]) {
			return false;
		}
	}
	return true;
}

void print(double* arr, const unsigned int N) {
	printf("\n");
	for (unsigned int i = 0; i < N; ++i) {
		printf("%.2f ", arr[i]);
	}
	printf("\n");
}

int main() {
	const unsigned int N = 1 << 26;
	const unsigned int Byte = sizeof(double) * N;

	double* in, * out, * temp;
	in = (double*)malloc(Byte);
	out = (double*)malloc(Byte);
	temp = (double*)malloc(Byte);
	double* d_in, * d_out;

	CHECK(cudaMalloc(&d_in, Byte));
	CHECK(cudaMalloc(&d_out, Byte));

	for (int i = 0; i < N; ++i) {
		in[i] = 1;
	}

	cudaEvent_t start, stop;
	CHECK(cudaEventCreate(&start));
	CHECK(cudaEventCreate(&stop));


	/*****************************CPU**************************************/
	CHECK(cudaEventRecord(start));
	CHECK(cudaEventSynchronize(start));

	prefix_sum(in, out, N);

	CHECK(cudaEventRecord(stop));
	CHECK(cudaEventSynchronize(stop));

	float ms;
	CHECK(cudaEventElapsedTime(&ms, start, stop));
	printf("CPU time: \t\t %.2f\n", ms);

	/*****************************GPU 1**************************************/
	CHECK(cudaMemcpy(d_in, in, Byte, cudaMemcpyHostToDevice));

	CHECK(cudaEventRecord(start));
	prefix_sum_not_efficient << <CEIL(N, BLOCKSIZE), BLOCKSIZE >> > (d_in, d_out, N);

	CHECK(cudaEventRecord(stop));
	CHECK(cudaEventSynchronize(stop));

	CHECK(cudaMemcpy(temp, d_out, Byte, cudaMemcpyDeviceToHost));
	for (int i = BLOCKSIZE, blocco = BLOCKSIZE - 1; i < N; ) {
		temp[i] += temp[blocco];
		++i;
		if (i % BLOCKSIZE == 0) {
			blocco += BLOCKSIZE;
		}
	}
	CHECK(cudaEventElapsedTime(&ms, start, stop));
	printf("GPU 1 time: \t\t %.2f", ms);

	if (check(out, temp, N)) {
		printf("\tCHECK PASSED\n");
	}
	else {
		printf("\tCHECK NOT PASSED\n");
	}

	//print(temp, N);

	/*****************************GPU 2**************************************/
	CHECK(cudaMemcpy(d_in, in, Byte, cudaMemcpyHostToDevice));

	CHECK(cudaEventRecord(start));
	prefix_sum_efficient << <CEIL(N, BLOCKSIZE), BLOCKSIZE >> > (d_in, d_out, N);
	CHECK(cudaEventRecord(stop));

	CHECK(cudaEventSynchronize(stop));

	CHECK(cudaMemcpy(temp, d_out, Byte, cudaMemcpyDeviceToHost));
	for (int i = BLOCKSIZE, blocco = BLOCKSIZE - 1; i < N; ) {
		temp[i] += temp[blocco];
		++i;
		if (i % BLOCKSIZE == 0) {
			blocco += BLOCKSIZE;
		}
	}

	CHECK(cudaEventElapsedTime(&ms, start, stop));
	printf("GPU 2 time: \t\t %.2f", ms);

	if (check(out, temp, N)) {
		printf("\tCHECK PASSED\n");
	}
	else {
		printf("\tCHECK NOT PASSED\n");
	}

	//print(temp, N);	

	/*****************************GPU 3 -- ver 32**************************************/
	CHECK(cudaMemcpy(d_in, in, Byte, cudaMemcpyHostToDevice));

	CHECK(cudaEventRecord(start));
	prefix_sum_not_efficient_32 << <CEIL(N, 32), 32 >> > (d_in, d_out, N);

	CHECK(cudaEventRecord(stop));
	CHECK(cudaEventSynchronize(stop));

	CHECK(cudaMemcpy(temp, d_out, Byte, cudaMemcpyDeviceToHost));
	for (int i = 32, blocco = 31; i < N; ) {
		temp[i] += temp[blocco];
		++i;
		if (i % 32 == 0) {
			blocco += 32;
		}
	}
	CHECK(cudaEventElapsedTime(&ms, start, stop));
	printf("GPU 3 time: \t\t %.2f", ms);

	if (check(out, temp, N)) {
		printf("\tCHECK PASSED\n");
	}
	else {
		printf("\tCHECK NOT PASSED\n");
	}

	//print(temp, N);

	/*****************************GPU 4 -- ver 32**************************************/
	CHECK(cudaMemcpy(d_in, in, Byte, cudaMemcpyHostToDevice));

	CHECK(cudaEventRecord(start));
	prefix_sum_efficient_32 << <CEIL(N, 32), 32 >> > (d_in, d_out, N);
	CHECK(cudaEventRecord(stop));

	CHECK(cudaEventSynchronize(stop));

	CHECK(cudaMemcpy(temp, d_out, Byte, cudaMemcpyDeviceToHost));
	for (int i = 32, blocco = 31; i < N; ) {
		temp[i] += temp[blocco];
		++i;
		if (i % 32 == 0) {
			blocco += 32;
		}
	}

	CHECK(cudaEventElapsedTime(&ms, start, stop));
	printf("GPU 4 time: \t\t %.2f", ms);

	if (check(out, temp, N)) {
		printf("\tCHECK PASSED\n");
	}
	else {
		printf("\tCHECK NOT PASSED\n");
	}

	//print(temp, N);

	return EXIT_SUCCESS;
}