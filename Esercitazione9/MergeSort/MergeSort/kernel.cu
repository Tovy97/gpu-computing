#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>
#include "../../../Lib/common.h"

//#define N (1 << 1)
#define BLOCK 1024

__global__ void merge(const int* in, int* out, int dim, int N) {
	const int tid = threadIdx.x + blockIdx.x * blockDim.x;

	if (tid >= N / dim) {
		return;
	}

	const int* A = &in[tid * dim];
	const int* B = &in[tid * dim + dim / 2];
	int* C = &out[tid * dim];

	int indexA = 0;
	int indexB = 0;
	int indexC = 0;

	while (indexC < dim) {
		if ((A[indexA] < B[indexB] && indexA < dim / 2) || (indexB >= dim / 2)) {
			C[indexC] = A[indexA];
			++indexA;
		}
		else {
			C[indexC] = B[indexB];
			++indexB;
		}
		++indexC;
	}
}

__global__ void merge_efficient(const int* in, int* out, int dim, int N) {
	const int idx = threadIdx.x + blockIdx.x * blockDim.x;
	const int bid = idx / (dim / 2);
	const int tid = idx % (dim / 2);

	if (idx >= N / 2) {
		return;
	}

	const int* A = &in[bid * dim];
	const int* B = &in[bid * dim + dim / 2];
	int* C = &out[bid * dim];

	int indexA = tid;
	int indexB = tid;
	if (A[tid] > B[tid]) {
		while (indexA < dim / 2 && A[tid] > B[indexA]) {
			++indexA;
		}
		while (indexB > 0 && A[indexB - 1] > B[tid]) {
			--indexB;
		}
	}
	else {
		while (indexA > 0 && A[tid] < B[indexA - 1]) {
			--indexA;
		}
		while (indexB < dim / 2 && A[indexB] < B[tid]) {
			++indexB;
		}
	}
	indexA += tid;
	indexB += tid;

	C[indexA] = A[tid];
	C[indexB] = B[tid];
}

int main(int argc, char* argv[]) {

	/*if (argc != 2) {
		printf("Manca la dimensione (da passare come parametro)");
		exit(1);
	}

	const int N = atoi(argv[1]);
	*/

	for (int N = 1, p = 0; N <= (1<<26); N=N<<1, ++p) {

		printf("------ 2 ^ %d = %d -------\n",p,N);

		srand(time(NULL));

		int* in = (int*)malloc(N * sizeof(int));
		int* d_in, * d_out;

		CHECK(cudaMalloc(&d_in, N * sizeof(int)));
		CHECK(cudaMalloc(&d_out, N * sizeof(int)));

		for (int i = 0; i < N; ++i) {
			in[i] = i;
		}

		for (int i = 0; i < N; ++i) {
			int tempI, temp;
			tempI = rand() % N;
			temp = in[0];
			in[0] = in[tempI];
			in[tempI] = temp;
		}

		bool ord = true;

		for (int i = 0; i < N - 1; ++i) {
			if (in[i] > in[i + 1]) {
				ord = false;
				break;
			}
		}
		if (ord) {
			printf("Ordinato\n\n");
		}
		else {
			printf("Disordinato\n\n");
		}

		CHECK(cudaMemcpy(d_in, in, N * sizeof(int), cudaMemcpyHostToDevice));

		cudaEvent_t start, stop;

		CHECK(cudaEventCreate(&start));
		CHECK(cudaEventCreate(&stop));

		CHECK(cudaEventRecord(start));

		dim3 block = BLOCK;

		for (int i = 0, dim = 2; i < log2(N); ++i, dim *= 2) {
			dim3 grid = CEIL((N / dim), block.x);
			merge << <grid, block >> > (d_in, d_out, dim, N);
			CHECK(cudaMemcpy(d_in, d_out, N * sizeof(int), cudaMemcpyDeviceToDevice));
		}
		CHECK(cudaEventRecord(stop));
		CHECK(cudaEventSynchronize(stop));
		float ms;
		cudaEventElapsedTime(&ms, start, stop);

		printf("Time: %.2f secondi\n", (ms / 1000.0));

		int* out = (int*)malloc(N * sizeof(int));

		CHECK(cudaMemcpy(out, d_out, N * sizeof(int), cudaMemcpyDeviceToHost));

		bool err = false;

		for (int i = 0; i < N - 1; ++i) {
			if (out[i] > out[i + 1]) {
				err = true;
				break;
			}
		}
		if (err) {
			printf("ERRORE\n\n");
		}
		else {
			printf("OK\n\n");
		}

		free(out);

		/***********************V2***************************/

		CHECK(cudaMemcpy(d_in, in, N * sizeof(int), cudaMemcpyHostToDevice));
		free(in);

		CHECK(cudaEventRecord(start));

		dim3 grid = CEIL(N / 2, block.x);
		for (int i = 0, dim = 2; i < log2(N); ++i, dim *= 2) {
			merge_efficient << <grid, block >> > (d_in, d_out, dim, N);
			CHECK(cudaMemcpy(d_in, d_out, N * sizeof(int), cudaMemcpyDeviceToDevice));
		}
		CHECK(cudaEventRecord(stop));
		CHECK(cudaEventSynchronize(stop));
		cudaEventElapsedTime(&ms, start, stop);

		printf("Time: %.2f secondi\n", (ms / 1000.0));

		out = (int*)malloc(N * sizeof(int));

		CHECK(cudaMemcpy(out, d_out, N * sizeof(int), cudaMemcpyDeviceToHost));

		CHECK(cudaFree(d_in));
		CHECK(cudaFree(d_out));
		CHECK(cudaDeviceReset());

		err = false;

		for (int i = 0; i < N - 1; ++i) {
			if (out[i] > out[i + 1]) {
				err = true;
				break;
			}
		}
		if (err) {
			printf("ERRORE\n\n");
		}
		else {
			printf("OK\n\n");
		}

		free(out);
	}
	return 0;
}